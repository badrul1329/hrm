$(document).ready(function () {
    $('.select2').select2();
    // Menu navigation
    $(".app-sidebar a").each(function () {
        if (window.location.href === $(this).attr('href')) {
            $(this).addClass('active');
            $(this).closest('.nav-item').addClass('open active');
        }
    });

    // date dropper
    $(".date").dateDropper({
        // dropBackgroundColor: "#bdc3c7",
        // dropBorder: "1px solid #1abc9c",
        dropBorderRadius: "20px",
        dropPrimaryColor: "#1abc9c",
        // dropShadow: "0 0 20px 0 rgba(26, 188, 156, 0.6)",
        // dropTextColor: "#FFF",
        dropWidth: 500,
        format: "d-M-Y",
        init_animation: "bounce",
        // lang: "ar",
        // lock: "from",
        // maxYear: "2020",
        // minYear: "1990",
        // yearsRange: "5",
        large:true,
        largeDefault: true,
        initSet: true
    });
    //time dropper
    $(".time").timeDropper({
        format: "HH:mm",
        init_animation: "bounce",
    });
    $.fn.modal.Constructor.prototype._enforceFocus = function() {};
    $.fn.dataTable.ext.errMode = 'throw';

    // $('table').DataTable({
    //     responsive: true,
    // //     select: true,
    // //     scrollX: true,
    //     pagingType: "full_numbers",
    //     lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    //     language: {
    //         "decimal": ",",
    //         "thousands": "."
    //     }
    // });
});
