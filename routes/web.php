<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('hrm');

});

Auth::routes();

Route::get('/home', function(){
    return redirect('hrm');
})->name('home');

Route::get('/users/all', [\App\Http\Controllers\UserController::class,'all'])->name('user.all');
Route::get('/roles/all', [\App\Http\Controllers\RoleController::class,'all'])->name('role.all');
Route::get('/users/{user}/change-status', [\App\Http\Controllers\UserController::class,'changeStatus']);
Route::get('/currencies/all', [\App\Http\Controllers\CurrencyController::class,'all'])->name('currency.all');
Route::get('/languages/all', [\App\Http\Controllers\LanguageController::class,'all'])->name('language.all');
Route::get('/locale/{locale}', [\App\Http\Controllers\LanguageController::class,'setLocale']);
Route::get('/messenger/users', [\App\Http\Controllers\MessengerController::class,'users']);
Route::get('/notices/all', [\App\Http\Controllers\NoticeController::class,'all'])->name('notice.all');
Route::get('/marital-info/all', [\App\Http\Controllers\MaritalInfoController::class,'all'])->name('marital_info.all');
Route::get('/religions/all', [\App\Http\Controllers\ReligionController::class,'all'])->name('religion.all');
Route::apiResources([
    'users' => \App\Http\Controllers\UserController::class,
    'roles' => \App\Http\Controllers\RoleController::class,
    'user-role' => \App\Http\Controllers\UserRoleController::class,
    'role-permission' => \App\Http\Controllers\RolePermissionController::class,
    'user-permission' => \App\Http\Controllers\UserPermissionController::class,
    'currencies' => \App\Http\Controllers\CurrencyController::class,
    'languages' => \App\Http\Controllers\LanguageController::class,
    'phrases' => \App\Http\Controllers\PhraseController::class,
    'messenger' => \App\Http\Controllers\MessengerController::class,
    'calender' => \App\Http\Controllers\CalenderController::class,
    'notices' => \App\Http\Controllers\NoticeController::class,
    'activities' => \App\Http\Controllers\ActivityController::class
]);
Route::resource('languages.phrases', \App\Http\Controllers\LanguagePhraseController::class, ['only' => ['index', 'store']]);
Route::get('role-permission', [\App\Http\Controllers\RolePermissionController::class,'index'])->name('role-permission.index');
Route::get('user-permission', [\App\Http\Controllers\UserPermissionController::class,'index'])->name('user-permission.index');
Route::get('symlink',function(){
    $target = storage_path('app/public');
    $shortcut = public_path('/storage');
    //dd($target,$shortcut);
    if(File::exists($shortcut)) {
        File::deleteDirectory($shortcut);
    }
    symlink($target, $shortcut);
//        exec('mklink /j "' . str_replace('/', '\\', $shortcut) . '" "' . str_replace('/', '\\', $target) . '"');
    return redirect('/');
});
