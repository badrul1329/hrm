-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2022 at 09:37 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_group_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_group_id`, `name`, `phone`, `email`, `address`, `city`, `state`, `zip`, `country`, `website`, `vat_number`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'VonRueden-Simonis', '+8801748304422', 'reynold.walker@yahoo.com', 'Destany Stravenue', 'Robertoberg', 'Georgia', '18454', 'Reunion', 'www.hill.com', '87085', 1, '2022-09-29 13:33:01', '2022-09-29 13:33:01'),
(2, 2, 'Littel-Bednar', '+8801060171516', 'jconroy@leuschke.com', 'Halvorson Extensions', 'Port Marvinside', 'Missouri', '05485', 'Swaziland', 'www.runolfsdottir.com', '81445', 1, '2022-09-29 13:33:02', '2022-09-29 13:33:02'),
(3, 3, 'Fisher, Hermiston and Goodwin', '+8801098846808', 'isai01@hotmail.com', 'Wiegand Keys', 'North Guadalupefort', 'New Mexico', '67167', 'Bahrain', 'www.dietrich.com', '85720', 1, '2022-09-29 13:33:02', '2022-09-29 13:33:02'),
(4, 3, 'Harber-Marvin', '+8801958418214', 'mckenzie.garnett@yahoo.com', 'Maribel Street', 'Wunschmouth', 'District of Columbia', '14361', 'Libyan Arab Jamahiriya', 'www.weimann.info', '11286', 1, '2022-09-29 13:33:02', '2022-09-29 13:33:02'),
(5, 1, 'Parisian-Littel', '+8801679075344', 'brakus.rolando@yahoo.com', 'Krajcik Isle', 'Stracketon', 'Kentucky', '20685', 'Jamaica', 'www.rau.com', '24214', 1, '2022-09-29 13:33:03', '2022-09-29 13:33:03'),
(6, 4, 'Gleason-Satterfield', '+8801414448042', 'mayert.jed@price.info', 'Stracke Street', 'Schaeferborough', 'Arkansas', '13835', 'Western Sahara', 'www.windler.com', '80395', 1, '2022-09-29 13:33:03', '2022-09-29 13:33:03'),
(7, 3, 'Bauch PLC', '+8801360545053', 'powlowski.stella@vonrueden.com', 'Pattie Parks', 'South Jessicamouth', 'Georgia', '96396', 'Bahamas', 'www.padberg.org', '78804', 1, '2022-09-29 13:33:03', '2022-09-29 13:33:03'),
(8, 1, 'Jones Ltd', '+8801902666311', 'itorphy@hotmail.com', 'Trantow Dale', 'Susieside', 'Tennessee', '55649-4348', 'Cape Verde', 'www.rowe.net', '22183', 1, '2022-09-29 13:33:04', '2022-09-29 13:33:04'),
(9, 3, 'Bergnaum, Effertz and Tremblay', '+8801118378197', 'kmclaughlin@hotmail.com', 'Ewell Inlet', 'Lake Heidi', 'Illinois', '89652-0221', 'Malta', 'www.smith.com', '54466', 1, '2022-09-29 13:33:04', '2022-09-29 13:33:04'),
(10, 2, 'Powlowski Ltd', '+8801306703664', 'zjones@yahoo.com', 'Camila Lock', 'Port Brennon', 'Colorado', '95464', 'Malta', 'www.gorczany.net', '46942', 1, '2022-09-29 13:33:04', '2022-09-29 13:33:04'),
(11, 3, 'Williamson-Abbott', '+8801107859796', 'wschmeler@schuppe.com', 'Rempel Plaza', 'South Amani', 'New Jersey', '98051-3145', 'Saint Lucia', 'www.bernhard.com', '23063', 1, '2022-09-29 13:33:05', '2022-09-29 13:33:05'),
(12, 1, 'Runte-Flatley', '+8801193886817', 'caesar35@champlin.info', 'Pfannerstill Station', 'Walkertown', 'Washington', '28778-2248', 'Cambodia', 'www.streich.info', '24049', 1, '2022-09-29 13:33:05', '2022-09-29 13:33:05'),
(13, 2, 'Donnelly PLC', '+8801760832550', 'arturo57@yahoo.com', 'Hudson Station', 'Ernestofort', 'Wyoming', '72497-5918', 'Spain', 'www.raynor.info', '27100', 1, '2022-09-29 13:33:05', '2022-09-29 13:33:05'),
(14, 1, 'Murray Ltd', '+8801118721825', 'luther.schultz@altenwerth.com', 'Stuart Extension', 'Kellenville', 'Washington', '17267-3208', 'Yemen', 'www.pollich.com', '22039', 1, '2022-09-29 13:33:06', '2022-09-29 13:33:06'),
(15, 4, 'Armstrong, Langworth and Kulas', '+8801045176433', 'patience.hoppe@yahoo.com', 'Baylee Dale', 'Janetchester', 'Georgia', '28613', 'Grenada', 'www.mcclure.net', '42850', 1, '2022-09-29 13:33:06', '2022-09-29 13:33:06'),
(16, 2, 'Hermiston, Konopelski and McCullough', '+8801937617055', 'corwin.maybelle@boehm.com', 'Natalia Manor', 'Erichhaven', 'Indiana', '17626-2396', 'Mali', 'www.kiehn.net', '89373', 1, '2022-09-29 13:33:06', '2022-09-29 13:33:06'),
(17, 3, 'Thiel LLC', '+8801361882334', 'angel89@balistreri.com', 'Nils Corner', 'North Dakota', 'Florida', '04985-9893', 'Costa Rica', 'www.cartwright.info', '23291', 1, '2022-09-29 13:33:06', '2022-09-29 13:33:06'),
(18, 1, 'Bartell PLC', '+8801260784383', 'emmalee.rodriguez@yahoo.com', 'Donnelly Cliff', 'East Nickolas', 'Arizona', '56943', 'Uruguay', 'www.kihn.org', '23781', 1, '2022-09-29 13:33:06', '2022-09-29 13:33:06'),
(19, 1, 'Padberg, Beatty and Lemke', '+8801626824521', 'abbey48@gmail.com', 'Mavis Mission', 'North Tylershire', 'Hawaii', '76177', 'Tokelau', 'www.wintheiser.com', '56779', 1, '2022-09-29 13:33:07', '2022-09-29 13:33:07'),
(20, 1, 'Bernier PLC', '+8801871458396', 'roberto.kihn@hotmail.com', 'Francesca Common', 'Carrollport', 'California', '30255-5451', 'United States of America', 'www.wolf.com', '85679', 1, '2022-09-29 13:33:07', '2022-09-29 13:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `client_contacts`
--

CREATE TABLE `client_contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_primary_contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `is_login_access` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `client_groups`
--

CREATE TABLE `client_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_groups`
--

INSERT INTO `client_groups` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'General', '', NULL, NULL),
(2, 'VIP', '', NULL, NULL),
(3, 'Low Budget', '', NULL, NULL),
(4, 'High Budget', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_projects`
--

CREATE TABLE `client_projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` bigint(20) DEFAULT 0,
  `project_status_id` bigint(20) UNSIGNED NOT NULL,
  `estimate_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `client_project_tags`
--

CREATE TABLE `client_project_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `estimated_items`
--

CREATE TABLE `estimated_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estimate_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) DEFAULT 1,
  `rate` int(11) DEFAULT 0,
  `discount` bigint(20) DEFAULT 0,
  `discount_type` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `estimates`
--

CREATE TABLE `estimates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` bigint(20) DEFAULT 0,
  `example_link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `discount` int(11) DEFAULT 0,
  `discount_type` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `billdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duedate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_value` bigint(20) DEFAULT 0,
  `payment_received` bigint(20) DEFAULT 0,
  `discount` bigint(20) DEFAULT 0,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) DEFAULT 1,
  `rate` int(11) DEFAULT 0,
  `discount` bigint(20) DEFAULT 0,
  `discount_type` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_type_id` bigint(20) UNSIGNED NOT NULL,
  `rate` int(11) DEFAULT 0,
  `discount` bigint(20) DEFAULT 0,
  `discount_type` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `payment_method_id` bigint(20) UNSIGNED NOT NULL,
  `amount` bigint(20) DEFAULT 1,
  `paymentdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cash', 1, NULL, NULL),
(2, 'Card', 1, NULL, NULL),
(3, 'Cheque', 1, NULL, NULL),
(4, 'bKash', 1, NULL, NULL),
(5, 'Rocket', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_members`
--

CREATE TABLE `project_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_statuses`
--

CREATE TABLE `project_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_statuses`
--

INSERT INTO `project_statuses` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Open', NULL, NULL),
(2, 'Completed', NULL, NULL),
(3, 'Hold', NULL, NULL),
(4, 'Canceled', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_tasks`
--

CREATE TABLE `project_tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `task_status_id` bigint(20) UNSIGNED NOT NULL,
  `startdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deadline` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `task_statuses`
--

CREATE TABLE `task_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colorcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_statuses`
--

INSERT INTO `task_statuses` (`id`, `title`, `colorcode`, `created_at`, `updated_at`) VALUES
(1, 'To do', '#F9A52D', NULL, NULL),
(2, 'In progress', '#1672B9', NULL, NULL),
(3, 'Testing', '#ad159e', NULL, NULL),
(4, 'Done', '#00B393', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `title`, `percentage`, `created_at`, `updated_at`) VALUES
(1, 'Tax(10%)', 10, NULL, NULL),
(2, 'Tax(5%)', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `unit_types`
--

CREATE TABLE `unit_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_contacts`
--
ALTER TABLE `client_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_groups`
--
ALTER TABLE `client_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_projects`
--
ALTER TABLE `client_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_project_tags`
--
ALTER TABLE `client_project_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimated_items`
--
ALTER TABLE `estimated_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimates`
--
ALTER TABLE `estimates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_members`
--
ALTER TABLE `project_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_statuses`
--
ALTER TABLE `project_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_tasks`
--
ALTER TABLE `project_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_statuses`
--
ALTER TABLE `task_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_types`
--
ALTER TABLE `unit_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `client_contacts`
--
ALTER TABLE `client_contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_groups`
--
ALTER TABLE `client_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `client_projects`
--
ALTER TABLE `client_projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_project_tags`
--
ALTER TABLE `client_project_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estimated_items`
--
ALTER TABLE `estimated_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `estimates`
--
ALTER TABLE `estimates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `project_members`
--
ALTER TABLE `project_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_statuses`
--
ALTER TABLE `project_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project_tasks`
--
ALTER TABLE `project_tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `task_statuses`
--
ALTER TABLE `task_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `unit_types`
--
ALTER TABLE `unit_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
