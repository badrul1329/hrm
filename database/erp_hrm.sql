-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2022 at 09:37 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp_hrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `allowances`
--

CREATE TABLE `allowances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `allowances`
--

INSERT INTO `allowances` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'House Rent', 0, NULL, NULL),
(2, 'Medical', 0, NULL, NULL),
(3, 'Educational', 0, NULL, NULL),
(4, 'Meal', 1, NULL, NULL),
(5, 'Transportation', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checkin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `employee_id`, `date`, `checkin`, `checkout`, `created_at`, `updated_at`) VALUES
(1, 1, '2022-09-29', '16:12:52', '16:13:36', '2022-09-29 10:12:52', '2022-09-29 10:13:36'),
(2, 2, '2022-09-29', '16:13:44', NULL, '2022-09-29 10:13:44', '2022-09-29 10:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'Yostton', 'Eveniet consequuntur sequi velit quia dolorem sit sunt. Officiis minima alias rerum explicabo. Blanditiis est fugit modi sunt harum. Blanditiis repellat sit non vero quidem voluptates blanditiis et.', '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(2, 'Lake Anselchester', 'Culpa consequatur et excepturi. Vel est aperiam unde rerum enim natus. Provident rerum officiis suscipit eum.', '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(3, 'Bechtelartown', 'Et qui illo et sunt dignissimos. Vel pariatur optio impedit in illo voluptate magnam. Molestiae at laudantium culpa cumque. Omnis quo non est excepturi fugit. Molestiae aliquam id sed reprehenderit.', '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(4, 'West Harmonshire', 'Amet reiciendis corrupti esse ea iure harum eum. Aut sint sunt sed voluptas sit illo. Quae et reprehenderit molestiae perspiciatis. Quia perspiciatis est quo deleniti.', '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(5, 'Wizastad', 'Neque excepturi et recusandae ex mollitia. Culpa aperiam quisquam eos qui ipsum minus. Ut ad quia quibusdam voluptatem. Nemo voluptate officiis quis impedit et.', '2022-09-28 23:40:01', '2022-09-28 23:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `dept_id`, `created_at`, `updated_at`) VALUES
(1, 'Travel Guide', 4, '2022-09-28 23:39:58', '2022-09-28 23:39:58'),
(2, 'Gas Plant Operator', 2, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(3, 'Aircraft Body Repairer', 5, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(4, 'Advertising Sales Agent', 5, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(5, 'Automotive Specialty Technician', 1, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(6, 'Sales and Related Workers', 3, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(7, 'Statistical Assistant', 1, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(8, 'Marine Engineer', 5, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(9, 'Management Analyst', 5, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(10, 'Elementary and Secondary School Administrators', 2, '2022-09-28 23:40:00', '2022-09-28 23:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT 0,
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_id` bigint(20) UNSIGNED NOT NULL,
  `religion_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `phone`, `photo`, `gender`, `birthday`, `marital_id`, `religion_id`, `created_at`, `updated_at`) VALUES
(1, 'Gillian Skiles', 'dicki.curt@gmail.com', '+1-949-559-1834', NULL, 1, '1999-09-09', 2, 1, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(2, 'Marilou Heaney V', 'walker.tyra@gmail.com', '856-444-9931', NULL, 0, '1987-11-11', 2, 4, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(3, 'Dr. Ronaldo Spencer', 'rosalia85@hotmail.com', '+1-234-554-0025', NULL, 0, '1970-11-26', 2, 2, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(4, 'Miss Maxie Pfeffer', 'willard.baumbach@gmail.com', '+1.575.539.7626', NULL, 1, '2016-07-09', 5, 1, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(5, 'Ms. Christine Murazik', 'maryjane.white@kuhlman.com', '+1-443-617-9678', NULL, 0, '1994-11-25', 3, 2, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(6, 'Prof. Eric Gulgowski', 'selina65@simonis.com', '+1-480-328-6910', NULL, 1, '1970-07-10', 1, 4, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(7, 'Dimitri Torphy', 'zrolfson@hermiston.biz', '1-424-460-2532', NULL, 1, '1981-05-19', 3, 2, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(8, 'Tiara Balistreri', 'oleannon@johnson.com', '(410) 756-0333', NULL, 0, '2018-12-28', 4, 3, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(9, 'Dr. Vivien Keeling', 'karley.hauck@schulist.com', '+1 (830) 543-1330', NULL, 1, '1994-04-23', 5, 2, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(10, 'Burnice Waelchi', 'bryce56@rice.net', '1-936-893-2252', NULL, 0, '1982-12-05', 4, 2, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(11, 'hello', 'hello@gmail.com', '123456789', NULL, 0, '2022-09-29', 1, 1, '2022-09-29 00:37:15', '2022-09-29 00:37:15');

-- --------------------------------------------------------

--
-- Table structure for table `employee_addresses`
--

CREATE TABLE `employee_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_addresses`
--

INSERT INTO `employee_addresses` (`id`, `employee_id`, `street`, `city`, `district`, `zip_code`, `country`, `created_at`, `updated_at`) VALUES
(1, 1, '4875 Eichmann Run', 'East Gay', 'Lake Rosaleeside', '832', 'Bhutan', '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(2, 2, '9221 Hermann Plains Apt. 688', 'Kubport', 'East Miller', '822', 'Faroe Islands', '2022-09-28 23:40:13', '2022-09-28 23:40:13'),
(3, 3, '37798 Goodwin Islands Suite 697', 'East Oswaldostad', 'Joeyfurt', '11', 'Georgia', '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(4, 4, '6105 Halvorson Camp Apt. 845', 'Paytonstad', 'Port Kailee', '861', 'Pakistan', '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(5, 5, '2954 Concepcion Unions', 'Brannonland', 'South Aimee', '661', 'Cameroon', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(6, 6, '14644 Gislason Prairie', 'Smithammouth', 'East Hilbert', '190', 'Armenia', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(7, 7, '945 Ankunding Mews Apt. 880', 'Lilystad', 'Rosenbaumside', '617', 'Oman', '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(8, 8, '93614 Westley Ford', 'Kuhnton', 'North Skyla', '852', 'Bahamas', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(9, 9, '202 Jaskolski Union Apt. 739', 'Port Jameson', 'Shieldsside', '143', 'Taiwan', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(10, 10, '69185 Karelle Square', 'South Zackary', 'Herminabury', '142', 'Andorra', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(11, 11, '1', 'dhaka', 'dhaka', '1234', 'Bangladesh', '2022-09-29 00:37:15', '2022-09-29 00:37:15');

-- --------------------------------------------------------

--
-- Table structure for table `employee_documents`
--

CREATE TABLE `employee_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_documents`
--

INSERT INTO `employee_documents` (`id`, `employee_id`, `title`, `file`, `created_at`, `updated_at`) VALUES
(1, 1, 'autem', 'c77b86c1-c19d-3f0f-9e2e-e0afeecaded8.docx', '2022-09-28 23:40:13', '2022-09-28 23:40:13'),
(2, 1, 'et', '86c99fbf-96c4-3b1c-9c23-9b0ee435802d.docx', '2022-09-28 23:40:13', '2022-09-28 23:40:13'),
(3, 1, 'eos', 'b9f978b6-5e1d-305d-80fd-ad1b92f81e1e.docx', '2022-09-28 23:40:13', '2022-09-28 23:40:13'),
(4, 2, 'molestiae', 'bf2792f7-9496-3437-9896-8904cea50049.docx', '2022-09-28 23:40:14', '2022-09-28 23:40:14'),
(5, 2, 'doloribus', '99a1e292-2303-3fbb-a002-d8c7d78d6ab1.pdf', '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(6, 2, 'earum', 'c5a2222b-b986-3813-a438-d7eb4b167626.jpg', '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(7, 3, 'recusandae', 'fe0d9c45-6057-312d-8e92-44c15587126d.jpg', '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(8, 3, 'distinctio', 'f890a10f-29bd-38a2-b9af-5766700d58ba.pdf', '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(9, 3, 'exercitationem', '9a377ee3-457a-301c-ac30-a3a01d0c09a0.docx', '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(10, 4, 'veniam', '5da3819c-a25c-3cd6-ac96-f23db65dc924.pdf', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(11, 4, 'eveniet', '797ffcca-fe6b-3869-897e-9c55ca6004c9.pdf', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(12, 4, 'consequuntur', '4cec4925-a04f-3225-aabe-a61f666a82f4.docx', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(13, 5, 'assumenda', '6b4f875a-3592-3e25-bff9-e8f05454d3b7.pdf', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(14, 5, 'a', '99186e09-53b8-3829-a292-cbfccbb7d15f.jpg', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(15, 5, 'iure', '1b021280-cac8-32fa-93e8-13d50ddc0bd0.jpg', '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(16, 6, 'nostrum', 'c39d1203-e665-3b6d-b70a-728bebbb949b.jpg', '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(17, 6, 'iusto', 'e58ab520-0f56-302c-bf8a-fc731aaaf7b3.jpg', '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(18, 6, 'aut', 'e7931c93-27c4-3ab7-a2e4-4e5c438ba863.jpg', '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(19, 7, 'consectetur', 'f64e6391-6b30-3f99-9e7e-d1f5b551ed51.jpg', '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(20, 7, 'voluptatum', 'dc7510f5-abbd-3a6f-b63c-2a541eb09d37.docx', '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(21, 7, 'voluptas', 'c612c6ab-46bb-381f-a8cc-921297049eff.docx', '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(22, 8, 'velit', '76eeeca9-2727-3a32-939a-d0557e1ec896.docx', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(23, 8, 'incidunt', '61787f6d-4b8e-332d-94dc-406433913f66.pdf', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(24, 8, 'ipsam', '8f7ce65b-0030-3d67-97cf-cde00241ec87.jpg', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(25, 9, 'dolorum', 'e54d6111-8c10-3798-bf09-6cca345025b2.pdf', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(26, 9, 'omnis', '9fe7ffc9-c0b7-365e-be98-be00fb7aa73e.pdf', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(27, 9, 'necessitatibus', '0fd82a21-f04d-3ce2-a394-0f537db7e9ee.jpg', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(28, 10, 'et', '4185b32d-ead8-3206-bbc1-9d2e4edfec36.docx', '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(29, 10, 'nam', '9b66b667-678a-3edc-b594-3ee321dc9dbe.jpg', '2022-09-28 23:40:19', '2022-09-28 23:40:19'),
(30, 10, 'consequatur', '9f2afbd2-1d0b-3fb8-969c-2dbc3c0c5991.pdf', '2022-09-28 23:40:19', '2022-09-28 23:40:19'),
(31, 11, 'sdgsgdsg', '1664433436.1245.py', '2022-09-29 00:37:16', '2022-09-29 00:37:16');

-- --------------------------------------------------------

--
-- Table structure for table `employee_officials`
--

CREATE TABLE `employee_officials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `division` bigint(20) UNSIGNED NOT NULL,
  `position` bigint(20) UNSIGNED NOT NULL,
  `job_type` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor` bigint(20) UNSIGNED DEFAULT NULL,
  `shift` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_officials`
--

INSERT INTO `employee_officials` (`id`, `employee_id`, `division`, `position`, `job_type`, `from`, `to`, `supervisor`, `shift`, `grade_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 4, 1, '1996-04-08', '1996-08-08', 3, 4, 1, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(2, 2, 8, 6, 3, '1984-12-15', NULL, 7, 1, 7, '2022-09-28 23:40:14', '2022-09-28 23:40:14'),
(3, 3, 6, 3, 3, '2004-12-16', '2005-05-16', 8, 4, 2, '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(4, 4, 4, 1, 1, '2021-03-04', NULL, 1, 4, 9, '2022-09-28 23:40:15', '2022-09-28 23:40:15'),
(5, 5, 2, 10, 3, '2017-01-22', NULL, 8, 5, 8, '2022-09-28 23:40:16', '2022-09-28 23:40:16'),
(6, 6, 5, 7, 3, '1974-05-16', NULL, 8, 3, 4, '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(7, 7, 3, 10, 1, '1996-06-24', NULL, 7, 4, 2, '2022-09-28 23:40:17', '2022-09-28 23:40:17'),
(8, 8, 9, 10, 2, '1999-11-21', NULL, 8, 5, 9, '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(9, 9, 6, 8, 3, '2010-04-07', NULL, 7, 3, 6, '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(10, 10, 4, 3, 2, '1999-07-04', '1999-08-04', 3, 3, 4, '2022-09-28 23:40:18', '2022-09-28 23:40:18'),
(11, 11, 2, 2, 1, '2022-01-29', '2022-09-29', 3, 3, 4, '2022-09-29 00:37:16', '2022-09-29 00:37:16');

-- --------------------------------------------------------

--
-- Table structure for table `employee_reschedules`
--

CREATE TABLE `employee_reschedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `shift` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_workshops`
--

CREATE TABLE `employee_workshops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `workshop_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `rate` int(11) NOT NULL DEFAULT 0,
  `overtime_rate` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `type`, `rate`, `overtime_rate`, `created_at`, `updated_at`) VALUES
(1, 'quae', 1, 188, 746, '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(2, 'ut', 1, 698, 580, '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(3, 'aut', 1, 805, 816, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(4, 'consectetur', 0, 639, 476, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(5, 'et', 0, 301, 871, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(6, 'dolor', 0, 12, 340, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(7, 'eaque', 0, 887, 224, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(8, 'quos', 1, 860, 409, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(9, 'perspiciatis', 0, 423, 910, '2022-09-28 23:40:06', '2022-09-28 23:40:06'),
(10, 'qui', 1, 119, 489, '2022-09-28 23:40:06', '2022-09-28 23:40:06');

-- --------------------------------------------------------

--
-- Table structure for table `grade_allowances`
--

CREATE TABLE `grade_allowances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED NOT NULL,
  `allowance_id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `rate` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade_allowances`
--

INSERT INTO `grade_allowances` (`id`, `grade_id`, `allowance_id`, `type`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 1, 2, '2022-09-28 23:40:06', '2022-09-28 23:40:06'),
(2, 1, 1, 0, 4, '2022-09-28 23:40:07', '2022-09-28 23:40:07'),
(3, 1, 3, 1, 8, '2022-09-28 23:40:07', '2022-09-28 23:40:07'),
(4, 2, 5, 0, 9, '2022-09-28 23:40:07', '2022-09-28 23:40:07'),
(5, 2, 2, 1, 8, '2022-09-28 23:40:07', '2022-09-28 23:40:07'),
(6, 2, 4, 1, 6, '2022-09-28 23:40:07', '2022-09-28 23:40:07'),
(7, 3, 4, 0, 7, '2022-09-28 23:40:07', '2022-09-28 23:40:07'),
(8, 3, 4, 0, 5, '2022-09-28 23:40:08', '2022-09-28 23:40:08'),
(9, 3, 5, 0, 5, '2022-09-28 23:40:08', '2022-09-28 23:40:08'),
(10, 4, 1, 1, 7, '2022-09-28 23:40:08', '2022-09-28 23:40:08'),
(11, 4, 1, 0, 8, '2022-09-28 23:40:08', '2022-09-28 23:40:08'),
(12, 4, 4, 1, 3, '2022-09-28 23:40:08', '2022-09-28 23:40:08'),
(13, 5, 2, 0, 1, '2022-09-28 23:40:08', '2022-09-28 23:40:08'),
(14, 5, 5, 1, 2, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(15, 5, 4, 0, 7, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(16, 6, 5, 0, 5, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(17, 6, 5, 1, 8, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(18, 6, 2, 1, 8, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(19, 7, 1, 1, 8, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(20, 7, 2, 1, 2, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(21, 7, 5, 1, 7, '2022-09-28 23:40:09', '2022-09-28 23:40:09'),
(22, 8, 5, 1, 2, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(23, 8, 5, 0, 6, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(24, 8, 3, 1, 4, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(25, 9, 3, 0, 8, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(26, 9, 5, 1, 8, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(27, 9, 3, 1, 9, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(28, 10, 3, 0, 2, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(29, 10, 3, 1, 7, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(30, 10, 2, 0, 4, '2022-09-28 23:40:10', '2022-09-28 23:40:10');

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE `job_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Full time', NULL, NULL),
(2, 'Part Time', NULL, NULL),
(3, 'Contractual', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_applications`
--

CREATE TABLE `leave_applications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `applied_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_by` bigint(20) UNSIGNED DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_documents`
--

CREATE TABLE `leave_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `application_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

CREATE TABLE `leave_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `days` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`id`, `name`, `days`, `created_at`, `updated_at`) VALUES
(1, 'Maternity', 20, NULL, NULL),
(2, 'Sickness', 10, NULL, NULL),
(3, 'Others', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `approved_by` bigint(20) UNSIGNED DEFAULT NULL,
  `approved_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `repayment_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `repayment_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `installment` int(11) NOT NULL DEFAULT 1,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `loan_installments`
--

CREATE TABLE `loan_installments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `installment_amount` int(11) NOT NULL DEFAULT 0,
  `paid` int(11) NOT NULL DEFAULT 0,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `received_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'Securities Sales Agent', 'Expedita vitae odio sit deleniti consequuntur sint eos accusamus. Eum eum nesciunt et et sint. Exercitationem ratione quos aspernatur et ut voluptatum. Molestias excepturi voluptatum molestiae sint.', '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(2, 'Pastry Chef', 'A iste quo omnis quia. Ut odio iusto molestiae nesciunt et. Alias quia sed sunt eos id harum. Alias eius veritatis et.', '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(3, 'Surveying Technician', 'Sit minus dicta qui ut adipisci quas. Totam repellendus est facilis. Voluptas veritatis voluptas quia hic nemo debitis.', '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(4, 'Soil Scientist', 'Impedit aut ea suscipit ab ut odio consectetur. Vel aut voluptates adipisci voluptate quod harum illo. Nulla eum accusantium modi cumque debitis. Cupiditate accusamus eaque aut qui.', '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(5, 'Board Of Directors', 'Ratione nemo nemo expedita commodi repellendus sunt ut. Sequi illum maxime illum quod harum. Hic dolor dolores quisquam dolorum autem ex dicta.', '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(6, 'Office Machine and Cash Register Servicer', 'Qui ipsa aliquid rem. Quis dolorem sapiente iure assumenda dolore. Dolorum iste magnam voluptas saepe aut voluptatem iure. Ut dolore magni est unde velit.', '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(7, 'Drilling and Boring Machine Tool Setter', 'Quam ea ut reiciendis itaque. Voluptatibus quas quia aut magnam dolorum quo est. Rerum eum dolorem iste molestias sunt. Rerum minima nihil ipsa a animi suscipit animi maiores.', '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(8, 'General Farmworker', 'Voluptatibus architecto neque nulla perferendis. Ea quis inventore asperiores libero omnis.', '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(9, 'Plate Finisher', 'Quam assumenda ut eligendi et. Numquam ipsum placeat et ut tenetur. Amet officiis nobis eos alias error amet.', '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(10, 'Private Sector Executive', 'Libero alias perferendis perspiciatis debitis. Ullam saepe at debitis eligendi asperiores quo. Eum commodi natus repellat et vitae sed. Quaerat ut doloribus itaque libero odit voluptatum.', '2022-09-28 23:40:04', '2022-09-28 23:40:04');

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `generated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `paid_by` bigint(20) UNSIGNED DEFAULT NULL,
  `paid_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_allowances`
--

CREATE TABLE `salary_allowances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `salary_id` bigint(20) UNSIGNED NOT NULL,
  `allowance_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `rate` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_details`
--

CREATE TABLE `salary_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `salary_id` bigint(20) UNSIGNED NOT NULL,
  `job_type` bigint(20) UNSIGNED DEFAULT NULL,
  `shift` bigint(20) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `basic` double(8,2) NOT NULL DEFAULT 0.00,
  `overtime` double(8,2) NOT NULL DEFAULT 0.00,
  `working_hour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_working_hour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `working_day` int(11) NOT NULL,
  `total_working_day` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `overtime_hour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overtime_day` int(11) NOT NULL,
  `overtime_rate` int(11) NOT NULL,
  `leave` int(11) NOT NULL,
  `loan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shift_schedules`
--

CREATE TABLE `shift_schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shift_schedules`
--

INSERT INTO `shift_schedules` (`id`, `name`, `from`, `to`, `status`, `created_at`, `updated_at`) VALUES
(1, 'placeat', '04:13', '10:25', 1, '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(2, 'ducimus', '22:41', '07:25', 1, '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(3, 'omnis', '15:35', '01:43', 1, '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(4, 'omnis', '07:21', '15:55', 1, '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(5, 'cupiditate', '19:31', '23:01', 1, '2022-09-28 23:40:02', '2022-09-28 23:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expertise` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_types`
--

CREATE TABLE `training_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `workshops`
--

CREATE TABLE `workshops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `trainer_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allowances`
--
ALTER TABLE `allowances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_phone_unique` (`phone`),
  ADD KEY `employees_marital_id_foreign` (`marital_id`),
  ADD KEY `employees_religion_id_foreign` (`religion_id`);

--
-- Indexes for table `employee_addresses`
--
ALTER TABLE `employee_addresses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_addresses_employee_id_unique` (`employee_id`);

--
-- Indexes for table `employee_documents`
--
ALTER TABLE `employee_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_documents_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employee_officials`
--
ALTER TABLE `employee_officials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_officials_employee_id_unique` (`employee_id`),
  ADD KEY `employee_officials_division_foreign` (`division`),
  ADD KEY `employee_officials_position_foreign` (`position`),
  ADD KEY `employee_officials_job_type_foreign` (`job_type`),
  ADD KEY `employee_officials_supervisor_foreign` (`supervisor`),
  ADD KEY `employee_officials_shift_foreign` (`shift`),
  ADD KEY `employee_officials_grade_id_foreign` (`grade_id`);

--
-- Indexes for table `employee_reschedules`
--
ALTER TABLE `employee_reschedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_reschedules_employee_id_foreign` (`employee_id`),
  ADD KEY `employee_reschedules_shift_foreign` (`shift`);

--
-- Indexes for table `employee_workshops`
--
ALTER TABLE `employee_workshops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_workshops_employee_id_foreign` (`employee_id`),
  ADD KEY `employee_workshops_workshop_id_foreign` (`workshop_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `grades_name_unique` (`name`);

--
-- Indexes for table `grade_allowances`
--
ALTER TABLE `grade_allowances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade_allowances_grade_id_foreign` (`grade_id`),
  ADD KEY `grade_allowances_allowance_id_foreign` (`allowance_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_types`
--
ALTER TABLE `job_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_applications`
--
ALTER TABLE `leave_applications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_applications_employee_id_foreign` (`employee_id`),
  ADD KEY `leave_applications_type_id_foreign` (`type_id`),
  ADD KEY `leave_applications_approved_by_foreign` (`approved_by`);

--
-- Indexes for table `leave_documents`
--
ALTER TABLE `leave_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_documents_application_id_foreign` (`application_id`);

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loans_employee_id_foreign` (`employee_id`),
  ADD KEY `loans_approved_by_foreign` (`approved_by`);

--
-- Indexes for table `loan_installments`
--
ALTER TABLE `loan_installments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loan_installments_employee_id_foreign` (`employee_id`),
  ADD KEY `loan_installments_received_by_foreign` (`received_by`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `salaries_employee_id_foreign` (`employee_id`),
  ADD KEY `salaries_generated_by_foreign` (`generated_by`),
  ADD KEY `salaries_paid_by_foreign` (`paid_by`);

--
-- Indexes for table `salary_allowances`
--
ALTER TABLE `salary_allowances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `salary_allowances_salary_id_foreign` (`salary_id`),
  ADD KEY `salary_allowances_allowance_id_foreign` (`allowance_id`);

--
-- Indexes for table `salary_details`
--
ALTER TABLE `salary_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `salary_details_salary_id_foreign` (`salary_id`),
  ADD KEY `salary_details_job_type_foreign` (`job_type`),
  ADD KEY `salary_details_shift_foreign` (`shift`);

--
-- Indexes for table `shift_schedules`
--
ALTER TABLE `shift_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_types`
--
ALTER TABLE `training_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workshops`
--
ALTER TABLE `workshops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workshops_type_id_foreign` (`type_id`),
  ADD KEY `workshops_trainer_id_foreign` (`trainer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allowances`
--
ALTER TABLE `allowances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `employee_addresses`
--
ALTER TABLE `employee_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `employee_documents`
--
ALTER TABLE `employee_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `employee_officials`
--
ALTER TABLE `employee_officials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `employee_reschedules`
--
ALTER TABLE `employee_reschedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_workshops`
--
ALTER TABLE `employee_workshops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `grade_allowances`
--
ALTER TABLE `grade_allowances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_types`
--
ALTER TABLE `job_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leave_applications`
--
ALTER TABLE `leave_applications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_documents`
--
ALTER TABLE `leave_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_types`
--
ALTER TABLE `leave_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_installments`
--
ALTER TABLE `loan_installments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_allowances`
--
ALTER TABLE `salary_allowances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_details`
--
ALTER TABLE `salary_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shift_schedules`
--
ALTER TABLE `shift_schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_types`
--
ALTER TABLE `training_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `workshops`
--
ALTER TABLE `workshops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_marital_id_foreign` FOREIGN KEY (`marital_id`) REFERENCES `erp`.`marital_infos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employees_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `erp`.`religions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_addresses`
--
ALTER TABLE `employee_addresses`
  ADD CONSTRAINT `employee_addresses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_documents`
--
ALTER TABLE `employee_documents`
  ADD CONSTRAINT `employee_documents_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_officials`
--
ALTER TABLE `employee_officials`
  ADD CONSTRAINT `employee_officials_division_foreign` FOREIGN KEY (`division`) REFERENCES `divisions` (`id`),
  ADD CONSTRAINT `employee_officials_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_officials_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `employee_officials_job_type_foreign` FOREIGN KEY (`job_type`) REFERENCES `job_types` (`id`),
  ADD CONSTRAINT `employee_officials_position_foreign` FOREIGN KEY (`position`) REFERENCES `positions` (`id`),
  ADD CONSTRAINT `employee_officials_shift_foreign` FOREIGN KEY (`shift`) REFERENCES `shift_schedules` (`id`),
  ADD CONSTRAINT `employee_officials_supervisor_foreign` FOREIGN KEY (`supervisor`) REFERENCES `employees` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `employee_reschedules`
--
ALTER TABLE `employee_reschedules`
  ADD CONSTRAINT `employee_reschedules_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_reschedules_shift_foreign` FOREIGN KEY (`shift`) REFERENCES `shift_schedules` (`id`);

--
-- Constraints for table `employee_workshops`
--
ALTER TABLE `employee_workshops`
  ADD CONSTRAINT `employee_workshops_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_workshops_workshop_id_foreign` FOREIGN KEY (`workshop_id`) REFERENCES `workshops` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `grade_allowances`
--
ALTER TABLE `grade_allowances`
  ADD CONSTRAINT `grade_allowances_allowance_id_foreign` FOREIGN KEY (`allowance_id`) REFERENCES `allowances` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grade_allowances_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `leave_applications`
--
ALTER TABLE `leave_applications`
  ADD CONSTRAINT `leave_applications_approved_by_foreign` FOREIGN KEY (`approved_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `leave_applications_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `leave_applications_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `leave_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `leave_documents`
--
ALTER TABLE `leave_documents`
  ADD CONSTRAINT `leave_documents_application_id_foreign` FOREIGN KEY (`application_id`) REFERENCES `leave_applications` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_approved_by_foreign` FOREIGN KEY (`approved_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `loans_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `loan_installments`
--
ALTER TABLE `loan_installments`
  ADD CONSTRAINT `loan_installments_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `loan_installments_received_by_foreign` FOREIGN KEY (`received_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `salaries`
--
ALTER TABLE `salaries`
  ADD CONSTRAINT `salaries_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `salaries_generated_by_foreign` FOREIGN KEY (`generated_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `salaries_paid_by_foreign` FOREIGN KEY (`paid_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `salary_allowances`
--
ALTER TABLE `salary_allowances`
  ADD CONSTRAINT `salary_allowances_allowance_id_foreign` FOREIGN KEY (`allowance_id`) REFERENCES `allowances` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `salary_allowances_salary_id_foreign` FOREIGN KEY (`salary_id`) REFERENCES `salaries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `salary_details`
--
ALTER TABLE `salary_details`
  ADD CONSTRAINT `salary_details_job_type_foreign` FOREIGN KEY (`job_type`) REFERENCES `job_types` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `salary_details_salary_id_foreign` FOREIGN KEY (`salary_id`) REFERENCES `salaries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `salary_details_shift_foreign` FOREIGN KEY (`shift`) REFERENCES `shift_schedules` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `workshops`
--
ALTER TABLE `workshops`
  ADD CONSTRAINT `workshops_trainer_id_foreign` FOREIGN KEY (`trainer_id`) REFERENCES `trainers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `workshops_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `training_types` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
