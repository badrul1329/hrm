<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Notice;
use App\Models\NoticeDocument;

class NoticesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Notice::factory()->count(15)->create()->each(function ($notice) {
            $notice->documents()->saveMany(NoticeDocument::factory()->count(rand(0,3))->make());
        });
    }
}
