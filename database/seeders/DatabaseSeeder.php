<?php
namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

		// $this->call(UsersTableSeeder::class);
		$this->call([
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            LanguagesTableSeeder::class,
            PhrasesTableSeeder::class,
            LanguagePhrasesTableSeeder::class,
            CurrenciesTableSeeder::class,
            SettingsTableSeeder::class,
            MaritalInfosTableSeeder::class,
            ReligionsTableSeeder::class,
        ]);
        Artisan::call('module:seed');
    }
}
