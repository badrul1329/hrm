-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2022 at 09:36 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `batch_uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `event`, `subject_id`, `causer_type`, `causer_id`, `properties`, `batch_uuid`, `created_at`, `updated_at`) VALUES
(1, 'default', 'User Daisy Zieme has been created', 'App\\Models\\User', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 13:04:46', '2022-09-28 13:04:46'),
(2, 'default', 'Division Healthcare Practitioner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:06:41', '2022-09-28 14:06:41'),
(3, 'default', 'Division Coating Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:06:41', '2022-09-28 14:06:41'),
(4, 'default', 'Division Welder and Cutter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(5, 'default', 'Division Production Control Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(6, 'default', 'Division Signal Repairer OR Track Switch Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(7, 'default', 'Division Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(8, 'default', 'Division Marriage and Family Therapist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(9, 'default', 'Division Stonemason has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(10, 'default', 'Division Environmental Science Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(11, 'default', 'Division Security Guard has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:06:42', '2022-09-28 14:06:42'),
(12, 'default', 'Division Recreation and Fitness Studies Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:07:42', '2022-09-28 14:07:42'),
(13, 'default', 'Division Nuclear Power Reactor Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:07:42', '2022-09-28 14:07:42'),
(14, 'default', 'Division Veterinarian has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:07:42', '2022-09-28 14:07:42'),
(15, 'default', 'Division Insurance Investigator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:07:42', '2022-09-28 14:07:42'),
(16, 'default', 'Division Agricultural Crop Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:07:42', '2022-09-28 14:07:42'),
(17, 'default', 'Division Mechanical Drafter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:07:42', '2022-09-28 14:07:42'),
(18, 'default', 'Division Personal Trainer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:07:43', '2022-09-28 14:07:43'),
(19, 'default', 'Division Religious Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:07:43', '2022-09-28 14:07:43'),
(20, 'default', 'Division Bellhop has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:07:43', '2022-09-28 14:07:43'),
(21, 'default', 'Division Pipe Fitter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:07:43', '2022-09-28 14:07:43'),
(22, 'default', 'Division Plating Operator OR Coating Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 21, NULL, NULL, '[]', NULL, '2022-09-28 14:09:00', '2022-09-28 14:09:00'),
(23, 'default', 'Division Grinder OR Polisher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 22, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(24, 'default', 'Division Political Scientist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 23, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(25, 'default', 'Division Credit Analyst has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 24, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(26, 'default', 'Division Gluing Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 25, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(27, 'default', 'Division Farm and Home Management Advisor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 26, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(28, 'default', 'Division Railroad Conductors has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 27, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(29, 'default', 'Division Forestry Conservation Science Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 28, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(30, 'default', 'Division Epidemiologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 29, NULL, NULL, '[]', NULL, '2022-09-28 14:09:01', '2022-09-28 14:09:01'),
(31, 'default', 'Division Service Station Attendant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 30, NULL, NULL, '[]', NULL, '2022-09-28 14:09:02', '2022-09-28 14:09:02'),
(32, 'default', 'Division Coroner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 31, NULL, NULL, '[]', NULL, '2022-09-28 14:09:02', '2022-09-28 14:09:02'),
(33, 'default', 'Division Algorithm Developer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 32, NULL, NULL, '[]', NULL, '2022-09-28 14:09:02', '2022-09-28 14:09:02'),
(34, 'default', 'Division Business Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 33, NULL, NULL, '[]', NULL, '2022-09-28 14:09:02', '2022-09-28 14:09:02'),
(35, 'default', 'Division Self-Enrichment Education Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 34, NULL, NULL, '[]', NULL, '2022-09-28 14:09:02', '2022-09-28 14:09:02'),
(36, 'default', 'Division Plating Operator OR Coating Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 35, NULL, NULL, '[]', NULL, '2022-09-28 14:09:02', '2022-09-28 14:09:02'),
(37, 'default', 'Division Electrotyper has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 36, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(38, 'default', 'Division Motor Vehicle Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 37, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(39, 'default', 'Division Statement Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 38, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(40, 'default', 'Division Outdoor Power Equipment Mechanic has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 39, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(41, 'default', 'Division Surveying and Mapping Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 40, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(42, 'default', 'Division Cartoonist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 41, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(43, 'default', 'Division Public Transportation Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 42, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(44, 'default', 'Division Community Service Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 43, NULL, NULL, '[]', NULL, '2022-09-28 14:09:48', '2022-09-28 14:09:48'),
(45, 'default', 'Division Refractory Materials Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 44, NULL, NULL, '[]', NULL, '2022-09-28 14:09:49', '2022-09-28 14:09:49'),
(46, 'default', 'Division Chemical Equipment Controller has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 45, NULL, NULL, '[]', NULL, '2022-09-28 14:09:49', '2022-09-28 14:09:49'),
(47, 'default', 'Division Laundry OR Dry-Cleaning Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 46, NULL, NULL, '[]', NULL, '2022-09-28 14:09:49', '2022-09-28 14:09:49'),
(48, 'default', 'Division Vocational Education Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 47, NULL, NULL, '[]', NULL, '2022-09-28 14:09:49', '2022-09-28 14:09:49'),
(49, 'default', 'Division Maid has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 48, NULL, NULL, '[]', NULL, '2022-09-28 14:09:50', '2022-09-28 14:09:50'),
(50, 'default', 'Division Aircraft Body Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 49, NULL, NULL, '[]', NULL, '2022-09-28 14:09:50', '2022-09-28 14:09:50'),
(51, 'default', 'Division Plumber has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 50, NULL, NULL, '[]', NULL, '2022-09-28 14:09:50', '2022-09-28 14:09:50'),
(52, 'default', 'Division Washing Equipment Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 51, NULL, NULL, '[]', NULL, '2022-09-28 14:09:50', '2022-09-28 14:09:50'),
(53, 'default', 'Division Avionics Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 52, NULL, NULL, '[]', NULL, '2022-09-28 14:09:50', '2022-09-28 14:09:50'),
(54, 'default', 'Division Transportation Equipment Maintenance has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 53, NULL, NULL, '[]', NULL, '2022-09-28 14:09:50', '2022-09-28 14:09:50'),
(55, 'default', 'Division Deburring Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 54, NULL, NULL, '[]', NULL, '2022-09-28 14:09:51', '2022-09-28 14:09:51'),
(56, 'default', 'Division Nuclear Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 55, NULL, NULL, '[]', NULL, '2022-09-28 14:09:51', '2022-09-28 14:09:51'),
(57, 'default', 'Division Mining Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 56, NULL, NULL, '[]', NULL, '2022-09-28 14:11:00', '2022-09-28 14:11:00'),
(58, 'default', 'Division Maid has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 57, NULL, NULL, '[]', NULL, '2022-09-28 14:11:00', '2022-09-28 14:11:00'),
(59, 'default', 'Division Compacting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 58, NULL, NULL, '[]', NULL, '2022-09-28 14:11:00', '2022-09-28 14:11:00'),
(60, 'default', 'Division Title Searcher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 59, NULL, NULL, '[]', NULL, '2022-09-28 14:11:00', '2022-09-28 14:11:00'),
(61, 'default', 'Division Aircraft Assembler has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 60, NULL, NULL, '[]', NULL, '2022-09-28 14:11:00', '2022-09-28 14:11:00'),
(62, 'default', 'Division Correspondence Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 61, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(63, 'default', 'Division Answering Service has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 62, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(64, 'default', 'Division Aircraft Assembler has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 63, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(65, 'default', 'Division Hotel Desk Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 64, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(66, 'default', 'Division Immigration Inspector OR Customs Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 65, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(67, 'default', 'Division HR Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 66, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(68, 'default', 'Division Fire Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 67, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(69, 'default', 'Division Management Analyst has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 68, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(70, 'default', 'Division Driver-Sales Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 69, NULL, NULL, '[]', NULL, '2022-09-28 14:11:01', '2022-09-28 14:11:01'),
(71, 'default', 'Division Electronic Equipment Assembler has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 70, NULL, NULL, '[]', NULL, '2022-09-28 14:11:02', '2022-09-28 14:11:02'),
(72, 'default', 'Division Control Valve Installer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 71, NULL, NULL, '[]', NULL, '2022-09-28 14:11:02', '2022-09-28 14:11:02'),
(73, 'default', 'Division Real Estate Sales Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 72, NULL, NULL, '[]', NULL, '2022-09-28 14:11:02', '2022-09-28 14:11:02'),
(74, 'default', 'Division Bicycle Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 73, NULL, NULL, '[]', NULL, '2022-09-28 14:11:02', '2022-09-28 14:11:02'),
(75, 'default', 'Division Electronic Masking System Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 74, NULL, NULL, '[]', NULL, '2022-09-28 14:11:02', '2022-09-28 14:11:02'),
(76, 'default', 'Division Receptionist and Information Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 75, NULL, NULL, '[]', NULL, '2022-09-28 14:11:02', '2022-09-28 14:11:02'),
(77, 'default', 'Division Custom Tailor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 76, NULL, NULL, '[]', NULL, '2022-09-28 14:11:02', '2022-09-28 14:11:02'),
(78, 'default', 'Division Timing Device Assemblers has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 77, NULL, NULL, '[]', NULL, '2022-09-28 14:11:03', '2022-09-28 14:11:03'),
(79, 'default', 'Division Health Technologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 78, NULL, NULL, '[]', NULL, '2022-09-28 14:11:03', '2022-09-28 14:11:03'),
(80, 'default', 'Division Human Resources Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 79, NULL, NULL, '[]', NULL, '2022-09-28 14:11:03', '2022-09-28 14:11:03'),
(81, 'default', 'Division Pewter Caster has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 80, NULL, NULL, '[]', NULL, '2022-09-28 14:11:03', '2022-09-28 14:11:03'),
(82, 'default', 'Division Silversmith has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 81, NULL, NULL, '[]', NULL, '2022-09-28 14:11:03', '2022-09-28 14:11:03'),
(83, 'default', 'Division Residential Advisor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 82, NULL, NULL, '[]', NULL, '2022-09-28 14:11:04', '2022-09-28 14:11:04'),
(84, 'default', 'Division Clinical Psychologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 83, NULL, NULL, '[]', NULL, '2022-09-28 14:11:04', '2022-09-28 14:11:04'),
(85, 'default', 'Division Hand Presser has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 84, NULL, NULL, '[]', NULL, '2022-09-28 14:11:04', '2022-09-28 14:11:04'),
(86, 'default', 'Division Educational Counselor OR Vocationall Counselor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 85, NULL, NULL, '[]', NULL, '2022-09-28 14:11:04', '2022-09-28 14:11:04'),
(87, 'default', 'Division Separating Machine Operators has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 86, NULL, NULL, '[]', NULL, '2022-09-28 14:11:55', '2022-09-28 14:11:55'),
(88, 'default', 'Division Data Entry Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 87, NULL, NULL, '[]', NULL, '2022-09-28 14:11:55', '2022-09-28 14:11:55'),
(89, 'default', 'Division Radio Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 88, NULL, NULL, '[]', NULL, '2022-09-28 14:11:55', '2022-09-28 14:11:55'),
(90, 'default', 'Division Pharmacy Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 89, NULL, NULL, '[]', NULL, '2022-09-28 14:11:55', '2022-09-28 14:11:55'),
(91, 'default', 'Division Meat Packer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 90, NULL, NULL, '[]', NULL, '2022-09-28 14:11:55', '2022-09-28 14:11:55'),
(92, 'default', 'Division Mechanical Door Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 91, NULL, NULL, '[]', NULL, '2022-09-28 14:11:55', '2022-09-28 14:11:55'),
(93, 'default', 'Division Production Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 92, NULL, NULL, '[]', NULL, '2022-09-28 14:11:56', '2022-09-28 14:11:56'),
(94, 'default', 'Division Slot Key Person has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 93, NULL, NULL, '[]', NULL, '2022-09-28 14:11:56', '2022-09-28 14:11:56'),
(95, 'default', 'Division Medical Secretary has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 94, NULL, NULL, '[]', NULL, '2022-09-28 14:11:56', '2022-09-28 14:11:56'),
(96, 'default', 'Division Lawyer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 95, NULL, NULL, '[]', NULL, '2022-09-28 14:11:56', '2022-09-28 14:11:56'),
(97, 'default', 'Division Floor Finisher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 96, NULL, NULL, '[]', NULL, '2022-09-28 14:11:56', '2022-09-28 14:11:56'),
(98, 'default', 'Division Protective Service Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 97, NULL, NULL, '[]', NULL, '2022-09-28 14:11:56', '2022-09-28 14:11:56'),
(99, 'default', 'Division Operating Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 98, NULL, NULL, '[]', NULL, '2022-09-28 14:11:57', '2022-09-28 14:11:57'),
(100, 'default', 'Division Waste Treatment Plant Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 99, NULL, NULL, '[]', NULL, '2022-09-28 14:11:57', '2022-09-28 14:11:57'),
(101, 'default', 'Division Hand Presser has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 100, NULL, NULL, '[]', NULL, '2022-09-28 14:11:57', '2022-09-28 14:11:57'),
(102, 'default', 'Division Extraction Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 101, NULL, NULL, '[]', NULL, '2022-09-28 14:11:57', '2022-09-28 14:11:57'),
(103, 'default', 'Division Social and Human Service Assistant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 102, NULL, NULL, '[]', NULL, '2022-09-28 14:11:57', '2022-09-28 14:11:57'),
(104, 'default', 'Division Mixing and Blending Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 103, NULL, NULL, '[]', NULL, '2022-09-28 14:11:57', '2022-09-28 14:11:57'),
(105, 'default', 'Division Glass Cutting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 104, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(106, 'default', 'Division Interviewer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 105, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(107, 'default', 'Division Keyboard Instrument Repairer and Tuner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 106, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(108, 'default', 'Division Team Assembler has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 107, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(109, 'default', 'Division Locksmith has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 108, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(110, 'default', 'Division Gas Pumping Station Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 109, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(111, 'default', 'Division Police Identification OR Records Officer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 110, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(112, 'default', 'Division Automatic Teller Machine Servicer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 111, NULL, NULL, '[]', NULL, '2022-09-28 14:11:58', '2022-09-28 14:11:58'),
(113, 'default', 'Division Medical Records Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 112, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(114, 'default', 'Division Computer Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 113, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(115, 'default', 'Division Tax Examiner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 114, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(116, 'default', 'Division Paving Equipment Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 115, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(117, 'default', 'Salary Grade ad has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(118, 'default', 'Salary Grade odit has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(119, 'default', 'Salary Grade qui has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(120, 'default', 'Salary Grade sint has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(121, 'default', 'Salary Grade est has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:11:59', '2022-09-28 14:11:59'),
(122, 'default', 'Salary Grade rerum has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:12:00', '2022-09-28 14:12:00'),
(123, 'default', 'Salary Grade harum has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:12:00', '2022-09-28 14:12:00'),
(124, 'default', 'Salary Grade quam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:12:00', '2022-09-28 14:12:00'),
(125, 'default', 'Salary Grade pariatur has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:12:00', '2022-09-28 14:12:00'),
(126, 'default', 'Salary Grade quos has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:12:00', '2022-09-28 14:12:00'),
(127, 'default', 'Division Manicurists has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 116, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(128, 'default', 'Division Clinical School Psychologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 117, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(129, 'default', 'Division Roofer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 118, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(130, 'default', 'Division Gaming Service Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 119, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(131, 'default', 'Division Offset Lithographic Press Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 120, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(132, 'default', 'Division Pile-Driver Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 121, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(133, 'default', 'Division Compacting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 122, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(134, 'default', 'Division Medical Scientists has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 123, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(135, 'default', 'Division Editor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 124, NULL, NULL, '[]', NULL, '2022-09-28 14:15:02', '2022-09-28 14:15:02'),
(136, 'default', 'Division Janitor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 125, NULL, NULL, '[]', NULL, '2022-09-28 14:15:03', '2022-09-28 14:15:03'),
(137, 'default', 'Division Title Searcher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 126, NULL, NULL, '[]', NULL, '2022-09-28 14:15:03', '2022-09-28 14:15:03'),
(138, 'default', 'Division Casting Machine Set-Up Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 127, NULL, NULL, '[]', NULL, '2022-09-28 14:15:03', '2022-09-28 14:15:03'),
(139, 'default', 'Division Physical Scientist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 128, NULL, NULL, '[]', NULL, '2022-09-28 14:15:03', '2022-09-28 14:15:03'),
(140, 'default', 'Division Drafter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 129, NULL, NULL, '[]', NULL, '2022-09-28 14:15:03', '2022-09-28 14:15:03'),
(141, 'default', 'Division Wellhead Pumper has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 130, NULL, NULL, '[]', NULL, '2022-09-28 14:15:03', '2022-09-28 14:15:03'),
(142, 'default', 'Division Management Analyst has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 131, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(143, 'default', 'Division Cutting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 132, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(144, 'default', 'Division Insurance Policy Processing Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 133, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(145, 'default', 'Division Sales Representative has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 134, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(146, 'default', 'Division Agricultural Crop Farm Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 135, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(147, 'default', 'Division Ceiling Tile Installer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 136, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(148, 'default', 'Division Air Traffic Controller has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 137, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(149, 'default', 'Division Forming Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 138, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(150, 'default', 'Division Forest and Conservation Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 139, NULL, NULL, '[]', NULL, '2022-09-28 14:15:04', '2022-09-28 14:15:04'),
(151, 'default', 'Division Recruiter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 140, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(152, 'default', 'Division Legal Secretary has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 141, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(153, 'default', 'Division Real Estate Sales Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 142, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(154, 'default', 'Division Electrical Engineering Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 143, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(155, 'default', 'Division Agricultural Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 144, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(156, 'default', 'Division Radar Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 145, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(157, 'default', 'Salary Grade reiciendis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(158, 'default', 'Salary Grade molestiae has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:15:05', '2022-09-28 14:15:05'),
(159, 'default', 'Division Sales Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 146, NULL, NULL, '[]', NULL, '2022-09-28 14:15:42', '2022-09-28 14:15:42'),
(160, 'default', 'Division Agricultural Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 147, NULL, NULL, '[]', NULL, '2022-09-28 14:15:42', '2022-09-28 14:15:42'),
(161, 'default', 'Division Mixing and Blending Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 148, NULL, NULL, '[]', NULL, '2022-09-28 14:15:42', '2022-09-28 14:15:42'),
(162, 'default', 'Division Cartographer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 149, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(163, 'default', 'Division Community Service Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 150, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(164, 'default', 'Division Technical Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 151, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(165, 'default', 'Division Platemaker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 152, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(166, 'default', 'Division Advertising Sales Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 153, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(167, 'default', 'Division Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 154, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(168, 'default', 'Division Marine Architect has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 155, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(169, 'default', 'Division Environmental Compliance Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 156, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(170, 'default', 'Division Electrician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 157, NULL, NULL, '[]', NULL, '2022-09-28 14:15:43', '2022-09-28 14:15:43'),
(171, 'default', 'Division Drycleaning Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 158, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(172, 'default', 'Division Casting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 159, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(173, 'default', 'Division Machine Tool Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 160, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(174, 'default', 'Division Crushing Grinding Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 161, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(175, 'default', 'Division Biological Scientist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 162, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(176, 'default', 'Division Costume Attendant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 163, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(177, 'default', 'Division Painting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 164, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(178, 'default', 'Division Forester has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 165, NULL, NULL, '[]', NULL, '2022-09-28 14:15:44', '2022-09-28 14:15:44'),
(179, 'default', 'Division Legal Secretary has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 166, NULL, NULL, '[]', NULL, '2022-09-28 14:15:45', '2022-09-28 14:15:45'),
(180, 'default', 'Division Artillery Crew Member has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 167, NULL, NULL, '[]', NULL, '2022-09-28 14:15:45', '2022-09-28 14:15:45'),
(181, 'default', 'Division Professional Photographer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 168, NULL, NULL, '[]', NULL, '2022-09-28 14:15:45', '2022-09-28 14:15:45'),
(182, 'default', 'Division Reservation Agent OR Transportation Ticket Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 169, NULL, NULL, '[]', NULL, '2022-09-28 14:15:45', '2022-09-28 14:15:45'),
(183, 'default', 'Division Power Distributors OR Dispatcher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 170, NULL, NULL, '[]', NULL, '2022-09-28 14:15:45', '2022-09-28 14:15:45'),
(184, 'default', 'Division Bookkeeper has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 171, NULL, NULL, '[]', NULL, '2022-09-28 14:15:45', '2022-09-28 14:15:45'),
(185, 'default', 'Division Fitter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 172, NULL, NULL, '[]', NULL, '2022-09-28 14:15:46', '2022-09-28 14:15:46'),
(186, 'default', 'Division Landscaper has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 173, NULL, NULL, '[]', NULL, '2022-09-28 14:15:46', '2022-09-28 14:15:46'),
(187, 'default', 'Division Sociology Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 174, NULL, NULL, '[]', NULL, '2022-09-28 14:15:46', '2022-09-28 14:15:46'),
(188, 'default', 'Division Social Sciences Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 175, NULL, NULL, '[]', NULL, '2022-09-28 14:15:46', '2022-09-28 14:15:46'),
(189, 'default', 'Division Poultry Cutter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:16:59', '2022-09-28 14:16:59'),
(190, 'default', 'Division Courier has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:16:59', '2022-09-28 14:16:59'),
(191, 'default', 'Division Sound Engineering Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:17:00', '2022-09-28 14:17:00'),
(192, 'default', 'Division Agricultural Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:17:00', '2022-09-28 14:17:00'),
(193, 'default', 'Division Advertising Manager OR Promotions Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:17:00', '2022-09-28 14:17:00'),
(194, 'default', 'Division Occupational Therapist Aide has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:17:00', '2022-09-28 14:17:00'),
(195, 'default', 'Division Painting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:17:00', '2022-09-28 14:17:00'),
(196, 'default', 'Division ccc has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:17:00', '2022-09-28 14:17:00'),
(197, 'default', 'Division Entertainer and Performer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:17:01', '2022-09-28 14:17:01'),
(198, 'default', 'Division Cartoonist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:17:01', '2022-09-28 14:17:01'),
(199, 'default', 'Division Claims Examiner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:17:01', '2022-09-28 14:17:01'),
(200, 'default', 'Division Personal Home Care Aide has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:17:02', '2022-09-28 14:17:02'),
(201, 'default', 'Division Plumber OR Pipefitter OR Steamfitter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:17:02', '2022-09-28 14:17:02'),
(202, 'default', 'Division Warehouse has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:17:02', '2022-09-28 14:17:02'),
(203, 'default', 'Division Sawing Machine Tool Setter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:17:02', '2022-09-28 14:17:02'),
(204, 'default', 'Division Real Estate Broker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:17:03', '2022-09-28 14:17:03'),
(205, 'default', 'Division Photoengraver has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:17:03', '2022-09-28 14:17:03'),
(206, 'default', 'Division Agricultural Product Grader Sorter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:17:03', '2022-09-28 14:17:03'),
(207, 'default', 'Division Database Administrator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:17:03', '2022-09-28 14:17:03'),
(208, 'default', 'Division Claims Adjuster has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:17:03', '2022-09-28 14:17:03'),
(209, 'default', 'Division Model Maker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 21, NULL, NULL, '[]', NULL, '2022-09-28 14:17:04', '2022-09-28 14:17:04'),
(210, 'default', 'Division Visual Designer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 22, NULL, NULL, '[]', NULL, '2022-09-28 14:17:04', '2022-09-28 14:17:04'),
(211, 'default', 'Division Musical Instrument Tuner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 23, NULL, NULL, '[]', NULL, '2022-09-28 14:17:04', '2022-09-28 14:17:04'),
(212, 'default', 'Division Shear Machine Set-Up Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 24, NULL, NULL, '[]', NULL, '2022-09-28 14:17:04', '2022-09-28 14:17:04'),
(213, 'default', 'Division Child Care Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 25, NULL, NULL, '[]', NULL, '2022-09-28 14:17:04', '2022-09-28 14:17:04'),
(214, 'default', 'Division Home Appliance Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 26, NULL, NULL, '[]', NULL, '2022-09-28 14:17:04', '2022-09-28 14:17:04'),
(215, 'default', 'Division Hairdresser OR Cosmetologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 27, NULL, NULL, '[]', NULL, '2022-09-28 14:17:04', '2022-09-28 14:17:04'),
(216, 'default', 'Division Government has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 28, NULL, NULL, '[]', NULL, '2022-09-28 14:17:05', '2022-09-28 14:17:05'),
(217, 'default', 'Division Freight Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 29, NULL, NULL, '[]', NULL, '2022-09-28 14:17:05', '2022-09-28 14:17:05'),
(218, 'default', 'Division Printing Press Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 30, NULL, NULL, '[]', NULL, '2022-09-28 14:17:05', '2022-09-28 14:17:05'),
(219, 'default', 'Salary Grade qui has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:17:05', '2022-09-28 14:17:05'),
(220, 'default', 'Salary Grade error has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:17:05', '2022-09-28 14:17:05'),
(221, 'default', 'Salary Grade sequi has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:17:05', '2022-09-28 14:17:05'),
(222, 'default', 'Salary Grade sunt has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:17:05', '2022-09-28 14:17:05'),
(223, 'default', 'Salary Grade dolorem has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:17:06', '2022-09-28 14:17:06'),
(224, 'default', 'Salary Grade dolor has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:17:06', '2022-09-28 14:17:06'),
(225, 'default', 'Salary Grade fuga has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:17:06', '2022-09-28 14:17:06'),
(226, 'default', 'Salary Grade ullam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:17:06', '2022-09-28 14:17:06'),
(227, 'default', 'Salary Grade sint has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:17:06', '2022-09-28 14:17:06'),
(228, 'default', 'Salary Grade illo has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:17:06', '2022-09-28 14:17:06'),
(229, 'default', 'Division Law Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 31, NULL, NULL, '[]', NULL, '2022-09-28 14:17:33', '2022-09-28 14:17:33'),
(230, 'default', 'Division Engineering Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 32, NULL, NULL, '[]', NULL, '2022-09-28 14:17:33', '2022-09-28 14:17:33'),
(231, 'default', 'Division Machinery Maintenance has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 33, NULL, NULL, '[]', NULL, '2022-09-28 14:17:33', '2022-09-28 14:17:33'),
(232, 'default', 'Division Waste Treatment Plant Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 34, NULL, NULL, '[]', NULL, '2022-09-28 14:17:33', '2022-09-28 14:17:33'),
(233, 'default', 'Division Judge has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 35, NULL, NULL, '[]', NULL, '2022-09-28 14:17:33', '2022-09-28 14:17:33'),
(234, 'default', 'Division Communication Equipment Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 36, NULL, NULL, '[]', NULL, '2022-09-28 14:17:34', '2022-09-28 14:17:34'),
(235, 'default', 'Division Patrol Officer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 37, NULL, NULL, '[]', NULL, '2022-09-28 14:17:34', '2022-09-28 14:17:34'),
(236, 'default', 'Division Medical Laboratory Technologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 38, NULL, NULL, '[]', NULL, '2022-09-28 14:17:34', '2022-09-28 14:17:34'),
(237, 'default', 'Division Alteration Tailor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 39, NULL, NULL, '[]', NULL, '2022-09-28 14:17:34', '2022-09-28 14:17:34'),
(238, 'default', 'Division Extruding Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 40, NULL, NULL, '[]', NULL, '2022-09-28 14:17:34', '2022-09-28 14:17:34'),
(239, 'default', 'Division Account Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 41, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(240, 'default', 'Division Cost Estimator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 42, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(241, 'default', 'Division Biologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 43, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(242, 'default', 'Division Manufacturing Sales Representative has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 44, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(243, 'default', 'Division Benefits Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 45, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(244, 'default', 'Division Architect has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 46, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(245, 'default', 'Division Producers and Director has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 47, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(246, 'default', 'Division Hand Trimmer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 48, NULL, NULL, '[]', NULL, '2022-09-28 14:17:35', '2022-09-28 14:17:35'),
(247, 'default', 'Division Special Force has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 49, NULL, NULL, '[]', NULL, '2022-09-28 14:17:36', '2022-09-28 14:17:36'),
(248, 'default', 'Division Geologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 50, NULL, NULL, '[]', NULL, '2022-09-28 14:17:36', '2022-09-28 14:17:36'),
(249, 'default', 'Division Marriage and Family Therapist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 51, NULL, NULL, '[]', NULL, '2022-09-28 14:17:36', '2022-09-28 14:17:36'),
(250, 'default', 'Division Well and Core Drill Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 52, NULL, NULL, '[]', NULL, '2022-09-28 14:17:36', '2022-09-28 14:17:36'),
(251, 'default', 'Division Logging Equipment Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 53, NULL, NULL, '[]', NULL, '2022-09-28 14:17:36', '2022-09-28 14:17:36'),
(252, 'default', 'Division Criminal Investigator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 54, NULL, NULL, '[]', NULL, '2022-09-28 14:17:36', '2022-09-28 14:17:36'),
(253, 'default', 'Division Financial Services Sales Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 55, NULL, NULL, '[]', NULL, '2022-09-28 14:17:37', '2022-09-28 14:17:37'),
(254, 'default', 'Division Stone Cutter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 56, NULL, NULL, '[]', NULL, '2022-09-28 14:17:37', '2022-09-28 14:17:37'),
(255, 'default', 'Division Truck Driver has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 57, NULL, NULL, '[]', NULL, '2022-09-28 14:17:37', '2022-09-28 14:17:37'),
(256, 'default', 'Division Communication Equipment Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 58, NULL, NULL, '[]', NULL, '2022-09-28 14:17:37', '2022-09-28 14:17:37'),
(257, 'default', 'Division Printing Press Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 59, NULL, NULL, '[]', NULL, '2022-09-28 14:17:37', '2022-09-28 14:17:37'),
(258, 'default', 'Division Precision Lens Grinders and Polisher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 60, NULL, NULL, '[]', NULL, '2022-09-28 14:17:37', '2022-09-28 14:17:37'),
(259, 'default', 'Salary Grade veniam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:17:38', '2022-09-28 14:17:38'),
(260, 'default', 'Salary Grade velit has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:17:38', '2022-09-28 14:17:38'),
(261, 'default', 'Salary Grade est has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:17:38', '2022-09-28 14:17:38'),
(262, 'default', 'Salary Grade id has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:17:38', '2022-09-28 14:17:38'),
(263, 'default', 'Salary Grade quis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:17:38', '2022-09-28 14:17:38'),
(264, 'default', 'Salary Grade harum has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:17:39', '2022-09-28 14:17:39'),
(265, 'default', 'Salary Grade numquam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:17:39', '2022-09-28 14:17:39'),
(266, 'default', 'Salary Grade impedit has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:17:39', '2022-09-28 14:17:39'),
(267, 'default', 'Salary Grade enim has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:17:39', '2022-09-28 14:17:39');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `event`, `subject_id`, `causer_type`, `causer_id`, `properties`, `batch_uuid`, `created_at`, `updated_at`) VALUES
(268, 'default', 'Salary Grade ducimus has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:17:39', '2022-09-28 14:17:39'),
(269, 'default', 'Division Floor Layer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 61, NULL, NULL, '[]', NULL, '2022-09-28 14:18:32', '2022-09-28 14:18:32'),
(270, 'default', 'Division Rough Carpenter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 62, NULL, NULL, '[]', NULL, '2022-09-28 14:18:33', '2022-09-28 14:18:33'),
(271, 'default', 'Division Maintenance Supervisor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 63, NULL, NULL, '[]', NULL, '2022-09-28 14:18:33', '2022-09-28 14:18:33'),
(272, 'default', 'Division Fashion Model has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 64, NULL, NULL, '[]', NULL, '2022-09-28 14:18:33', '2022-09-28 14:18:33'),
(273, 'default', 'Division Securities Sales Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 65, NULL, NULL, '[]', NULL, '2022-09-28 14:18:33', '2022-09-28 14:18:33'),
(274, 'default', 'Division Construction Equipment Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 66, NULL, NULL, '[]', NULL, '2022-09-28 14:18:34', '2022-09-28 14:18:34'),
(275, 'default', 'Division Private Household Cook has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 67, NULL, NULL, '[]', NULL, '2022-09-28 14:18:34', '2022-09-28 14:18:34'),
(276, 'default', 'Division Concierge has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 68, NULL, NULL, '[]', NULL, '2022-09-28 14:18:34', '2022-09-28 14:18:34'),
(277, 'default', 'Division Park Naturalist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 69, NULL, NULL, '[]', NULL, '2022-09-28 14:18:35', '2022-09-28 14:18:35'),
(278, 'default', 'Division Cutting Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 70, NULL, NULL, '[]', NULL, '2022-09-28 14:18:35', '2022-09-28 14:18:35'),
(279, 'default', 'Division Metal Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 71, NULL, NULL, '[]', NULL, '2022-09-28 14:18:35', '2022-09-28 14:18:35'),
(280, 'default', 'Division Set Designer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 72, NULL, NULL, '[]', NULL, '2022-09-28 14:18:35', '2022-09-28 14:18:35'),
(281, 'default', 'Division Skin Care Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 73, NULL, NULL, '[]', NULL, '2022-09-28 14:18:35', '2022-09-28 14:18:35'),
(282, 'default', 'Division Space Sciences Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 74, NULL, NULL, '[]', NULL, '2022-09-28 14:18:36', '2022-09-28 14:18:36'),
(283, 'default', 'Division Supervisor Correctional Officer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 75, NULL, NULL, '[]', NULL, '2022-09-28 14:18:36', '2022-09-28 14:18:36'),
(284, 'default', 'Division Transportation Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 76, NULL, NULL, '[]', NULL, '2022-09-28 14:18:36', '2022-09-28 14:18:36'),
(285, 'default', 'Division Agricultural Crop Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 77, NULL, NULL, '[]', NULL, '2022-09-28 14:18:36', '2022-09-28 14:18:36'),
(286, 'default', 'Division Power Plant Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 78, NULL, NULL, '[]', NULL, '2022-09-28 14:18:37', '2022-09-28 14:18:37'),
(287, 'default', 'Division Door To Door Sales has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 79, NULL, NULL, '[]', NULL, '2022-09-28 14:18:37', '2022-09-28 14:18:37'),
(288, 'default', 'Division Railroad Switch Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 80, NULL, NULL, '[]', NULL, '2022-09-28 14:18:37', '2022-09-28 14:18:37'),
(289, 'default', 'Division Business Development Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 81, NULL, NULL, '[]', NULL, '2022-09-28 14:18:37', '2022-09-28 14:18:37'),
(290, 'default', 'Division Fiberglass Laminator and Fabricator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 82, NULL, NULL, '[]', NULL, '2022-09-28 14:18:38', '2022-09-28 14:18:38'),
(291, 'default', 'Division Bill and Account Collector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 83, NULL, NULL, '[]', NULL, '2022-09-28 14:18:38', '2022-09-28 14:18:38'),
(292, 'default', 'Division Physician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 84, NULL, NULL, '[]', NULL, '2022-09-28 14:18:38', '2022-09-28 14:18:38'),
(293, 'default', 'Division Physician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 85, NULL, NULL, '[]', NULL, '2022-09-28 14:18:38', '2022-09-28 14:18:38'),
(294, 'default', 'Division General Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 86, NULL, NULL, '[]', NULL, '2022-09-28 14:18:38', '2022-09-28 14:18:38'),
(295, 'default', 'Division Drywall Installer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 87, NULL, NULL, '[]', NULL, '2022-09-28 14:18:38', '2022-09-28 14:18:38'),
(296, 'default', 'Division Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 88, NULL, NULL, '[]', NULL, '2022-09-28 14:18:39', '2022-09-28 14:18:39'),
(297, 'default', 'Division Utility Meter Reader has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 89, NULL, NULL, '[]', NULL, '2022-09-28 14:18:39', '2022-09-28 14:18:39'),
(298, 'default', 'Division Probation Officers and Correctional Treatment Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 90, NULL, NULL, '[]', NULL, '2022-09-28 14:18:39', '2022-09-28 14:18:39'),
(299, 'default', 'Salary Grade ut has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 21, NULL, NULL, '[]', NULL, '2022-09-28 14:18:39', '2022-09-28 14:18:39'),
(300, 'default', 'Salary Grade delectus has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 22, NULL, NULL, '[]', NULL, '2022-09-28 14:18:40', '2022-09-28 14:18:40'),
(301, 'default', 'Salary Grade sit has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 23, NULL, NULL, '[]', NULL, '2022-09-28 14:18:40', '2022-09-28 14:18:40'),
(302, 'default', 'Salary Grade corporis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 24, NULL, NULL, '[]', NULL, '2022-09-28 14:18:40', '2022-09-28 14:18:40'),
(303, 'default', 'Salary Grade illum has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 25, NULL, NULL, '[]', NULL, '2022-09-28 14:18:41', '2022-09-28 14:18:41'),
(304, 'default', 'Salary Grade tempora has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 26, NULL, NULL, '[]', NULL, '2022-09-28 14:18:41', '2022-09-28 14:18:41'),
(305, 'default', 'Salary Grade praesentium has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 27, NULL, NULL, '[]', NULL, '2022-09-28 14:18:41', '2022-09-28 14:18:41'),
(306, 'default', 'Salary Grade alias has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 28, NULL, NULL, '[]', NULL, '2022-09-28 14:18:41', '2022-09-28 14:18:41'),
(307, 'default', 'Salary Grade iste has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 29, NULL, NULL, '[]', NULL, '2022-09-28 14:18:42', '2022-09-28 14:18:42'),
(308, 'default', 'Salary Grade unde has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 30, NULL, NULL, '[]', NULL, '2022-09-28 14:18:42', '2022-09-28 14:18:42'),
(309, 'default', 'Employee Sonya Stehr has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:18:46', '2022-09-28 14:18:46'),
(310, 'default', 'Employee Rudy Berge has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:18:46', '2022-09-28 14:18:46'),
(311, 'default', 'Employee Cassie Kulas has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:18:46', '2022-09-28 14:18:46'),
(312, 'default', 'Employee Tyreek Little DDS has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:18:47', '2022-09-28 14:18:47'),
(313, 'default', 'Employee Erna Lehner has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:18:47', '2022-09-28 14:18:47'),
(314, 'default', 'Employee Breanne Veum has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:18:47', '2022-09-28 14:18:47'),
(315, 'default', 'Employee Lia Kiehn has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:18:48', '2022-09-28 14:18:48'),
(316, 'default', 'Employee Prof. Timmy Cummings has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:18:48', '2022-09-28 14:18:48'),
(317, 'default', 'Employee Jefferey Hammes has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:18:48', '2022-09-28 14:18:48'),
(318, 'default', 'Employee Peggie Haley has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:18:48', '2022-09-28 14:18:48'),
(319, 'default', 'Division Motorboat Mechanic has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 91, NULL, NULL, '[]', NULL, '2022-09-28 14:19:34', '2022-09-28 14:19:34'),
(320, 'default', 'Division Sociology Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 92, NULL, NULL, '[]', NULL, '2022-09-28 14:19:34', '2022-09-28 14:19:34'),
(321, 'default', 'Division Wellhead Pumper has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 93, NULL, NULL, '[]', NULL, '2022-09-28 14:19:34', '2022-09-28 14:19:34'),
(322, 'default', 'Division Industrial Safety Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 94, NULL, NULL, '[]', NULL, '2022-09-28 14:19:34', '2022-09-28 14:19:34'),
(323, 'default', 'Division Business Operations Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 95, NULL, NULL, '[]', NULL, '2022-09-28 14:19:34', '2022-09-28 14:19:34'),
(324, 'default', 'Division Interior Designer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 96, NULL, NULL, '[]', NULL, '2022-09-28 14:19:35', '2022-09-28 14:19:35'),
(325, 'default', 'Division Spraying Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 97, NULL, NULL, '[]', NULL, '2022-09-28 14:19:35', '2022-09-28 14:19:35'),
(326, 'default', 'Division Electric Motor Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 98, NULL, NULL, '[]', NULL, '2022-09-28 14:19:35', '2022-09-28 14:19:35'),
(327, 'default', 'Division Sculptor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 99, NULL, NULL, '[]', NULL, '2022-09-28 14:19:35', '2022-09-28 14:19:35'),
(328, 'default', 'Division Psychiatric Aide has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 100, NULL, NULL, '[]', NULL, '2022-09-28 14:19:35', '2022-09-28 14:19:35'),
(329, 'default', 'Division Gauger has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 101, NULL, NULL, '[]', NULL, '2022-09-28 14:19:35', '2022-09-28 14:19:35'),
(330, 'default', 'Division Costume Attendant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 102, NULL, NULL, '[]', NULL, '2022-09-28 14:19:35', '2022-09-28 14:19:35'),
(331, 'default', 'Division Shampooer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 103, NULL, NULL, '[]', NULL, '2022-09-28 14:19:36', '2022-09-28 14:19:36'),
(332, 'default', 'Division Logging Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 104, NULL, NULL, '[]', NULL, '2022-09-28 14:19:36', '2022-09-28 14:19:36'),
(333, 'default', 'Division Social Scientists has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 105, NULL, NULL, '[]', NULL, '2022-09-28 14:19:36', '2022-09-28 14:19:36'),
(334, 'default', 'Division Lodging Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 106, NULL, NULL, '[]', NULL, '2022-09-28 14:19:36', '2022-09-28 14:19:36'),
(335, 'default', 'Division Psychology Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 107, NULL, NULL, '[]', NULL, '2022-09-28 14:19:36', '2022-09-28 14:19:36'),
(336, 'default', 'Division Extruding Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 108, NULL, NULL, '[]', NULL, '2022-09-28 14:19:36', '2022-09-28 14:19:36'),
(337, 'default', 'Division Mechanical Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 109, NULL, NULL, '[]', NULL, '2022-09-28 14:19:36', '2022-09-28 14:19:36'),
(338, 'default', 'Division Garment has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 110, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(339, 'default', 'Division Substation Maintenance has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 111, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(340, 'default', 'Division Landscape Artist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 112, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(341, 'default', 'Division Mail Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 113, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(342, 'default', 'Division Surgeon has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 114, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(343, 'default', 'Division Retail Salesperson has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 115, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(344, 'default', 'Division Embalmer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 116, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(345, 'default', 'Division Cost Estimator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 117, NULL, NULL, '[]', NULL, '2022-09-28 14:19:37', '2022-09-28 14:19:37'),
(346, 'default', 'Division Copy Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 118, NULL, NULL, '[]', NULL, '2022-09-28 14:19:38', '2022-09-28 14:19:38'),
(347, 'default', 'Division Graphic Designer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 119, NULL, NULL, '[]', NULL, '2022-09-28 14:19:38', '2022-09-28 14:19:38'),
(348, 'default', 'Division Glass Blower has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 120, NULL, NULL, '[]', NULL, '2022-09-28 14:19:38', '2022-09-28 14:19:38'),
(349, 'default', 'Salary Grade a has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 31, NULL, NULL, '[]', NULL, '2022-09-28 14:19:38', '2022-09-28 14:19:38'),
(350, 'default', 'Division Roofer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 121, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(351, 'default', 'Division Administrative Services Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 122, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(352, 'default', 'Division Recreation and Fitness Studies Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 123, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(353, 'default', 'Division Supervisor of Police has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 124, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(354, 'default', 'Division General Farmworker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 125, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(355, 'default', 'Division Etcher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 126, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(356, 'default', 'Division Cultural Studies Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 127, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(357, 'default', 'Division Advertising Manager OR Promotions Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 128, NULL, NULL, '[]', NULL, '2022-09-28 14:20:14', '2022-09-28 14:20:14'),
(358, 'default', 'Division Receptionist and Information Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 129, NULL, NULL, '[]', NULL, '2022-09-28 14:20:15', '2022-09-28 14:20:15'),
(359, 'default', 'Division Insurance Claims Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 130, NULL, NULL, '[]', NULL, '2022-09-28 14:20:15', '2022-09-28 14:20:15'),
(360, 'default', 'Division Computer Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 131, NULL, NULL, '[]', NULL, '2022-09-28 14:20:15', '2022-09-28 14:20:15'),
(361, 'default', 'Division Broadcast Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 132, NULL, NULL, '[]', NULL, '2022-09-28 14:20:15', '2022-09-28 14:20:15'),
(362, 'default', 'Division Set Designer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 133, NULL, NULL, '[]', NULL, '2022-09-28 14:20:15', '2022-09-28 14:20:15'),
(363, 'default', 'Division Grinding Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 134, NULL, NULL, '[]', NULL, '2022-09-28 14:20:15', '2022-09-28 14:20:15'),
(364, 'default', 'Division Precision Lens Grinders and Polisher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 135, NULL, NULL, '[]', NULL, '2022-09-28 14:20:15', '2022-09-28 14:20:15'),
(365, 'default', 'Division Agricultural Product Grader Sorter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 136, NULL, NULL, '[]', NULL, '2022-09-28 14:20:16', '2022-09-28 14:20:16'),
(366, 'default', 'Division Environmental Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 137, NULL, NULL, '[]', NULL, '2022-09-28 14:20:16', '2022-09-28 14:20:16'),
(367, 'default', 'Division Dishwasher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 138, NULL, NULL, '[]', NULL, '2022-09-28 14:20:16', '2022-09-28 14:20:16'),
(368, 'default', 'Division Aircraft Rigging Assembler has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 139, NULL, NULL, '[]', NULL, '2022-09-28 14:20:16', '2022-09-28 14:20:16'),
(369, 'default', 'Division Rail Transportation Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 140, NULL, NULL, '[]', NULL, '2022-09-28 14:20:16', '2022-09-28 14:20:16'),
(370, 'default', 'Division School Bus Driver has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 141, NULL, NULL, '[]', NULL, '2022-09-28 14:20:16', '2022-09-28 14:20:16'),
(371, 'default', 'Division Radiation Therapist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 142, NULL, NULL, '[]', NULL, '2022-09-28 14:20:17', '2022-09-28 14:20:17'),
(372, 'default', 'Division Warehouse has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 143, NULL, NULL, '[]', NULL, '2022-09-28 14:20:17', '2022-09-28 14:20:17'),
(373, 'default', 'Division Bulldozer Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 144, NULL, NULL, '[]', NULL, '2022-09-28 14:20:17', '2022-09-28 14:20:17'),
(374, 'default', 'Division Occupational Therapist Assistant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 145, NULL, NULL, '[]', NULL, '2022-09-28 14:20:17', '2022-09-28 14:20:17'),
(375, 'default', 'Division Well and Core Drill Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 146, NULL, NULL, '[]', NULL, '2022-09-28 14:20:17', '2022-09-28 14:20:17'),
(376, 'default', 'Division Boilermaker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 147, NULL, NULL, '[]', NULL, '2022-09-28 14:20:17', '2022-09-28 14:20:17'),
(377, 'default', 'Division Biophysicist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 148, NULL, NULL, '[]', NULL, '2022-09-28 14:20:17', '2022-09-28 14:20:17'),
(378, 'default', 'Division Art Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 149, NULL, NULL, '[]', NULL, '2022-09-28 14:20:18', '2022-09-28 14:20:18'),
(379, 'default', 'Division Postal Service Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 150, NULL, NULL, '[]', NULL, '2022-09-28 14:20:18', '2022-09-28 14:20:18'),
(380, 'default', 'Salary Grade ipsa has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:20:18', '2022-09-28 14:20:18'),
(381, 'default', 'Salary Grade iusto has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:20:18', '2022-09-28 14:20:18'),
(382, 'default', 'Salary Grade aliquam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:20:18', '2022-09-28 14:20:18'),
(383, 'default', 'Salary Grade facere has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:20:18', '2022-09-28 14:20:18'),
(384, 'default', 'Salary Grade a has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:20:18', '2022-09-28 14:20:18'),
(385, 'default', 'Salary Grade impedit has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:20:19', '2022-09-28 14:20:19'),
(386, 'default', 'Salary Grade illum has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:20:19', '2022-09-28 14:20:19'),
(387, 'default', 'Salary Grade voluptas has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:20:19', '2022-09-28 14:20:19'),
(388, 'default', 'Salary Grade qui has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:20:19', '2022-09-28 14:20:19'),
(389, 'default', 'Salary Grade expedita has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:20:19', '2022-09-28 14:20:19'),
(390, 'default', 'Employee Lilly Braun has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:20:23', '2022-09-28 14:20:23'),
(391, 'default', 'Employee Burley Champlin I has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:20:23', '2022-09-28 14:20:23'),
(392, 'default', 'Employee Kayleigh Swift has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:20:23', '2022-09-28 14:20:23'),
(393, 'default', 'Employee Lesly Deckow has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:20:23', '2022-09-28 14:20:23'),
(394, 'default', 'Employee Dolores McGlynn has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:20:23', '2022-09-28 14:20:23'),
(395, 'default', 'Employee Dr. Lee Nienow has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:20:23', '2022-09-28 14:20:23'),
(396, 'default', 'Employee Miss Verla Metz has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:20:23', '2022-09-28 14:20:23'),
(397, 'default', 'Employee Marge Bins has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:20:24', '2022-09-28 14:20:24'),
(398, 'default', 'Employee Thalia Sipes has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:20:24', '2022-09-28 14:20:24'),
(399, 'default', 'Employee Rickey Nolan has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:20:24', '2022-09-28 14:20:24'),
(400, 'default', 'Division Food Service Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 151, NULL, NULL, '[]', NULL, '2022-09-28 14:21:00', '2022-09-28 14:21:00'),
(401, 'default', 'Division Security Guard has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 152, NULL, NULL, '[]', NULL, '2022-09-28 14:21:00', '2022-09-28 14:21:00'),
(402, 'default', 'Division Furnace Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 153, NULL, NULL, '[]', NULL, '2022-09-28 14:21:00', '2022-09-28 14:21:00'),
(403, 'default', 'Division Weapons Specialists has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 154, NULL, NULL, '[]', NULL, '2022-09-28 14:21:01', '2022-09-28 14:21:01'),
(404, 'default', 'Division Housekeeper has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 155, NULL, NULL, '[]', NULL, '2022-09-28 14:21:01', '2022-09-28 14:21:01'),
(405, 'default', 'Division Web Developer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 156, NULL, NULL, '[]', NULL, '2022-09-28 14:21:01', '2022-09-28 14:21:01'),
(406, 'default', 'Division Model Maker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 157, NULL, NULL, '[]', NULL, '2022-09-28 14:21:01', '2022-09-28 14:21:01'),
(407, 'default', 'Division Craft Artist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 158, NULL, NULL, '[]', NULL, '2022-09-28 14:21:01', '2022-09-28 14:21:01'),
(408, 'default', 'Division Broadcast Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 159, NULL, NULL, '[]', NULL, '2022-09-28 14:21:01', '2022-09-28 14:21:01'),
(409, 'default', 'Division Molding Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 160, NULL, NULL, '[]', NULL, '2022-09-28 14:21:01', '2022-09-28 14:21:01'),
(410, 'default', 'Division Underground Mining has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 161, NULL, NULL, '[]', NULL, '2022-09-28 14:21:02', '2022-09-28 14:21:02'),
(411, 'default', 'Division Home Economics Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 162, NULL, NULL, '[]', NULL, '2022-09-28 14:21:02', '2022-09-28 14:21:02'),
(412, 'default', 'Division License Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 163, NULL, NULL, '[]', NULL, '2022-09-28 14:21:02', '2022-09-28 14:21:02'),
(413, 'default', 'Division Talent Director has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 164, NULL, NULL, '[]', NULL, '2022-09-28 14:21:02', '2022-09-28 14:21:02'),
(414, 'default', 'Division Educational Counselor OR Vocationall Counselor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 165, NULL, NULL, '[]', NULL, '2022-09-28 14:21:02', '2022-09-28 14:21:02'),
(415, 'default', 'Division Pewter Caster has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 166, NULL, NULL, '[]', NULL, '2022-09-28 14:21:02', '2022-09-28 14:21:02'),
(416, 'default', 'Division Cafeteria Cook has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 167, NULL, NULL, '[]', NULL, '2022-09-28 14:21:02', '2022-09-28 14:21:02'),
(417, 'default', 'Division Petroleum Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 168, NULL, NULL, '[]', NULL, '2022-09-28 14:21:03', '2022-09-28 14:21:03'),
(418, 'default', 'Division License Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 169, NULL, NULL, '[]', NULL, '2022-09-28 14:21:03', '2022-09-28 14:21:03'),
(419, 'default', 'Division Social Media Marketing Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 170, NULL, NULL, '[]', NULL, '2022-09-28 14:21:03', '2022-09-28 14:21:03'),
(420, 'default', 'Division Sewing Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 171, NULL, NULL, '[]', NULL, '2022-09-28 14:21:03', '2022-09-28 14:21:03'),
(421, 'default', 'Division Statement Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 172, NULL, NULL, '[]', NULL, '2022-09-28 14:21:03', '2022-09-28 14:21:03'),
(422, 'default', 'Division Insulation Installer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 173, NULL, NULL, '[]', NULL, '2022-09-28 14:21:03', '2022-09-28 14:21:03'),
(423, 'default', 'Division Product Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 174, NULL, NULL, '[]', NULL, '2022-09-28 14:21:04', '2022-09-28 14:21:04'),
(424, 'default', 'Division Fabric Mender has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 175, NULL, NULL, '[]', NULL, '2022-09-28 14:21:04', '2022-09-28 14:21:04'),
(425, 'default', 'Division Rental Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 176, NULL, NULL, '[]', NULL, '2022-09-28 14:21:04', '2022-09-28 14:21:04'),
(426, 'default', 'Division Metal-Refining Furnace Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 177, NULL, NULL, '[]', NULL, '2022-09-28 14:21:04', '2022-09-28 14:21:04'),
(427, 'default', 'Division Gas Appliance Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 178, NULL, NULL, '[]', NULL, '2022-09-28 14:21:04', '2022-09-28 14:21:04'),
(428, 'default', 'Division Explosives Expert has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 179, NULL, NULL, '[]', NULL, '2022-09-28 14:21:04', '2022-09-28 14:21:04'),
(429, 'default', 'Division Chemical Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 180, NULL, NULL, '[]', NULL, '2022-09-28 14:21:04', '2022-09-28 14:21:04'),
(430, 'default', 'Salary Grade non has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:21:05', '2022-09-28 14:21:05'),
(431, 'default', 'Salary Grade accusantium has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:21:05', '2022-09-28 14:21:05'),
(432, 'default', 'Salary Grade quaerat has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:21:05', '2022-09-28 14:21:05'),
(433, 'default', 'Salary Grade cumque has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:21:05', '2022-09-28 14:21:05'),
(434, 'default', 'Salary Grade possimus has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:21:05', '2022-09-28 14:21:05'),
(435, 'default', 'Salary Grade voluptate has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:21:06', '2022-09-28 14:21:06'),
(436, 'default', 'Salary Grade perspiciatis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:21:06', '2022-09-28 14:21:06'),
(437, 'default', 'Salary Grade corrupti has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:21:06', '2022-09-28 14:21:06'),
(438, 'default', 'Salary Grade unde has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:21:06', '2022-09-28 14:21:06'),
(439, 'default', 'Salary Grade ex has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:21:06', '2022-09-28 14:21:06'),
(440, 'default', 'Employee Mrs. Zula Fadel III has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 21, NULL, NULL, '[]', NULL, '2022-09-28 14:21:09', '2022-09-28 14:21:09'),
(441, 'default', 'Employee Dr. Kaleigh Altenwerth has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 22, NULL, NULL, '[]', NULL, '2022-09-28 14:21:09', '2022-09-28 14:21:09'),
(442, 'default', 'Employee Frederik Streich has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 23, NULL, NULL, '[]', NULL, '2022-09-28 14:21:10', '2022-09-28 14:21:10'),
(443, 'default', 'Employee Heber Jenkins has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 24, NULL, NULL, '[]', NULL, '2022-09-28 14:21:10', '2022-09-28 14:21:10'),
(444, 'default', 'Employee Mr. Stanford Collier has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 25, NULL, NULL, '[]', NULL, '2022-09-28 14:21:10', '2022-09-28 14:21:10'),
(445, 'default', 'Employee Hope Cremin V has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 26, NULL, NULL, '[]', NULL, '2022-09-28 14:21:10', '2022-09-28 14:21:10'),
(446, 'default', 'Employee Mr. Ezra Rowe has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 27, NULL, NULL, '[]', NULL, '2022-09-28 14:21:10', '2022-09-28 14:21:10'),
(447, 'default', 'Employee Constance Wiegand has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 28, NULL, NULL, '[]', NULL, '2022-09-28 14:21:10', '2022-09-28 14:21:10'),
(448, 'default', 'Employee Agustina Boehm has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 29, NULL, NULL, '[]', NULL, '2022-09-28 14:21:11', '2022-09-28 14:21:11'),
(449, 'default', 'Employee Salma Kreiger has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 30, NULL, NULL, '[]', NULL, '2022-09-28 14:21:11', '2022-09-28 14:21:11'),
(450, 'default', 'Division City has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:22:03', '2022-09-28 14:22:03'),
(451, 'default', 'Division Transportation and Material-Moving has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:22:04', '2022-09-28 14:22:04'),
(452, 'default', 'Division Social Service Specialists has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:22:04', '2022-09-28 14:22:04'),
(453, 'default', 'Division MARCOM Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:22:04', '2022-09-28 14:22:04'),
(454, 'default', 'Division Able Seamen has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:22:05', '2022-09-28 14:22:05'),
(455, 'default', 'Division Claims Taker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:22:05', '2022-09-28 14:22:05'),
(456, 'default', 'Division Radio Mechanic has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:22:05', '2022-09-28 14:22:05'),
(457, 'default', 'Division Software Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:22:05', '2022-09-28 14:22:05'),
(458, 'default', 'Division Surveyor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:22:06', '2022-09-28 14:22:06'),
(459, 'default', 'Division Human Resource Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:22:06', '2022-09-28 14:22:06'),
(460, 'default', 'Division Employment Interviewer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:22:06', '2022-09-28 14:22:06'),
(461, 'default', 'Division Private Detective and Investigator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:22:06', '2022-09-28 14:22:06'),
(462, 'default', 'Division Technical Director has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:22:06', '2022-09-28 14:22:06'),
(463, 'default', 'Division General Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:22:06', '2022-09-28 14:22:06'),
(464, 'default', 'Division Broadcast News Analyst has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:22:07', '2022-09-28 14:22:07'),
(465, 'default', 'Division Silversmith has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:22:07', '2022-09-28 14:22:07'),
(466, 'default', 'Division Freight Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:22:07', '2022-09-28 14:22:07'),
(467, 'default', 'Division Service Station Attendant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:22:07', '2022-09-28 14:22:07'),
(468, 'default', 'Division Nursery Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:22:07', '2022-09-28 14:22:07'),
(469, 'default', 'Division Online Marketing Analyst has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:22:07', '2022-09-28 14:22:07'),
(470, 'default', 'Division Credit Checker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 21, NULL, NULL, '[]', NULL, '2022-09-28 14:22:08', '2022-09-28 14:22:08'),
(471, 'default', 'Division Child Care has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 22, NULL, NULL, '[]', NULL, '2022-09-28 14:22:08', '2022-09-28 14:22:08'),
(472, 'default', 'Division Chemical Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 23, NULL, NULL, '[]', NULL, '2022-09-28 14:22:08', '2022-09-28 14:22:08'),
(473, 'default', 'Division Computer Support Specialist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 24, NULL, NULL, '[]', NULL, '2022-09-28 14:22:08', '2022-09-28 14:22:08'),
(474, 'default', 'Division Physical Therapist Assistant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 25, NULL, NULL, '[]', NULL, '2022-09-28 14:22:09', '2022-09-28 14:22:09'),
(475, 'default', 'Division Surgeon has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 26, NULL, NULL, '[]', NULL, '2022-09-28 14:22:09', '2022-09-28 14:22:09'),
(476, 'default', 'Division Home Health Aide has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 27, NULL, NULL, '[]', NULL, '2022-09-28 14:22:09', '2022-09-28 14:22:09'),
(477, 'default', 'Division Economist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 28, NULL, NULL, '[]', NULL, '2022-09-28 14:22:09', '2022-09-28 14:22:09'),
(478, 'default', 'Division Appliance Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 29, NULL, NULL, '[]', NULL, '2022-09-28 14:22:09', '2022-09-28 14:22:09'),
(479, 'default', 'Division Account Manager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 30, NULL, NULL, '[]', NULL, '2022-09-28 14:22:09', '2022-09-28 14:22:09'),
(480, 'default', 'Salary Grade iure has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:22:10', '2022-09-28 14:22:10'),
(481, 'default', 'Salary Grade molestiae has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:22:10', '2022-09-28 14:22:10'),
(482, 'default', 'Salary Grade quisquam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:22:10', '2022-09-28 14:22:10'),
(483, 'default', 'Salary Grade iusto has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:22:10', '2022-09-28 14:22:10'),
(484, 'default', 'Salary Grade ex has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:22:10', '2022-09-28 14:22:10'),
(485, 'default', 'Salary Grade perferendis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:22:10', '2022-09-28 14:22:10'),
(486, 'default', 'Salary Grade voluptatem has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:22:11', '2022-09-28 14:22:11'),
(487, 'default', 'Salary Grade qui has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:22:11', '2022-09-28 14:22:11'),
(488, 'default', 'Salary Grade maiores has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:22:11', '2022-09-28 14:22:11'),
(489, 'default', 'Salary Grade sed has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:22:11', '2022-09-28 14:22:11'),
(490, 'default', 'Employee Zena Gaylord has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:22:14', '2022-09-28 14:22:14'),
(491, 'default', 'Employee Abdullah Gorczany has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:22:14', '2022-09-28 14:22:14'),
(492, 'default', 'Employee Glennie Abbott has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:22:15', '2022-09-28 14:22:15'),
(493, 'default', 'Employee Isaac Runte has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:22:15', '2022-09-28 14:22:15'),
(494, 'default', 'Employee Gideon Christiansen has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:22:15', '2022-09-28 14:22:15'),
(495, 'default', 'Employee Pat Kirlin MD has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:22:15', '2022-09-28 14:22:15'),
(496, 'default', 'Employee Ms. Delores Kuphal I has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:22:15', '2022-09-28 14:22:15'),
(497, 'default', 'Employee Dr. Earl Ryan has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:22:16', '2022-09-28 14:22:16'),
(498, 'default', 'Employee Edmund Renner has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:22:16', '2022-09-28 14:22:16'),
(499, 'default', 'Employee Ms. Rebecca Quitzon PhD has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:22:16', '2022-09-28 14:22:16'),
(500, 'default', 'Division Animal Husbandry Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 31, NULL, NULL, '[]', NULL, '2022-09-28 14:27:31', '2022-09-28 14:27:31'),
(501, 'default', 'Division Supervisor Fire Fighting Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 32, NULL, NULL, '[]', NULL, '2022-09-28 14:27:31', '2022-09-28 14:27:31'),
(502, 'default', 'Division Clergy has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 33, NULL, NULL, '[]', NULL, '2022-09-28 14:27:32', '2022-09-28 14:27:32'),
(503, 'default', 'Division Automotive Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 34, NULL, NULL, '[]', NULL, '2022-09-28 14:27:32', '2022-09-28 14:27:32'),
(504, 'default', 'Division Logging Tractor Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 35, NULL, NULL, '[]', NULL, '2022-09-28 14:27:32', '2022-09-28 14:27:32'),
(505, 'default', 'Division Structural Iron and Steel Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 36, NULL, NULL, '[]', NULL, '2022-09-28 14:27:32', '2022-09-28 14:27:32'),
(506, 'default', 'Division Commercial and Industrial Designer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 37, NULL, NULL, '[]', NULL, '2022-09-28 14:27:32', '2022-09-28 14:27:32'),
(507, 'default', 'Division Night Shift has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 38, NULL, NULL, '[]', NULL, '2022-09-28 14:27:32', '2022-09-28 14:27:32'),
(508, 'default', 'Division Computer Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 39, NULL, NULL, '[]', NULL, '2022-09-28 14:27:33', '2022-09-28 14:27:33'),
(509, 'default', 'Division Cost Estimator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 40, NULL, NULL, '[]', NULL, '2022-09-28 14:27:33', '2022-09-28 14:27:33'),
(510, 'default', 'Division Typesetter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 41, NULL, NULL, '[]', NULL, '2022-09-28 14:27:33', '2022-09-28 14:27:33'),
(511, 'default', 'Division Reporters OR Correspondent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 42, NULL, NULL, '[]', NULL, '2022-09-28 14:27:33', '2022-09-28 14:27:33'),
(512, 'default', 'Division Forging Machine Setter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 43, NULL, NULL, '[]', NULL, '2022-09-28 14:27:33', '2022-09-28 14:27:33'),
(513, 'default', 'Division Dental Hygienist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 44, NULL, NULL, '[]', NULL, '2022-09-28 14:27:34', '2022-09-28 14:27:34'),
(514, 'default', 'Division Food Tobacco Roasting has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 45, NULL, NULL, '[]', NULL, '2022-09-28 14:27:34', '2022-09-28 14:27:34'),
(515, 'default', 'Division Admin has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 46, NULL, NULL, '[]', NULL, '2022-09-28 14:27:34', '2022-09-28 14:27:34'),
(516, 'default', 'Division Loading Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 47, NULL, NULL, '[]', NULL, '2022-09-28 14:27:35', '2022-09-28 14:27:35'),
(517, 'default', 'Division Precision Pattern and Die Caster has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 48, NULL, NULL, '[]', NULL, '2022-09-28 14:27:35', '2022-09-28 14:27:35'),
(518, 'default', 'Division House Cleaner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 49, NULL, NULL, '[]', NULL, '2022-09-28 14:27:35', '2022-09-28 14:27:35'),
(519, 'default', 'Division Veterinarian has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 50, NULL, NULL, '[]', NULL, '2022-09-28 14:27:35', '2022-09-28 14:27:35'),
(520, 'default', 'Position Fishery Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:27:35', '2022-09-28 14:27:35'),
(521, 'default', 'Position Data Processing Equipment Repairer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:27:35', '2022-09-28 14:27:35'),
(522, 'default', 'Position Medical Technician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:27:36', '2022-09-28 14:27:36'),
(523, 'default', 'Position Wellhead Pumper has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:27:36', '2022-09-28 14:27:36'),
(524, 'default', 'Position Legal Support Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:27:36', '2022-09-28 14:27:36'),
(525, 'default', 'Position Vice President Of Marketing has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:27:36', '2022-09-28 14:27:36'),
(526, 'default', 'Position Molding and Casting Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:27:36', '2022-09-28 14:27:36'),
(527, 'default', 'Position Merchandise Displayer OR Window Trimmer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:27:36', '2022-09-28 14:27:36'),
(528, 'default', 'Position Occupational Therapist Aide has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:27:36', '2022-09-28 14:27:36'),
(529, 'default', 'Position Electrical Drafter has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:27:37', '2022-09-28 14:27:37'),
(530, 'default', 'Division Maid has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:29:17', '2022-09-28 14:29:17'),
(531, 'default', 'Division Central Office and PBX Installers has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:29:17', '2022-09-28 14:29:17'),
(532, 'default', 'Division Explosives Expert has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18'),
(533, 'default', 'Division Private Sector Executive has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18'),
(534, 'default', 'Division Geoscientists has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18'),
(535, 'default', 'Division Sys Admin has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18'),
(536, 'default', 'Division Rail Car Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18'),
(537, 'default', 'Division Order Filler OR Stock Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18'),
(538, 'default', 'Division Commercial Diver has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `event`, `subject_id`, `causer_type`, `causer_id`, `properties`, `batch_uuid`, `created_at`, `updated_at`) VALUES
(539, 'default', 'Division Customer Service Representative has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:29:18', '2022-09-28 14:29:18'),
(540, 'default', 'Division Welding Machine Setter has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:29:19', '2022-09-28 14:29:19'),
(541, 'default', 'Division Creative Writer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:29:19', '2022-09-28 14:29:19'),
(542, 'default', 'Division Recreation and Fitness Studies Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:29:19', '2022-09-28 14:29:19'),
(543, 'default', 'Division Gas Pumping Station Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:29:19', '2022-09-28 14:29:19'),
(544, 'default', 'Division Cashier has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:29:19', '2022-09-28 14:29:19'),
(545, 'default', 'Division Medical Equipment Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:29:19', '2022-09-28 14:29:19'),
(546, 'default', 'Division Dental Assistant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:29:19', '2022-09-28 14:29:19'),
(547, 'default', 'Division Motorboat Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:29:20', '2022-09-28 14:29:20'),
(548, 'default', 'Division Music Director has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:29:20', '2022-09-28 14:29:20'),
(549, 'default', 'Division Financial Examiner has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:29:20', '2022-09-28 14:29:20'),
(550, 'default', 'Position Gaming Manager has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:29:20', '2022-09-28 14:29:20'),
(551, 'default', 'Position Taper has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:29:21', '2022-09-28 14:29:21'),
(552, 'default', 'Position Agricultural Crop Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:29:21', '2022-09-28 14:29:21'),
(553, 'default', 'Position Social and Human Service Assistant has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:29:21', '2022-09-28 14:29:21'),
(554, 'default', 'Position Weapons Specialists has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:29:21', '2022-09-28 14:29:21'),
(555, 'default', 'Position Electrician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:29:21', '2022-09-28 14:29:21'),
(556, 'default', 'Position Sawing Machine Setter has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:29:21', '2022-09-28 14:29:21'),
(557, 'default', 'Position Communication Equipment Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:29:22', '2022-09-28 14:29:22'),
(558, 'default', 'Position Actuary has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:29:22', '2022-09-28 14:29:22'),
(559, 'default', 'Position Civil Engineer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:29:22', '2022-09-28 14:29:22'),
(560, 'default', 'Salary Grade accusamus has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:29:22', '2022-09-28 14:29:22'),
(561, 'default', 'Salary Grade culpa has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:29:22', '2022-09-28 14:29:22'),
(562, 'default', 'Salary Grade maxime has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:29:22', '2022-09-28 14:29:22'),
(563, 'default', 'Salary Grade ipsam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:29:23', '2022-09-28 14:29:23'),
(564, 'default', 'Salary Grade ut has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:29:23', '2022-09-28 14:29:23'),
(565, 'default', 'Salary Grade ad has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:29:23', '2022-09-28 14:29:23'),
(566, 'default', 'Salary Grade voluptatem has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:29:23', '2022-09-28 14:29:23'),
(567, 'default', 'Salary Grade molestiae has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:29:23', '2022-09-28 14:29:23'),
(568, 'default', 'Salary Grade id has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:29:23', '2022-09-28 14:29:23'),
(569, 'default', 'Salary Grade voluptas has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:29:23', '2022-09-28 14:29:23'),
(570, 'default', 'Employee Lempi Herzog has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:29:26', '2022-09-28 14:29:26'),
(571, 'default', 'Employee Ms. Meda Morissette Jr. has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:29:26', '2022-09-28 14:29:26'),
(572, 'default', 'Employee Giovani Bosco has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:29:27', '2022-09-28 14:29:27'),
(573, 'default', 'Employee Thurman Anderson has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:29:27', '2022-09-28 14:29:27'),
(574, 'default', 'Employee Brandi Bayer has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:29:27', '2022-09-28 14:29:27'),
(575, 'default', 'Employee Stefan Kovacek has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:29:27', '2022-09-28 14:29:27'),
(576, 'default', 'Employee Cedrick Braun has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:29:27', '2022-09-28 14:29:27'),
(577, 'default', 'Employee Ewald Rutherford III has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:29:27', '2022-09-28 14:29:27'),
(578, 'default', 'Employee Miss Roselyn Howe has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:29:27', '2022-09-28 14:29:27'),
(579, 'default', 'Employee Prof. Sandrine Bode has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:29:28', '2022-09-28 14:29:28'),
(580, 'default', 'Division Pharmaceutical Sales Representative has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 21, NULL, NULL, '[]', NULL, '2022-09-28 14:30:49', '2022-09-28 14:30:49'),
(581, 'default', 'Division Umpire and Referee has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 22, NULL, NULL, '[]', NULL, '2022-09-28 14:30:49', '2022-09-28 14:30:49'),
(582, 'default', 'Division Cement Mason and Concrete Finisher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 23, NULL, NULL, '[]', NULL, '2022-09-28 14:30:49', '2022-09-28 14:30:49'),
(583, 'default', 'Division Clerk has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 24, NULL, NULL, '[]', NULL, '2022-09-28 14:30:50', '2022-09-28 14:30:50'),
(584, 'default', 'Division Pile-Driver Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 25, NULL, NULL, '[]', NULL, '2022-09-28 14:30:50', '2022-09-28 14:30:50'),
(585, 'default', 'Division Food Preparation has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 26, NULL, NULL, '[]', NULL, '2022-09-28 14:30:50', '2022-09-28 14:30:50'),
(586, 'default', 'Division Staff Psychologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 27, NULL, NULL, '[]', NULL, '2022-09-28 14:30:50', '2022-09-28 14:30:50'),
(587, 'default', 'Division Packer and Packager has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 28, NULL, NULL, '[]', NULL, '2022-09-28 14:30:50', '2022-09-28 14:30:50'),
(588, 'default', 'Division Surgical Technologist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 29, NULL, NULL, '[]', NULL, '2022-09-28 14:30:50', '2022-09-28 14:30:50'),
(589, 'default', 'Division Graphic Designer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 30, NULL, NULL, '[]', NULL, '2022-09-28 14:30:50', '2022-09-28 14:30:50'),
(590, 'default', 'Division Power Generating Plant Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 31, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(591, 'default', 'Division Computer Hardware Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 32, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(592, 'default', 'Division Farm Labor Contractor has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 33, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(593, 'default', 'Division Social and Human Service Assistant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 34, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(594, 'default', 'Division Driver-Sales Worker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 35, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(595, 'default', 'Shift Schedule doloremque has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(596, 'default', 'Shift Schedule amet has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(597, 'default', 'Shift Schedule illo has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:30:51', '2022-09-28 14:30:51'),
(598, 'default', 'Shift Schedule magnam has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:30:52', '2022-09-28 14:30:52'),
(599, 'default', 'Shift Schedule beatae has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:30:52', '2022-09-28 14:30:52'),
(600, 'default', 'Position Event Planner has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:30:52', '2022-09-28 14:30:52'),
(601, 'default', 'Position Anesthesiologist has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:30:52', '2022-09-28 14:30:52'),
(602, 'default', 'Position Logistician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:30:52', '2022-09-28 14:30:52'),
(603, 'default', 'Position Plumber has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:30:52', '2022-09-28 14:30:52'),
(604, 'default', 'Position Makeup Artists has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:30:53', '2022-09-28 14:30:53'),
(605, 'default', 'Position Actuary has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:30:53', '2022-09-28 14:30:53'),
(606, 'default', 'Position Geological Sample Test Technician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:30:53', '2022-09-28 14:30:53'),
(607, 'default', 'Position Production Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:30:53', '2022-09-28 14:30:53'),
(608, 'default', 'Position Food Service Manager has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:30:53', '2022-09-28 14:30:53'),
(609, 'default', 'Position Glass Blower has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:30:53', '2022-09-28 14:30:53'),
(610, 'default', 'Salary Grade quia has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(611, 'default', 'Salary Grade nam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(612, 'default', 'Salary Grade et has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(613, 'default', 'Salary Grade sunt has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(614, 'default', 'Salary Grade provident has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(615, 'default', 'Salary Grade rerum has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(616, 'default', 'Salary Grade nobis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(617, 'default', 'Salary Grade occaecati has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:30:54', '2022-09-28 14:30:54'),
(618, 'default', 'Salary Grade nisi has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:30:55', '2022-09-28 14:30:55'),
(619, 'default', 'Salary Grade sit has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:30:55', '2022-09-28 14:30:55'),
(620, 'default', 'Employee Alvina O\'Connell has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:30:58', '2022-09-28 14:30:58'),
(621, 'default', 'Employee Michele Kuphal has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:30:59', '2022-09-28 14:30:59'),
(622, 'default', 'Employee Ahmed Johns has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:30:59', '2022-09-28 14:30:59'),
(623, 'default', 'Employee Prof. Dexter Bailey II has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:30:59', '2022-09-28 14:30:59'),
(624, 'default', 'Employee Dr. Maximilian O\'Connell III has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:31:00', '2022-09-28 14:31:00'),
(625, 'default', 'Employee Margarette Fisher has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 16, NULL, NULL, '[]', NULL, '2022-09-28 14:31:00', '2022-09-28 14:31:00'),
(626, 'default', 'Employee Dr. Jana Stoltenberg II has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 17, NULL, NULL, '[]', NULL, '2022-09-28 14:31:00', '2022-09-28 14:31:00'),
(627, 'default', 'Employee Prof. Hayden Eichmann has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 18, NULL, NULL, '[]', NULL, '2022-09-28 14:31:00', '2022-09-28 14:31:00'),
(628, 'default', 'Employee Prof. Queenie Fadel IV has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 19, NULL, NULL, '[]', NULL, '2022-09-28 14:31:00', '2022-09-28 14:31:00'),
(629, 'default', 'Employee Coleman Kautzer IV has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 20, NULL, NULL, '[]', NULL, '2022-09-28 14:31:00', '2022-09-28 14:31:00'),
(630, 'default', 'Division Athletic Trainer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 36, NULL, NULL, '[]', NULL, '2022-09-28 14:32:07', '2022-09-28 14:32:07'),
(631, 'default', 'Division Police Identification OR Records Officer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 37, NULL, NULL, '[]', NULL, '2022-09-28 14:32:07', '2022-09-28 14:32:07'),
(632, 'default', 'Division Gas Pumping Station Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 38, NULL, NULL, '[]', NULL, '2022-09-28 14:32:07', '2022-09-28 14:32:07'),
(633, 'default', 'Division Railroad Inspector has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 39, NULL, NULL, '[]', NULL, '2022-09-28 14:32:07', '2022-09-28 14:32:07'),
(634, 'default', 'Division Environmental Engineering Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 40, NULL, NULL, '[]', NULL, '2022-09-28 14:32:07', '2022-09-28 14:32:07'),
(635, 'default', 'Division Respiratory Therapy Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 41, NULL, NULL, '[]', NULL, '2022-09-28 14:32:07', '2022-09-28 14:32:07'),
(636, 'default', 'Division Market Research Analyst has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 42, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(637, 'default', 'Division Percussion Instrument Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 43, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(638, 'default', 'Division Engineering Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 44, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(639, 'default', 'Division Claims Taker has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 45, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(640, 'default', 'Division Economist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 46, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(641, 'default', 'Division Mathematical Scientist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 47, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(642, 'default', 'Division Computer Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 48, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(643, 'default', 'Division Solderer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 49, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(644, 'default', 'Division Plant Scientist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 50, NULL, NULL, '[]', NULL, '2022-09-28 14:32:08', '2022-09-28 14:32:08'),
(645, 'default', 'Shift Schedule quibusdam has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(646, 'default', 'Shift Schedule autem has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(647, 'default', 'Shift Schedule dolor has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(648, 'default', 'Shift Schedule maxime has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(649, 'default', 'Shift Schedule earum has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(650, 'default', 'Position School Social Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 21, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(651, 'default', 'Position Clinical Laboratory Technician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 22, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(652, 'default', 'Position Library Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 23, NULL, NULL, '[]', NULL, '2022-09-28 14:32:09', '2022-09-28 14:32:09'),
(653, 'default', 'Position Veterinary Technician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 24, NULL, NULL, '[]', NULL, '2022-09-28 14:32:10', '2022-09-28 14:32:10'),
(654, 'default', 'Position Usher has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 25, NULL, NULL, '[]', NULL, '2022-09-28 14:32:10', '2022-09-28 14:32:10'),
(655, 'default', 'Position Fabric Mender has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 26, NULL, NULL, '[]', NULL, '2022-09-28 14:32:10', '2022-09-28 14:32:10'),
(656, 'default', 'Position Warehouse has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 27, NULL, NULL, '[]', NULL, '2022-09-28 14:32:10', '2022-09-28 14:32:10'),
(657, 'default', 'Position Film Laboratory Technician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 28, NULL, NULL, '[]', NULL, '2022-09-28 14:32:10', '2022-09-28 14:32:10'),
(658, 'default', 'Position Nuclear Equipment Operation Technician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 29, NULL, NULL, '[]', NULL, '2022-09-28 14:32:11', '2022-09-28 14:32:11'),
(659, 'default', 'Position Food Servers has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 30, NULL, NULL, '[]', NULL, '2022-09-28 14:32:11', '2022-09-28 14:32:11'),
(660, 'default', 'Division Plant Scientist has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:35:02', '2022-09-28 14:35:02'),
(661, 'default', 'Division Computer Software Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:35:02', '2022-09-28 14:35:02'),
(662, 'default', 'Division Shoe and Leather Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:35:02', '2022-09-28 14:35:02'),
(663, 'default', 'Division Rotary Drill Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(664, 'default', 'Division Veterinarian has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(665, 'default', 'Division Chemistry Teacher has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(666, 'default', 'Division Electronic Engineering Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(667, 'default', 'Division Locker Room Attendant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(668, 'default', 'Division Reservation Agent OR Transportation Ticket Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(669, 'default', 'Division Glazier has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(670, 'default', 'Division Night Security Guard has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 11, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(671, 'default', 'Division Web Developer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 12, NULL, NULL, '[]', NULL, '2022-09-28 14:35:03', '2022-09-28 14:35:03'),
(672, 'default', 'Division Refinery Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 13, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(673, 'default', 'Division Continuous Mining Machine Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 14, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(674, 'default', 'Division Bartender has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 15, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(675, 'default', 'Shift Schedule est has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(676, 'default', 'Shift Schedule doloremque has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(677, 'default', 'Shift Schedule et has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(678, 'default', 'Shift Schedule possimus has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(679, 'default', 'Shift Schedule labore has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:35:04', '2022-09-28 14:35:04'),
(680, 'default', 'Position Ship Mates has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:35:05', '2022-09-28 14:35:05'),
(681, 'default', 'Position Legal Secretary has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:35:05', '2022-09-28 14:35:05'),
(682, 'default', 'Position Substance Abuse Social Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:35:05', '2022-09-28 14:35:05'),
(683, 'default', 'Position Drywall Ceiling Tile Installer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:35:06', '2022-09-28 14:35:06'),
(684, 'default', 'Position Watch Repairer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:35:06', '2022-09-28 14:35:06'),
(685, 'default', 'Position Gaming Dealer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:35:06', '2022-09-28 14:35:06'),
(686, 'default', 'Position Substation Maintenance has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:35:06', '2022-09-28 14:35:06'),
(687, 'default', 'Position Electric Motor Repairer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:35:06', '2022-09-28 14:35:06'),
(688, 'default', 'Position Communication Equipment Worker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:35:06', '2022-09-28 14:35:06'),
(689, 'default', 'Position City Planning Aide has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:35:06', '2022-09-28 14:35:06'),
(690, 'default', 'Salary Grade est has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(691, 'default', 'Salary Grade ut has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(692, 'default', 'Salary Grade laborum has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(693, 'default', 'Salary Grade tempore has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(694, 'default', 'Salary Grade perferendis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(695, 'default', 'Salary Grade nam has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(696, 'default', 'Salary Grade sint has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(697, 'default', 'Salary Grade a has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:35:07', '2022-09-28 14:35:07'),
(698, 'default', 'Salary Grade sit has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:35:08', '2022-09-28 14:35:08'),
(699, 'default', 'Salary Grade repellendus has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:35:08', '2022-09-28 14:35:08'),
(700, 'default', 'Employee Maribel Aufderhar I has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 14:35:10', '2022-09-28 14:35:10'),
(701, 'default', 'Employee Marina Becker has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 14:35:10', '2022-09-28 14:35:10'),
(702, 'default', 'Employee Dr. Fredy Cummings has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 14:35:10', '2022-09-28 14:35:10'),
(703, 'default', 'Employee Madaline Dare has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 14:35:11', '2022-09-28 14:35:11'),
(704, 'default', 'Employee Giovanna Goldner has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 14:35:11', '2022-09-28 14:35:11'),
(705, 'default', 'Employee Ms. Delphine Cronin has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 14:35:11', '2022-09-28 14:35:11'),
(706, 'default', 'Employee Adalberto Parker has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 14:35:11', '2022-09-28 14:35:11'),
(707, 'default', 'Employee Dr. Torey Stanton Jr. has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 14:35:11', '2022-09-28 14:35:11'),
(708, 'default', 'Employee Juston Strosin has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 14:35:11', '2022-09-28 14:35:11'),
(709, 'default', 'Employee Kareem Cremin II has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 14:35:11', '2022-09-28 14:35:11'),
(710, 'default', 'Position Melanie Mccray has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 11, 'App\\Models\\User', 1, '[]', NULL, '2022-09-28 14:36:51', '2022-09-28 14:36:51'),
(711, 'default', 'Position asdfasdf has been updated', 'Modules\\Hrm\\Entities\\Position', 'updated', 1, 'App\\Models\\User', 1, '[]', NULL, '2022-09-28 14:37:23', '2022-09-28 14:37:23'),
(712, 'default', 'Position asdfasdf has been deleted', 'Modules\\Hrm\\Entities\\Position', 'deleted', 1, 'App\\Models\\User', 1, '[]', NULL, '2022-09-28 14:37:28', '2022-09-28 14:37:28'),
(713, 'default', 'Department Xandra Mcclain has been created', 'Modules\\Hrm\\Entities\\Department', 'created', 1, 'App\\Models\\User', 1, '[]', NULL, '2022-09-28 14:37:53', '2022-09-28 14:37:53'),
(714, 'default', 'Department Uriel Olsen has been updated', 'Modules\\Hrm\\Entities\\Department', 'updated', 1, 'App\\Models\\User', 1, '[]', NULL, '2022-09-28 14:38:03', '2022-09-28 14:38:03'),
(715, 'default', 'Department Uriel Olsen has been deleted', 'Modules\\Hrm\\Entities\\Department', 'deleted', 1, 'App\\Models\\User', 1, '[]', NULL, '2022-09-28 14:38:09', '2022-09-28 14:38:09'),
(716, 'default', 'Division Travel Guide has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(717, 'default', 'Division Gas Plant Operator has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(718, 'default', 'Division Aircraft Body Repairer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(719, 'default', 'Division Advertising Sales Agent has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(720, 'default', 'Division Automotive Specialty Technician has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 23:39:59', '2022-09-28 23:39:59'),
(721, 'default', 'Division Sales and Related Workers has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(722, 'default', 'Division Statistical Assistant has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(723, 'default', 'Division Marine Engineer has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(724, 'default', 'Division Management Analyst has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(725, 'default', 'Division Elementary and Secondary School Administrators has been created', 'Modules\\Hrm\\Entities\\Division', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(726, 'default', 'Department Yostton has been created', 'Modules\\Hrm\\Entities\\Department', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 23:40:00', '2022-09-28 23:40:00'),
(727, 'default', 'Department Lake Anselchester has been created', 'Modules\\Hrm\\Entities\\Department', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(728, 'default', 'Department Bechtelartown has been created', 'Modules\\Hrm\\Entities\\Department', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(729, 'default', 'Department West Harmonshire has been created', 'Modules\\Hrm\\Entities\\Department', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(730, 'default', 'Department Wizastad has been created', 'Modules\\Hrm\\Entities\\Department', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(731, 'default', 'Shift Schedule placeat has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 23:40:01', '2022-09-28 23:40:01'),
(732, 'default', 'Shift Schedule ducimus has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(733, 'default', 'Shift Schedule omnis has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(734, 'default', 'Shift Schedule omnis has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(735, 'default', 'Shift Schedule cupiditate has been created', 'Modules\\Hrm\\Entities\\ShiftSchedule', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 23:40:02', '2022-09-28 23:40:02'),
(736, 'default', 'Position Securities Sales Agent has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(737, 'default', 'Position Pastry Chef has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(738, 'default', 'Position Surveying Technician has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(739, 'default', 'Position Soil Scientist has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(740, 'default', 'Position Board Of Directors has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(741, 'default', 'Position Office Machine and Cash Register Servicer has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(742, 'default', 'Position Drilling and Boring Machine Tool Setter has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 23:40:03', '2022-09-28 23:40:03'),
(743, 'default', 'Position General Farmworker has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(744, 'default', 'Position Plate Finisher has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(745, 'default', 'Position Private Sector Executive has been created', 'Modules\\Hrm\\Entities\\Position', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(746, 'default', 'Salary Grade quae has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(747, 'default', 'Salary Grade ut has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 23:40:04', '2022-09-28 23:40:04'),
(748, 'default', 'Salary Grade aut has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(749, 'default', 'Salary Grade consectetur has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(750, 'default', 'Salary Grade et has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(751, 'default', 'Salary Grade dolor has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(752, 'default', 'Salary Grade eaque has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 23:40:05', '2022-09-28 23:40:05'),
(753, 'default', 'Salary Grade quos has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 23:40:06', '2022-09-28 23:40:06'),
(754, 'default', 'Salary Grade perspiciatis has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 23:40:06', '2022-09-28 23:40:06'),
(755, 'default', 'Salary Grade qui has been created', 'Modules\\Hrm\\Entities\\Grade', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 23:40:06', '2022-09-28 23:40:06'),
(756, 'default', 'Employee Gillian Skiles has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 1, NULL, NULL, '[]', NULL, '2022-09-28 23:40:10', '2022-09-28 23:40:10'),
(757, 'default', 'Employee Marilou Heaney V has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 2, NULL, NULL, '[]', NULL, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(758, 'default', 'Employee Dr. Ronaldo Spencer has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 3, NULL, NULL, '[]', NULL, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(759, 'default', 'Employee Miss Maxie Pfeffer has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 4, NULL, NULL, '[]', NULL, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(760, 'default', 'Employee Ms. Christine Murazik has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 5, NULL, NULL, '[]', NULL, '2022-09-28 23:40:11', '2022-09-28 23:40:11'),
(761, 'default', 'Employee Prof. Eric Gulgowski has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 6, NULL, NULL, '[]', NULL, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(762, 'default', 'Employee Dimitri Torphy has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 7, NULL, NULL, '[]', NULL, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(763, 'default', 'Employee Tiara Balistreri has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 8, NULL, NULL, '[]', NULL, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(764, 'default', 'Employee Dr. Vivien Keeling has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 9, NULL, NULL, '[]', NULL, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(765, 'default', 'Employee Burnice Waelchi has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 10, NULL, NULL, '[]', NULL, '2022-09-28 23:40:12', '2022-09-28 23:40:12'),
(766, 'default', 'Employee hello has been created', 'Modules\\Hrm\\Entities\\Employee', 'created', 11, 'App\\Models\\User', 1, '[]', NULL, '2022-09-29 00:37:15', '2022-09-29 00:37:15'),
(767, 'default', 'User Daisy Zieme has been updated', 'App\\Models\\User', 'updated', 1, 'App\\Models\\User', 1, '[]', NULL, '2022-09-29 12:17:35', '2022-09-29 12:17:35'),
(768, 'default', 'User Daisy Zieme has been updated', 'App\\Models\\User', 'updated', 1, 'App\\Models\\User', 1, '[]', NULL, '2022-09-29 13:36:23', '2022-09-29 13:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `symbol`, `name`, `created_at`, `updated_at`) VALUES
(1, '৳', 'Taka', NULL, NULL),
(2, 'Lek', 'Leke', NULL, NULL),
(3, '$', 'Dollars', NULL, NULL),
(4, '؋', 'Afghanis', NULL, NULL),
(5, '$', 'Pesos', NULL, NULL),
(6, 'ƒ', 'Guilders', NULL, NULL),
(7, '$', 'Dollars', NULL, NULL),
(8, 'ман', 'New Manats', NULL, NULL),
(9, '$', 'Dollars', NULL, NULL),
(10, '$', 'Dollars', NULL, NULL),
(11, 'p.', 'Rubles', NULL, NULL),
(12, '€', 'Euro', NULL, NULL),
(13, 'BZ$', 'Dollars', NULL, NULL),
(14, '$', 'Dollars', NULL, NULL),
(15, 'KM', 'Convertible Marka', NULL, NULL),
(16, 'P', 'Pula\'s', NULL, NULL),
(17, 'лв', 'Leva', NULL, NULL),
(18, 'R$', 'Reais', NULL, NULL),
(19, '£', 'Pounds', NULL, NULL),
(20, '$', 'Dollars', NULL, NULL),
(21, '៛', 'Riels', NULL, NULL),
(22, '$', 'Dollars', NULL, NULL),
(23, '$', 'Dollars', NULL, NULL),
(24, '$', 'Pesos', NULL, NULL),
(25, '¥', 'Yuan Renminbi', NULL, NULL),
(26, '$', 'Pesos', NULL, NULL),
(27, '₡', 'Colón', NULL, NULL),
(28, 'kn', 'Kuna', NULL, NULL),
(29, '₱', 'Pesos', NULL, NULL),
(30, '€', 'Euro', NULL, NULL),
(31, 'Kč', 'Koruny', NULL, NULL),
(32, 'kr', 'Kroner', NULL, NULL),
(33, 'RD$', 'Pesos', NULL, NULL),
(34, '$', 'Dollars', NULL, NULL),
(35, '£', 'Pounds', NULL, NULL),
(36, '$', 'Colones', NULL, NULL),
(37, '£', 'Pounds', NULL, NULL),
(38, '€', 'Euro', NULL, NULL),
(39, '£', 'Pounds', NULL, NULL),
(40, '$', 'Dollars', NULL, NULL),
(41, '€', 'Euro', NULL, NULL),
(42, '¢', 'Cedis', NULL, NULL),
(43, '£', 'Pounds', NULL, NULL),
(44, '€', 'Euro', NULL, NULL),
(45, 'Q', 'Quetzales', NULL, NULL),
(46, '£', 'Pounds', NULL, NULL),
(47, '$', 'Dollars', NULL, NULL),
(48, '€', 'Euro', NULL, NULL),
(49, 'L', 'Lempiras', NULL, NULL),
(50, '$', 'Dollars', NULL, NULL),
(51, 'Ft', 'Forint', NULL, NULL),
(52, 'kr', 'Kronur', NULL, NULL),
(53, '₹', 'Rupees', NULL, NULL),
(54, 'Rp', 'Rupiahs', NULL, NULL),
(55, '﷼', 'Rials', NULL, NULL),
(56, '€', 'Euro', NULL, NULL),
(57, '£', 'Pounds', NULL, NULL),
(58, '₪', 'New Shekels', NULL, NULL),
(59, '€', 'Euro', NULL, NULL),
(60, 'J$', 'Dollars', NULL, NULL),
(61, '¥', 'Yen', NULL, NULL),
(62, '£', 'Pounds', NULL, NULL),
(63, 'лв', 'Tenge', NULL, NULL),
(64, '₩', 'Won', NULL, NULL),
(65, '₩', 'Won', NULL, NULL),
(66, 'лв', 'Soms', NULL, NULL),
(67, '₭', 'Kips', NULL, NULL),
(68, 'Ls', 'Lati', NULL, NULL),
(69, '£', 'Pounds', NULL, NULL),
(70, '$', 'Dollars', NULL, NULL),
(71, 'CHF', 'Switzerland Francs', NULL, NULL),
(72, 'Lt', 'Litai', NULL, NULL),
(73, '€', 'Euro', NULL, NULL),
(74, 'ден', 'Denars', NULL, NULL),
(75, 'RM', 'Ringgits', NULL, NULL),
(76, '€', 'Euro', NULL, NULL),
(77, '₨', 'Rupees', NULL, NULL),
(78, '$', 'Pesos', NULL, NULL),
(79, '₮', 'Tugriks', NULL, NULL),
(80, 'MT', 'Meticais', NULL, NULL),
(81, '$', 'Dollars', NULL, NULL),
(82, '₨', 'Rupees', NULL, NULL),
(83, 'ƒ', 'Guilders', NULL, NULL),
(84, '€', 'Euro', NULL, NULL),
(85, '$', 'Dollars', NULL, NULL),
(86, 'C$', 'Cordobas', NULL, NULL),
(87, '₦', 'Nairas', NULL, NULL),
(88, '₩', 'Won', NULL, NULL),
(89, 'kr', 'Krone', NULL, NULL),
(90, '﷼', 'Rials', NULL, NULL),
(91, '₨', 'Rupees', NULL, NULL),
(92, 'B\\/.', 'Balboa', NULL, NULL),
(93, 'Gs', 'Guarani', NULL, NULL),
(94, 'S\\/.', 'Nuevos Soles', NULL, NULL),
(95, 'Php', 'Pesos', NULL, NULL),
(96, 'zł', 'Zlotych', NULL, NULL),
(97, '﷼', 'Rials', NULL, NULL),
(98, 'lei', 'New Lei', NULL, NULL),
(99, 'руб', 'Rubles', NULL, NULL),
(100, '£', 'Pounds', NULL, NULL),
(101, '﷼', 'Riyals', NULL, NULL),
(102, 'Дин.', 'Dinars', NULL, NULL),
(103, '₨', 'Rupees', NULL, NULL),
(104, '$', 'Dollars', NULL, NULL),
(105, '€', 'Euro', NULL, NULL),
(106, '$', 'Dollars', NULL, NULL),
(107, 'S', 'Shillings', NULL, NULL),
(108, 'R', 'Rand', NULL, NULL),
(109, '₩', 'Won', NULL, NULL),
(110, '€', 'Euro', NULL, NULL),
(111, '₨', 'Rupees', NULL, NULL),
(112, 'kr', 'Kronor', NULL, NULL),
(113, 'CHF', 'Francs', NULL, NULL),
(114, '$', 'Dollars', NULL, NULL),
(115, '£', 'Pounds', NULL, NULL),
(116, 'NT$', 'New Dollars', NULL, NULL),
(117, '฿', 'Baht', NULL, NULL),
(118, 'TT$', 'Dollars', NULL, NULL),
(119, 'TL', 'Lira', NULL, NULL),
(120, '£', 'Liras', NULL, NULL),
(121, '$', 'Dollars', NULL, NULL),
(122, '₴', 'Hryvnia', NULL, NULL),
(123, '£', 'Pounds', NULL, NULL),
(124, '$', 'Dollars', NULL, NULL),
(125, 'лв', 'Sums', NULL, NULL),
(126, '€', 'Euro', NULL, NULL),
(127, 'Bs', 'Bolivares Fuertes', NULL, NULL),
(128, '₫', 'Dong', NULL, NULL),
(129, '﷼', 'Rials', NULL, NULL),
(130, 'Z$', 'Zimbabwe Dollars', NULL, NULL),
(131, 'د.ع', 'Iraqi dinar', NULL, NULL),
(132, 'KSh', 'Kenyan shilling', NULL, NULL),
(133, 'د.ج', 'Algerian dinar', NULL, NULL),
(134, 'د.إ', 'United Arab Emirates dirham', NULL, NULL),
(135, 'USh', 'Uganda shillings', NULL, NULL),
(136, 'TSh', 'Tanzanian shilling', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `code`, `flag`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', NULL, NULL, NULL),
(2, 'Bangla', 'bn', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language_phrases`
--

CREATE TABLE `language_phrases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language_id` bigint(20) UNSIGNED NOT NULL,
  `phrase_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language_phrases`
--

INSERT INTO `language_phrases` (`id`, `language_id`, `phrase_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'কার্যাবলী', NULL, NULL),
(2, 2, 2, 'সফলভাবে সংরক্ষিত হয়েছে', NULL, NULL),
(3, 2, 3, 'ভাতা ও সুবিধাদি', NULL, NULL),
(4, 2, 4, 'উপস্থিতি', NULL, NULL),
(5, 2, 5, 'বন্ধ করুন', NULL, NULL),
(6, 2, 6, 'ড্যাশবোর্ড', NULL, NULL),
(7, 2, 7, 'বাতিল', NULL, NULL),
(8, 2, 8, 'কর্মচারী', NULL, NULL),
(9, 2, 9, 'আবার দেখা হবে', NULL, NULL),
(10, 2, 10, 'হোম', NULL, NULL),
(11, 2, 11, 'লিভ', NULL, NULL),
(12, 2, 12, 'ধার', NULL, NULL),
(13, 2, 13, 'প্রবেশ করুন', NULL, NULL),
(14, 2, 14, 'পে-রোল', NULL, NULL),
(15, 2, 15, 'বাক্যাংশ', NULL, NULL),
(16, 2, 16, 'নিবন্ধন করুন', NULL, NULL),
(17, 2, 17, 'রোল এবং পারমিশন', NULL, NULL),
(18, 2, 18, 'সংরক্ষন করুন', NULL, NULL),
(19, 2, 19, 'সেটিংস', NULL, NULL),
(20, 2, 20, 'প্রশিক্ষন', NULL, NULL),
(21, 2, 21, 'ব্যবহারকারীগন', NULL, NULL),
(22, 2, 22, 'কার্য বিবরণ', NULL, NULL),
(23, 2, 23, 'বিভাগ যুক্ত করুন', NULL, NULL),
(24, 2, 24, 'বিভাগ যোগ করুন', NULL, NULL),
(25, 2, 25, 'কর্মচারী যুক্ত করুন', NULL, NULL),
(26, 2, 26, 'গ্রেড যোগ করুন', NULL, NULL),
(27, 2, 27, 'ছুটি যুক্ত করুন', NULL, NULL),
(28, 2, 28, 'বাক্যাংশ যোগ করুন', NULL, NULL),
(29, 2, 29, 'পুনঃনির্ধারণ করুন', NULL, NULL),
(30, 2, 30, 'তফসিল যোগ করুন', NULL, NULL),
(31, 2, 31, 'প্রশিক্ষক যুক্ত করুন', NULL, NULL),
(32, 2, 32, 'প্রকার যোগ করুন', NULL, NULL),
(33, 2, 33, 'কর্মশালা যোগ করুন', NULL, NULL),
(34, 2, 34, 'বিবরন সহ সকল বিভাগ', NULL, NULL),
(35, 2, 35, 'সব বিভাগ', NULL, NULL),
(36, 2, 36, 'সমস্ত কর্মচারী নথি', NULL, NULL),
(37, 2, 37, 'বিবরণ সহ সমস্ত অবস্থান এখানে তালিকাভুক্ত করা হয়েছে', NULL, NULL),
(38, 2, 38, 'সমস্ত পুনঃনির্ধারিত তথ্য', NULL, NULL),
(39, 2, 39, 'সমস্ত বেতন গ্রেড', NULL, NULL),
(40, 2, 40, 'সমস্ত বেতন তথ্য', NULL, NULL),
(41, 2, 41, 'সমস্ত প্রশিক্ষণের ধরণের ইনফোগুলি', NULL, NULL),
(42, 2, 42, 'সমস্ত কর্মশালার তথ্য', NULL, NULL),
(43, 2, 43, 'অ্যাপ্লিকেশন', NULL, NULL),
(44, 2, 44, 'উপস্থিতির সময়গুলি 24 ঘন্টা ভিত্তিক', NULL, NULL),
(45, 2, 45, 'উপস্থিতি', NULL, NULL),
(46, 2, 46, 'নীল', NULL, NULL),
(47, 2, 47, 'পঞ্জিকা', NULL, NULL),
(48, 2, 48, 'চেক ইন', NULL, NULL),
(49, 2, 49, 'চেক আউট', NULL, NULL),
(50, 2, 50, 'শহর', NULL, NULL),
(51, 2, 51, 'দেশ', NULL, NULL),
(52, 2, 52, 'এ নির্মিত', NULL, NULL),
(53, 2, 53, 'মুদ্রা', NULL, NULL),
(54, 2, 54, 'দিন', NULL, NULL),
(55, 2, 55, 'দিনসমূহ', NULL, NULL),
(56, 2, 56, 'বিভাগ', NULL, NULL),
(57, 2, 57, 'বিভাগ', NULL, NULL),
(58, 2, 58, 'কাগজপত্র', NULL, NULL),
(59, 2, 59, 'ইমেইল', NULL, NULL),
(60, 2, 60, 'কর্মচারী আজ অনুপস্থিত', NULL, NULL),
(61, 2, 61, 'বিভাগ দ্বারা কর্মচারী', NULL, NULL),
(62, 2, 62, 'পদবী দ্বারা কর্মচারী', NULL, NULL),
(63, 2, 63, 'কর্মচারী আজ উপস্থিত', NULL, NULL),
(64, 2, 64, 'কর্মীদের তাদের বিবরণ সহ', NULL, NULL),
(65, 2, 65, 'শুক্রবার', NULL, NULL),
(66, 2, 66, 'থেকে', NULL, NULL),
(67, 2, 67, 'লিঙ্গ', NULL, NULL),
(68, 2, 68, 'সাধারণ', NULL, NULL),
(69, 2, 69, 'জেনারেট করুন', NULL, NULL),
(70, 2, 70, 'সাধারণ বেতন', NULL, NULL),
(71, 2, 71, 'বিভাগ বা কর্মচারীদের উপর বেতন তৈরি করুন', NULL, NULL),
(72, 2, 72, 'বিভাগ অথবা কর্মচারী', NULL, NULL),
(73, 2, 73, 'শ্রেনীসমূহ', NULL, NULL),
(74, 2, 74, 'সাধারণ ঋণ', NULL, NULL),
(75, 2, 75, 'সবুজ', NULL, NULL),
(76, 2, 76, 'ছুটি', NULL, NULL),
(77, 2, 77, 'এইচআরএম', NULL, NULL),
(78, 2, 78, 'শৈত্য', NULL, NULL),
(79, 2, 79, 'আইডি', NULL, NULL),
(80, 2, 80, 'কিস্তিতে', NULL, NULL),
(81, 2, 81, 'যোগ দিয়েছে', NULL, NULL),
(82, 2, 82, 'ভাষাসমূহ', NULL, NULL),
(83, 2, 83, 'লম্বা', NULL, NULL),
(84, 2, 84, 'প্রস্থান করুন', NULL, NULL),
(85, 2, 85, 'বেতন পরিচালনা করুন', NULL, NULL),
(86, 2, 86, 'বার্তাবহ', NULL, NULL),
(87, 2, 87, 'সোমবার', NULL, NULL),
(88, 2, 88, 'মাস', NULL, NULL),
(89, 2, 89, 'অধিক', NULL, NULL),
(90, 2, 90, 'নাম', NULL, NULL),
(91, 2, 91, 'পরবর্তী', NULL, NULL),
(92, 2, 92, 'সারণীতে কোনও ডেটা উপলব্ধ নেই', NULL, NULL),
(93, 2, 93, 'কোনও নথি রেকর্ড করা হয়নি', NULL, NULL),
(94, 2, 94, 'নোটিসবোর্ড', NULL, NULL),
(95, 2, 95, 'প্রদত্ত', NULL, NULL),
(96, 2, 96, 'প্রদত্ত', NULL, NULL),
(97, 2, 97, 'ফোন', NULL, NULL),
(98, 2, 98, 'বাক্যাংশ', NULL, NULL),
(99, 2, 99, 'অবস্থান', NULL, NULL),
(100, 2, 100, 'চাপ', NULL, NULL),
(101, 2, 101, 'আগে', NULL, NULL),
(102, 2, 102, 'লাল', NULL, NULL),
(103, 2, 103, 'প্রতিবেদন', NULL, NULL),
(104, 2, 104, 'ভূমিকা', NULL, NULL),
(105, 2, 105, 'রোল এবং পারমিশন', NULL, NULL),
(106, 2, 106, 'রোল এবং পারমিশন', NULL, NULL),
(107, 2, 107, 'শনিবার', NULL, NULL),
(108, 2, 108, 'অনুসন্ধান', NULL, NULL),
(109, 2, 109, 'শিফটে রেসডিউলস', NULL, NULL),
(110, 2, 110, 'শিফট সূচী', NULL, NULL),
(111, 2, 111, 'এন্ট্রি দেখান', NULL, NULL),
(112, 2, 112, '4 টি এন্ট্রি 1 থেকে 4 দেখানো হচ্ছে', NULL, NULL),
(113, 2, 113, 'অবস্থা', NULL, NULL),
(114, 2, 114, 'রবিবার', NULL, NULL),
(115, 2, 115, 'বৃহস্পতিবার', NULL, NULL),
(116, 2, 116, 'প্রতি', NULL, NULL),
(117, 2, 117, 'আজ', NULL, NULL),
(118, 2, 118, 'প্রশিক্ষক', NULL, NULL),
(119, 2, 119, 'মঙ্গলবার', NULL, NULL),
(120, 2, 120, 'প্রকারভেদ', NULL, NULL),
(121, 2, 121, 'ব্যবহারকারীর অনুমতি', NULL, NULL),
(122, 2, 122, 'ব্যবহারকারী ভূমিকা', NULL, NULL),
(123, 2, 123, 'সমস্ত ক্রিয়াকলাপ দেখুন', NULL, NULL),
(124, 2, 124, 'আবহাওয়া প্রতিবেদন', NULL, NULL),
(125, 2, 125, 'বুধবার', NULL, NULL),
(126, 2, 126, 'সপ্তাহ', NULL, NULL),
(127, 2, 127, 'বায়ু', NULL, NULL),
(128, 2, 128, 'কর্মদিবস', NULL, NULL),
(129, 2, 129, 'কর্মঘন্টা', NULL, NULL),
(130, 2, 130, 'ওয়ার্কশপ', NULL, NULL),
(131, 2, 131, 'জিপ কোড', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `marital_infos`
--

CREATE TABLE `marital_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marital_infos`
--

INSERT INTO `marital_infos` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Single', NULL, NULL),
(2, 'Married', NULL, NULL),
(3, 'Divorced', NULL, NULL),
(4, 'Widowed', NULL, NULL),
(5, 'Other', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `created_to` bigint(20) UNSIGNED NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT 0,
  `read_at` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_21_061120_create_marital_infos_table', 1),
(4, '2019_05_21_070346_create_religions_table', 1),
(5, '2019_06_12_055013_create_languages_table', 1),
(6, '2019_06_12_055323_create_currencies_table', 1),
(7, '2019_06_15_073424_create_settings_table', 1),
(8, '2019_07_30_180451_create_phrases_table', 1),
(9, '2019_07_30_184515_create_language_phrases_table', 1),
(10, '2019_08_19_000000_create_failed_jobs_table', 1),
(11, '2019_08_20_122825_create_messages_table', 1),
(12, '2019_08_25_180707_create_notices_table', 1),
(13, '2019_08_25_182035_create_notice_documents_table', 1),
(14, '2019_09_01_123448_create_user_details_table', 1),
(15, '2019_09_01_163041_create_user_addresses_table', 1),
(16, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(17, '2022_09_27_140846_create_permission_tables', 1),
(18, '2022_09_27_141218_create_activity_log_table', 1),
(19, '2022_09_27_141219_add_event_column_to_activity_log_table', 1),
(20, '2022_09_27_141220_add_batch_uuid_column_to_activity_log_table', 1),
(75, '2019_05_25_052858_create_departments_table', 2),
(76, '2019_06_10_063451_create_positions_table', 2),
(77, '2019_06_10_085527_create_divisions_table', 2),
(78, '2019_06_12_045317_create_job_types_table', 2),
(79, '2019_06_12_050210_create_shift_schedules_table', 2),
(80, '2019_06_12_050716_create_holidays_table', 2),
(81, '2019_06_12_052744_create_leave_types_table', 2),
(82, '2019_06_13_095705_create_employees_table', 2),
(83, '2019_06_13_102853_create_employee_addresses_table', 2),
(84, '2019_06_16_102611_create_allowances_table', 2),
(85, '2019_06_25_062753_create_employee_documents_table', 2),
(86, '2019_06_27_093310_create_employee_reschedules_table', 2),
(87, '2019_06_29_053924_create_leave_applications_table', 2),
(88, '2019_06_30_054842_create_attendances_table', 2),
(89, '2019_07_02_070404_create_leave_documents_table', 2),
(90, '2019_07_04_105126_create_grades_table', 2),
(91, '2019_07_04_153430_create_grade_allowances_table', 2),
(92, '2019_07_04_172820_create_employee_officials_table', 2),
(93, '2019_07_06_154234_create_loans_table', 2),
(94, '2019_07_07_160248_create_loan_installments_table', 2),
(95, '2019_07_09_121056_create_salaries_table', 2),
(96, '2019_07_09_122650_create_salary_details_table', 2),
(97, '2019_07_09_122810_create_salary_allowances_table', 2),
(98, '2019_07_24_132116_create_training_types_table', 2),
(99, '2019_07_24_141602_create_trainers_table', 2),
(100, '2019_07_24_172131_create_workshops_table', 2),
(101, '2019_07_24_175656_create_employee_workshops_table', 2),
(102, '2019_09_30_124603_create_client_groups_table', 3),
(103, '2019_10_03_112006_create_clients_table', 3),
(104, '2019_10_03_171804_create_client_contacts_table', 3),
(105, '2019_10_12_172122_create_unit_types_table', 3),
(106, '2019_10_13_104435_create_items_table', 3),
(107, '2019_10_14_110653_create_task_statuses_table', 3),
(108, '2019_11_07_175229_create_client_projects_table', 3),
(109, '2019_11_07_175637_create_client_project_tags_table', 3),
(110, '2019_11_09_161327_create_project_statuses_table', 3),
(111, '2019_11_11_112647_create_project_tasks_table', 3),
(112, '2019_11_16_105519_create_project_members_table', 3),
(113, '2019_11_17_105207_create_payment_methods_table', 3),
(114, '2019_11_17_153055_create_invoices_table', 3),
(115, '2019_11_18_151754_create_payments_table', 3),
(116, '2019_11_20_113244_create_taxes_table', 3),
(117, '2019_11_20_145903_create_estimates_table', 3),
(118, '2019_11_23_153609_create_estimated_items_table', 3),
(119, '2019_12_02_164213_create_invoice_items_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 1),
(4, 'App\\Models\\User', 1),
(5, 'App\\Models\\User', 1),
(6, 'App\\Models\\User', 1),
(7, 'App\\Models\\User', 1),
(8, 'App\\Models\\User', 1),
(9, 'App\\Models\\User', 1),
(10, 'App\\Models\\User', 1),
(11, 'App\\Models\\User', 1),
(12, 'App\\Models\\User', 1),
(13, 'App\\Models\\User', 1),
(14, 'App\\Models\\User', 1),
(15, 'App\\Models\\User', 1),
(16, 'App\\Models\\User', 1),
(17, 'App\\Models\\User', 1),
(18, 'App\\Models\\User', 1),
(19, 'App\\Models\\User', 1),
(20, 'App\\Models\\User', 1),
(21, 'App\\Models\\User', 1),
(22, 'App\\Models\\User', 1),
(23, 'App\\Models\\User', 1),
(24, 'App\\Models\\User', 1),
(25, 'App\\Models\\User', 1),
(26, 'App\\Models\\User', 1),
(27, 'App\\Models\\User', 1),
(28, 'App\\Models\\User', 1),
(29, 'App\\Models\\User', 1),
(30, 'App\\Models\\User', 1),
(31, 'App\\Models\\User', 1),
(32, 'App\\Models\\User', 1),
(33, 'App\\Models\\User', 1),
(34, 'App\\Models\\User', 1),
(35, 'App\\Models\\User', 1),
(36, 'App\\Models\\User', 1),
(37, 'App\\Models\\User', 1),
(38, 'App\\Models\\User', 1),
(39, 'App\\Models\\User', 1),
(40, 'App\\Models\\User', 1),
(41, 'App\\Models\\User', 1),
(42, 'App\\Models\\User', 1),
(43, 'App\\Models\\User', 1),
(44, 'App\\Models\\User', 1),
(45, 'App\\Models\\User', 1),
(46, 'App\\Models\\User', 1),
(47, 'App\\Models\\User', 1),
(48, 'App\\Models\\User', 1),
(49, 'App\\Models\\User', 1),
(50, 'App\\Models\\User', 1),
(51, 'App\\Models\\User', 1),
(52, 'App\\Models\\User', 1),
(53, 'App\\Models\\User', 1),
(54, 'App\\Models\\User', 1),
(55, 'App\\Models\\User', 1),
(56, 'App\\Models\\User', 1),
(57, 'App\\Models\\User', 1),
(58, 'App\\Models\\User', 1),
(59, 'App\\Models\\User', 1),
(60, 'App\\Models\\User', 1),
(61, 'App\\Models\\User', 1),
(62, 'App\\Models\\User', 1),
(63, 'App\\Models\\User', 1),
(64, 'App\\Models\\User', 1),
(65, 'App\\Models\\User', 1),
(66, 'App\\Models\\User', 1),
(67, 'App\\Models\\User', 1),
(68, 'App\\Models\\User', 1),
(69, 'App\\Models\\User', 1),
(70, 'App\\Models\\User', 1),
(71, 'App\\Models\\User', 1),
(72, 'App\\Models\\User', 1),
(73, 'App\\Models\\User', 1),
(74, 'App\\Models\\User', 1),
(75, 'App\\Models\\User', 1),
(76, 'App\\Models\\User', 1),
(77, 'App\\Models\\User', 1),
(78, 'App\\Models\\User', 1),
(79, 'App\\Models\\User', 1),
(80, 'App\\Models\\User', 1),
(81, 'App\\Models\\User', 1),
(82, 'App\\Models\\User', 1),
(83, 'App\\Models\\User', 1),
(84, 'App\\Models\\User', 1),
(85, 'App\\Models\\User', 1),
(86, 'App\\Models\\User', 1),
(87, 'App\\Models\\User', 1),
(88, 'App\\Models\\User', 1),
(89, 'App\\Models\\User', 1),
(90, 'App\\Models\\User', 1),
(91, 'App\\Models\\User', 1),
(92, 'App\\Models\\User', 1),
(93, 'App\\Models\\User', 1),
(94, 'App\\Models\\User', 1),
(95, 'App\\Models\\User', 1),
(96, 'App\\Models\\User', 1),
(97, 'App\\Models\\User', 1),
(98, 'App\\Models\\User', 1),
(99, 'App\\Models\\User', 1),
(100, 'App\\Models\\User', 1),
(101, 'App\\Models\\User', 1),
(102, 'App\\Models\\User', 1),
(103, 'App\\Models\\User', 1),
(104, 'App\\Models\\User', 1),
(105, 'App\\Models\\User', 1),
(106, 'App\\Models\\User', 1),
(107, 'App\\Models\\User', 1),
(108, 'App\\Models\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notice_documents`
--

CREATE TABLE `notice_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notice_id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'hrm-position-view', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(2, 'hrm-position-add', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(3, 'hrm-position-edit', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(4, 'hrm-position-delete', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(5, 'hrm-department-view', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(6, 'hrm-department-add', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(7, 'hrm-department-edit', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(8, 'hrm-department-delete', 'web', '2022-09-29 12:12:27', '2022-09-29 12:12:27'),
(9, 'hrm-division-view', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(10, 'hrm-division-add', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(11, 'hrm-division-edit', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(12, 'hrm-division-delete', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(13, 'hrm-employee-view', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(14, 'hrm-employee-add', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(15, 'hrm-employee-edit', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(16, 'hrm-employee-delete', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(17, 'hrm-training-type-view', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(18, 'hrm-training-type-add', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(19, 'hrm-training-type-edit', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(20, 'hrm-training-type-delete', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(21, 'hrm-trainer-view', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(22, 'hrm-trainer-add', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(23, 'hrm-trainer-edit', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(24, 'hrm-trainer-delete', 'web', '2022-09-29 12:12:28', '2022-09-29 12:12:28'),
(25, 'hrm-workshop-view', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(26, 'hrm-workshop-add', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(27, 'hrm-workshop-edit', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(28, 'hrm-workshop-delete', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(29, 'hrm-salary-grade-view', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(30, 'hrm-salary-grade-add', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(31, 'hrm-salary-grade-edit', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(32, 'hrm-salary-grade-delete', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(33, 'hrm-salary-view', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(34, 'hrm-salary-add', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(35, 'hrm-salary-print', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(36, 'hrm-salary-delete', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(37, 'hrm-salary-pay', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(38, 'hrm-attendance-view', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(39, 'hrm-attendance-add', 'web', '2022-09-29 12:12:29', '2022-09-29 12:12:29'),
(40, 'hrm-attendance-edit', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(41, 'hrm-attendance-delete', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(42, 'hrm-shift-view', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(43, 'hrm-shift-add', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(44, 'hrm-shift-edit', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(45, 'hrm-shift-delete', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(46, 'hrm-reshift-view', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(47, 'hrm-reshift-add', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(48, 'hrm-reshift-edit', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(49, 'hrm-reshift-delete', 'web', '2022-09-29 12:12:30', '2022-09-29 12:12:30'),
(50, 'hrm-holiday-view', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(51, 'hrm-holiday-add', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(52, 'hrm-holiday-edit', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(53, 'hrm-holiday-delete', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(54, 'hrm-leave-type-view', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(55, 'hrm-leave-type-add', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(56, 'hrm-leave-type-edit', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(57, 'hrm-leave-type-delete', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(58, 'hrm-leave-application-view', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(59, 'hrm-leave-application-add', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(60, 'hrm-leave-application-edit', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(61, 'hrm-leave-application-delete', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(62, 'hrm-allowance-view', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(63, 'hrm-allowance-add', 'web', '2022-09-29 12:12:31', '2022-09-29 12:12:31'),
(64, 'hrm-allowance-edit', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(65, 'hrm-allowance-delete', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(66, 'hrm-loan-view', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(67, 'hrm-loan-add', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(68, 'hrm-loan-edit', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(69, 'hrm-loan-delete', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(70, 'hrm-loan-installment-view', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(71, 'hrm-loan-installment-add', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(72, 'hrm-loan-installment-edit', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(73, 'hrm-loan-installment-delete', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(74, 'hrm-report-loan', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(75, 'hrm-notice-view', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(76, 'hrm-notice-add', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(77, 'hrm-notice-edit', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(78, 'hrm-notice-delete', 'web', '2022-09-29 12:12:32', '2022-09-29 12:12:32'),
(79, 'hrm-log-view', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(80, 'hrm-user-view', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(81, 'hrm-user-add', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(82, 'hrm-user-edit', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(83, 'hrm-user-delete', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(84, 'hrm-user-status', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(85, 'hrm-role-view', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(86, 'hrm-role-add', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(87, 'hrm-role-edit', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(88, 'hrm-role-delete', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(89, 'hrm-user-role-view', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(90, 'hrm-user-role-edit', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(91, 'hrm-user-role-delete', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(92, 'hrm-permission-view', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(93, 'hrm-permission-edit', 'web', '2022-09-29 12:12:33', '2022-09-29 12:12:33'),
(94, 'hrm-settings-view', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(95, 'hrm-settings-edit', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(96, 'hrm-language-view', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(97, 'hrm-language-add', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(98, 'hrm-language-edit', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(99, 'hrm-language-delete', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(100, 'hrm-language-phrase', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(101, 'hrm-phrase-view', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(102, 'hrm-phrase-add', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(103, 'hrm-phrase-edit', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(104, 'hrm-phrase-delete', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(105, 'hrm-currency-view', 'web', '2022-09-29 12:12:34', '2022-09-29 12:12:34'),
(106, 'hrm-currency-add', 'web', '2022-09-29 12:12:35', '2022-09-29 12:12:35'),
(107, 'hrm-currency-edit', 'web', '2022-09-29 12:12:35', '2022-09-29 12:12:35'),
(108, 'hrm-currency-delete', 'web', '2022-09-29 12:12:35', '2022-09-29 12:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phrases`
--

CREATE TABLE `phrases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phrases`
--

INSERT INTO `phrases` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Activities', NULL, NULL),
(2, 'Added Successfully', NULL, NULL),
(3, 'Allowance', NULL, NULL),
(4, 'Attendance', NULL, NULL),
(5, 'Close', NULL, NULL),
(6, 'Dashboard', NULL, NULL),
(7, 'Delete', NULL, NULL),
(8, 'Employee', NULL, NULL),
(9, 'Happy to see you again', NULL, NULL),
(10, 'Home', NULL, NULL),
(11, 'Leave', NULL, NULL),
(12, 'Loan', NULL, NULL),
(13, 'Login', NULL, NULL),
(14, 'Payroll', NULL, NULL),
(15, 'Phrases', NULL, NULL),
(16, 'Register', NULL, NULL),
(17, 'Role and Permission', NULL, NULL),
(18, 'Save', NULL, NULL),
(19, 'Settings', NULL, NULL),
(20, 'Training', NULL, NULL),
(21, 'Users', NULL, NULL),
(22, 'Activity Log', NULL, NULL),
(23, 'Add Department', NULL, NULL),
(24, 'Add Division', NULL, NULL),
(25, 'Add Employee', NULL, NULL),
(26, 'Add Grade', NULL, NULL),
(27, 'Add Holiday', NULL, NULL),
(28, 'add phrase', NULL, NULL),
(29, 'Add Reschedule', NULL, NULL),
(30, 'Add Schedule', NULL, NULL),
(31, 'Add Trainer', NULL, NULL),
(32, 'Add Type', NULL, NULL),
(33, 'Add Workshop', NULL, NULL),
(34, 'All departments with their description', NULL, NULL),
(35, 'All divisions', NULL, NULL),
(36, 'All Employee Documents', NULL, NULL),
(37, 'All positions with description are enlisted here', NULL, NULL),
(38, 'All Reschedule information', NULL, NULL),
(39, 'All Salary grades', NULL, NULL),
(40, 'All Salary information', NULL, NULL),
(41, 'All Training type infos', NULL, NULL),
(42, 'All workshop informations', NULL, NULL),
(43, 'Applications', NULL, NULL),
(44, 'Attendance times are based on 24 Hours', NULL, NULL),
(45, 'Attendances', NULL, NULL),
(46, 'Blue', NULL, NULL),
(47, 'Calender', NULL, NULL),
(48, 'Check In', NULL, NULL),
(49, 'Check Out', NULL, NULL),
(50, 'City', NULL, NULL),
(51, 'Country', NULL, NULL),
(52, 'Created at', NULL, NULL),
(53, 'Currency', NULL, NULL),
(54, 'Day', NULL, NULL),
(55, 'Days', NULL, NULL),
(56, 'Department', NULL, NULL),
(57, 'Division', NULL, NULL),
(58, 'Documents', NULL, NULL),
(59, 'Email', NULL, NULL),
(60, 'Employee absent today', NULL, NULL),
(61, 'Employee by department', NULL, NULL),
(62, 'Employee by designation', NULL, NULL),
(63, 'Employee present today', NULL, NULL),
(64, 'Employees with their details', NULL, NULL),
(65, 'Friday', NULL, NULL),
(66, 'From', NULL, NULL),
(67, 'Gender', NULL, NULL),
(68, 'General', NULL, NULL),
(69, 'Generate at', NULL, NULL),
(70, 'Generate Salary', NULL, NULL),
(71, 'Generate salary on specific department', NULL, NULL),
(72, 'division or employees', NULL, NULL),
(73, 'Grades', NULL, NULL),
(74, 'Grant Loan', NULL, NULL),
(75, 'Green', NULL, NULL),
(76, 'Holidays', NULL, NULL),
(77, 'HRM', NULL, NULL),
(78, 'Humidity', NULL, NULL),
(79, 'ID', NULL, NULL),
(80, 'Installments', NULL, NULL),
(81, 'Joined', NULL, NULL),
(82, 'Languages', NULL, NULL),
(83, 'Length', NULL, NULL),
(84, 'Logout', NULL, NULL),
(85, 'Manage Salary', NULL, NULL),
(86, 'Messenger', NULL, NULL),
(87, 'Monday', NULL, NULL),
(88, 'Month', NULL, NULL),
(89, 'More', NULL, NULL),
(90, 'Name', NULL, NULL),
(91, 'Next', NULL, NULL),
(92, 'No data available in table', NULL, NULL),
(93, 'No Documents Recorded', NULL, NULL),
(94, 'Notice Board', NULL, NULL),
(95, 'Paid at', NULL, NULL),
(96, 'Paid By', NULL, NULL),
(97, 'Phone', NULL, NULL),
(98, 'phrase', NULL, NULL),
(99, 'Position', NULL, NULL),
(100, 'Pressure', NULL, NULL),
(101, 'Previous', NULL, NULL),
(102, 'red', NULL, NULL),
(103, 'Report', NULL, NULL),
(104, 'Role', NULL, NULL),
(105, 'Role & Permission', NULL, NULL),
(106, 'Role Permission', NULL, NULL),
(107, 'Saturday', NULL, NULL),
(108, 'Search', NULL, NULL),
(109, 'Shift Reschedules', NULL, NULL),
(110, 'Shift Schedules', NULL, NULL),
(111, 'Show entries', NULL, NULL),
(112, 'Showing 1 to 4 of 4 entries', NULL, NULL),
(113, 'Status', NULL, NULL),
(114, 'Sunday', NULL, NULL),
(115, 'Thursday', NULL, NULL),
(116, 'To', NULL, NULL),
(117, 'Today', NULL, NULL),
(118, 'Trainers', NULL, NULL),
(119, 'Tuesday', NULL, NULL),
(120, 'Types', NULL, NULL),
(121, 'User Permission', NULL, NULL),
(122, 'User Roles', NULL, NULL),
(123, 'View all activities', NULL, NULL),
(124, 'Weather Report', NULL, NULL),
(125, 'Wednesday', NULL, NULL),
(126, 'Week', NULL, NULL),
(127, 'Wind', NULL, NULL),
(128, 'Working Days', NULL, NULL),
(129, 'Working Hours', NULL, NULL),
(130, 'Workshops', NULL, NULL),
(131, 'Zip Code', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Islam', NULL, NULL),
(2, 'Cristian', NULL, NULL),
(3, 'Buddhist', NULL, NULL),
(4, 'Hindu', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'web', '2022-09-28 13:04:46', '2022-09-28 13:04:46');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `option`, `value`, `created_at`, `updated_at`) VALUES
(1, 'title', 'HRM', NULL, NULL),
(2, 'address', 'New Market', NULL, NULL),
(3, 'email', 'hrm@erp.com', NULL, NULL),
(4, 'phone', '123456789', NULL, NULL),
(5, 'favicon', '', NULL, NULL),
(6, 'logo', '', NULL, NULL),
(7, 'language', '1', NULL, NULL),
(8, 'currency', '1', NULL, NULL),
(9, 'login', '', NULL, NULL),
(10, 'register', '', NULL, NULL),
(11, 'footer', '<span class=\"text-center text-sm-left d-md-inline-block\">Copyright © 2018 M3 v1.0. All Rights Reserved.</span>\r\n        <span class=\"float-none float-sm-right mt-1 mt-sm-0 text-center\">\r\n            Crafted with <i class=\"fa fa-heart text-danger\"></i> by <a href=\"javascript:void(0)\" class=\"text-dark\" target=\"_blank\">M3</a></span>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT 0,
  `details_id` int(11) DEFAULT NULL,
  `details_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `is_approved`, `details_id`, `details_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Daisy Zieme', 'admin@gmail.com', '2022-09-28 13:04:46', '$2y$10$YG9JYXr2rHGf2uygwvgQLuOahaG3esYlZB4vRWJGxCU1p58Qoob3a', 1, NULL, NULL, 'Qq41Pv4xR6hO9B0dQUCvIardLqW7ESJyubNCmXwrE5ld36XXMdGIZ9NTPMHX', '2022-09-28 13:04:46', '2022-09-28 13:04:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_details_id` bigint(20) UNSIGNED NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT 0,
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_id` bigint(20) UNSIGNED NOT NULL,
  `religion_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject` (`subject_type`,`subject_id`),
  ADD KEY `causer` (`causer_type`,`causer_id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language_phrases`
--
ALTER TABLE `language_phrases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_phrases_language_id_foreign` (`language_id`),
  ADD KEY `language_phrases_phrase_id_foreign` (`phrase_id`);

--
-- Indexes for table `marital_infos`
--
ALTER TABLE `marital_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notices_created_by_foreign` (`created_by`);

--
-- Indexes for table `notice_documents`
--
ALTER TABLE `notice_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notice_documents_notice_id_foreign` (`notice_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `phrases`
--
ALTER TABLE `phrases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_details_id_foreign` (`user_details_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_details_phone_unique` (`phone`),
  ADD KEY `user_details_marital_id_foreign` (`marital_id`),
  ADD KEY `user_details_religion_id_foreign` (`religion_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=769;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `language_phrases`
--
ALTER TABLE `language_phrases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `marital_infos`
--
ALTER TABLE `marital_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notice_documents`
--
ALTER TABLE `notice_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phrases`
--
ALTER TABLE `phrases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `language_phrases`
--
ALTER TABLE `language_phrases`
  ADD CONSTRAINT `language_phrases_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `language_phrases_phrase_id_foreign` FOREIGN KEY (`phrase_id`) REFERENCES `phrases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notices`
--
ALTER TABLE `notices`
  ADD CONSTRAINT `notices_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `notice_documents`
--
ALTER TABLE `notice_documents`
  ADD CONSTRAINT `notice_documents_notice_id_foreign` FOREIGN KEY (`notice_id`) REFERENCES `notices` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_user_details_id_foreign` FOREIGN KEY (`user_details_id`) REFERENCES `user_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_marital_id_foreign` FOREIGN KEY (`marital_id`) REFERENCES `marital_infos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_details_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
