<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\NoticeDocument;
use Faker\Generator as Faker;

$factory->define(NoticeDocument::class, function (Faker $faker) {
    return [
        'file' => $faker->file(public_path('demo'), public_path('storage\notices'), false),
    ];
});
