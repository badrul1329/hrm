<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\User;
use App\Models\Notice;
use Faker\Generator as Faker;

$factory->define(Notice::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence,
        'desc'=>$faker->text,
        'created_by'=> User::all()->random()
    ];
});
