@extends('layouts.auth')

@section('title')
    {{__('Reset Password')}}
@endsection

@section('content')
{{--    content start--}}
    <div class="container-fluid h-100">
        <div class="row flex-row h-100 bg-white">
            <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
                {{--background image--}}
                <div class="lavalite-bg"
                     style="background-image: url({{$settings['login'] ? asset('storage/'.$settings['login']):asset('resources/login-bg.jpg')}})">
                    <div class="lavalite-overlay"></div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
                <div class="authentication-form mx-auto">
                    <div class="text-center mb-4">
                        {{--logo--}}
                        <a href="{{url('/')}}"><img
                                src="{{$settings['logo'] ? asset('storage/'.$settings['logo']):asset('resources/logo.png')}}"
                                alt="" class="img-fluid" ></a>
                    </div>
                    <h3>{{__('Forgot Password')}}</h3>
                    <p>{{__('We will send you a link to reset password')}}.</p>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{--Form Start--}}
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                   value="{{ old('email') }}" placeholder="{{__('email address')}}" required
                                   autocomplete="email" autofocus>
                            <i class="ik ik-mail"></i>
                        </div>
                        <div class="sign-btn text-center">
                            <button type="submit" class="btn btn-theme">{{__('Submit')}}</button>
                        </div>
                    </form>
                    {{--Form End--}}
                    <div class="register">
                        <p>Not a member? <a href="{{route('register')}}">{{__('Create an account')}}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    content end--}}
@endsection
