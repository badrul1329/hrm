@extends('layouts.auth')

@section('title')
{{__('Login')}}
@endsection

@section('content')
{{--    content start--}}
    <div class="container-fluid h-100">
        <div class="row flex-row h-100 bg-white">
            <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
{{--                Background Image--}}
                <div class="lavalite-bg" style="background-image: url({{$settings['login'] ? asset('storage/'.$settings['login']):asset('resources/login-bg.jpg')}})">
                    <div class="lavalite-overlay"></div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
                <div class="authentication-form mx-auto">
                    <div class="text-center mb-4">
{{--                        Logo--}}
                        <a href="{{url('/')}}"><img src="{{$settings['logo'] ? asset('storage/'.$settings['logo']):asset('resources/logo.png')}}" class="img-fluid" alt=""></a>
                    </div>
                    <h3 class="text-center">{{__('Sign In to '.config('app.name', 'Laravel'))}}</h3>
                    <p>{{__('Happy to see you again')}}!</p>
{{--                    Login form start--}}
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                   placeholder="{{__('Email')}}" name="email" value="{{ old('email') }}" required
                                   autocomplete="email" autofocus>
                            <i class="ik ik-user"></i>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ __("$message") }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   placeholder="{{__('Password')}}" name="password" required autocomplete="current-password">
                            <i class="ik ik-lock"></i>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ __("$message") }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col text-left">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="item_checkbox"
                                           name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="custom-control-label">&nbsp;{{__('Remember Me')}}</span>
                                </label>
                            </div>
                            @if (Route::has('password.request'))
                                <div class="col text-right">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Password?') }}
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="sign-btn text-center">
                            <button type="submit" class="btn btn-theme">{{__('Sign In')}}</button>
                        </div>
                    </form>
{{--                    Login form end--}}
                    <div class="register">
                        <p>{{__('Don\'t have an account')}}? <a class="" href="{{route('register')}}">{{__('Create an account')}}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    content end--}}
@endsection
