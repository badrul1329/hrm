@extends('master')
@section('title')
{{__('Home')}}
@endsection
@section('content')

@endsection
@section('extraJS')
    <script>
        $('#appsModal').modal({
            // 'backdrop' : 'static',
            'keyboard' : false,
            'show' : true,
            'focus' : true
        });
    </script>
@endsection
@section('extraCSS')
    <style>
        .main-content{
            padding-left : 0;
        }
        .modal{
            position:static;
        }
    </style>
@endsection
