@extends('master')
@section('title')
    {{__('Settings')}}
@endsection
@section('content')
    {{--    content start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Settings')}}</h5>
                            <span>{{__('Settings parameters collection')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Settings')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('General')}} </h3>
                    </div>
                    <div class="card-body">
                        {{--                        settings form start--}}
                        <form class="forms-sample" action="javascript:void(0)" method="post"
                              enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="title" class="col-sm-3 col-form-label">{{__('Title')}}</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="title" name="title"
                                           placeholder="{{__('Title')}}"
                                           value="{{$settings['title'] ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-sm-3 col-form-label">{{__('Address')}}</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="address" rows="4"
                                              placeholder="{{__('Address here')}} ..."
                                              name="address">{{$settings['address'] ?? ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">{{__('Email')}}</label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="{{__('Email')}}"
                                           value="{{$settings['email'] ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-sm-3 col-form-label">{{__('Mobile')}}</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="phone" name="phone"
                                           placeholder="{{__('Mobile number')}}" value="{{$settings['phone'] ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="favicon" class="col-sm-3 col-form-label">{{__('Favicon')}}</label>
                                <div class="col-sm-6">
                                    <input type="file" name="favicon" id="favicon">
                                    @if($settings['favicon'])
                                        <img src="{{asset('storage/'.$settings['favicon'])}}"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @else
                                        <img src="{{asset('resources')}}/favicon.ico"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="logo" class="col-sm-3 col-form-label">{{__('Logo')}}</label>
                                <div class="col-sm-6">
                                    <input type="file" name="logo" id="logo">
                                    @if($settings['logo'])
                                        <img src="{{asset('storage/'.$settings['logo'] ?? '')}}"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @else
                                        <img src="{{asset('resources')}}/logo.png"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="language">{{__('Language')}}</label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" id="language" name="language">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="currency">{{__('Currency')}}</label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" id="currency" name="currency">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="favicon" class="col-sm-3 col-form-label">{{__('Login cover')}}</label>
                                <div class="col-sm-6">
                                    <input type="file" name="login" id="login">
                                    @if($settings['login'])
                                        <img src="{{asset('storage/'.$settings['login'])}}"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @else
                                        <img src="{{asset('resources')}}/login-bg.jpg"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="logo" class="col-sm-3 col-form-label">{{__('Register Cover')}}</label>
                                <div class="col-sm-6">
                                    <input type="file" name="register" id="register">
                                    @if($settings['register'])
                                        <img src="{{asset('storage/'.$settings['register'] ?? '')}}"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @else
                                        <img src="{{asset('resources')}}/register-bg.jpg"
                                             class="img-thumbnail" height="100" width="100"/>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="title" class="col-sm-3 col-form-label">{{__('Footer')}}</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" rows="5" name="footer"
                                              placeholder="{{__('Footer here')}}...">{{$settings['footer'] ?? ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3 mx-auto">
                                    <button type="reset" class="btn btn-info mr-2">{{__('Reset')}}</button>
                                    @can('hrm-settings-edit')
                                        <button type="submit" class="btn btn-primary mr-2">{{__('Update')}}</button>
                                    @endcan
                                    <button class="btn btn-light">{{__('Cancel')}}</button>
                                </div>
                            </div>
                        </form>
                        {{--                        settings form end--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    content end--}}
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            // show file thumbnail
            $(document).on('change', 'input[type=file]', function (event) {
                var that = $(this);
                var reader = new FileReader();
                reader.onload = function () {
                    var img = that.closest('.form-group').find('img');
                    img.attr('src', reader.result);
                };
                reader.readAsDataURL(event.target.files[0]);
            });

            $('.select2').select2();

            // load Language list
            loadLanguage();

            function loadLanguage() {
                $.ajax({
                    url: '{{route('language.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#language').empty();
                        $('#language').append('<option selected value="" disabled>{{__("Select a language")}}</option>');
                        $.each(response, function (i, language) {
                            $('#language').append($('<option>', {
                                value: language.id,
                                text: language.name
                            }));
                        });
                        $('#language').val(`{{$settings['language']}}`).trigger('change');
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                })
            }

            // load currency list
            loadCurrency();

            function loadCurrency() {
                $.ajax({
                    url: '{{route('currency.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#currency').empty();
                        $('#currency').append('<option selected value="" disabled>{{__("Select a Currency")}}</option>');
                        $.each(response, function (i, currency) {
                            $('#currency').append($('<option>', {
                                value: currency.id,
                                text: `${currency.name} (${currency.symbol})`
                            }));
                        });
                        $('#currency').val(`{{$settings['currency']}}`).trigger('change');
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            // form submit listener
            $('form').on('submit', function (e) {
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{route('settings.store')}}',
                    type: 'POST',
                    data: formData,
                    async: false,
                    contentType: false,
                    processData: false,
                    cache: false,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Updated Successfully")}}');
                        showInfoToast('{{__("Change will effect after refresh")}}');
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

