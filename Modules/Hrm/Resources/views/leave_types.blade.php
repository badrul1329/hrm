@extends('hrm::master')
@section('title')
{{__('Type')}}
@endsection
@section('content')
{{--    content start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Types')}}</h5>
                            <span>{{__('All leave types with details')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Leave')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Types')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('Types')}} </h3>
{{--                        Add button--}}
                        @if(Auth::user()->can('hrm-leave-type-add'))
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#typeModal" data-whatever="0"> + {{__('Add Type')}}
                            </button>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
{{--                            Leave type list start--}}
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Type')}}</th>
                                    <th>{{__('Days')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
{{--                            leave type list end--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Type Modal -->
    <div class="modal animated zoomIn faster" id="typeModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Type')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="departmentForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('New Type')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('name here')}}" name="name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Days')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('days here')}}" name="days" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
{{--    content end--}}
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('leave.types.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'days', name: 'days'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            // Reset form
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
            });
            // Show modal
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes

                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('Edit Type');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('leave.types.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('input[name=name]').val(response.name);
                            modal.find('input[name=days]').val(response.days);
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Type")}}');
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });
            // Save event listener
            $(document).on('click', '.save-btn', function () {
                let data = $('form').serialize();
                $.ajax({
                    url: '{{route('leave.types.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#typeModal').modal('toggle');
                        $('form').trigger("reset");
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Update event listener
            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('leave.types.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#typeModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Delete event listener
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                var id = $(this).data('id');
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                $.ajax({
                    url: '{{ route('leave.types.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

