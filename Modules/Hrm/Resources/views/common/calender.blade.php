@extends('hrm::master')
@section('title')
    {{__('Calender')}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h3>{{__('Calender')}}</h3></div>
                <div class="card-body">
                    {{--                    Calender--}}
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extraCSS')
{{--    calender--}}
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/fullcalendar/dist/fullcalendar.min.css">
@endsection
@section('extraJS')
{{--    calender--}}
    <script src="{{asset('resources')}}/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="{{asset('resources')}}/js/calendar.js"></script>
@endsection

