@extends('hrm::master')
@section('title')
    {{__('Salary')}}
@endsection
@section('content')
    {{--    Content start--}}
    <div class="container-fluid">
        <div class="page-header d-print-none">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    {{--                    <div class="page-header-title">--}}
                    {{--                        <i class="ik ik-inbox bg-blue"></i>--}}
                    {{--                        <div class="d-inline">--}}
                    {{--                            <h5>{{$salary->employee->name}}</h5>--}}
                    {{--                            <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                <div class="col-lg-4">
                    <a class="float-right print-btn"><i class="ik ik-printer ik-4x text-red"></i></a>
                </div>
            </div>
        </div>
        @php
            $salaryAmount=0
        @endphp
        <div class="row">
            <div class="col-sm-12">
                {{--                Salary details card--}}
                <div class="card">
                    <div class="card-header">
                        <h3 class="d-block w-100">
                            @if($settings['logo'])
                                <img src="{{url('storage',$settings['logo'])}}" class="header-brand-img img-fluid" alt="lavalite" width="100" height="50">
                            @else
                                <img src="{{asset('resources')}}/logo.png" class="header-brand-img img-fluid" alt="lavalite"  width="100" height="50">
                            @endif
                            <span
                                class="text ml-2"> {{$settings['title'] ?? config('app.name', 'Laravel')}} </span>
                            <small class="float-right">{{__('Date')}}
                                : {{\Carbon\Carbon::now()->format('d F,Y')}}</small>
                        </h3>
                    </div>
                    <div class="card-body ">
                        <div class="salaryinfo mx-md-5">
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
{{--                                    Phofile photo--}}
                                    @if($salary->employee->photo)
                                        <img src="{{asset('storage')}}/hrm/employee/photo/{{$salary->employee->photo}}"
                                             class="rounded-circle m-auto d-block"
                                             height="150" width="150"/>
                                    @else
                                        <img src="{{asset('resources')}}/img/user.jpg"
                                             class="rounded-circle m-auto d-block"
                                             height="150" width="150"/>
                                    @endif
                                </div>
                                <div class="col-md-8 col-sm-12">
                                    <h4 class="mt-0 name">{{$salary->employee->name}}</h4>
                                    <hr/>
                                    <dl class="row">
                                        <dt class="col-sm-3">{{__('Employee ID')}}</dt>
                                        <dd class="col-sm-9 employeeId">{{$salary->employee->id}}</dd>
                                        <dt class="col-sm-3">{{__('Position')}}</dt>
                                        <dd class="col-sm-9 position">{{$salary->employee->official->position()->first()->name}}</dd>
                                        <dt class="col-sm-3">{{__('Department')}}</dt>
                                        <dd class="col-sm-9 department">{{$salary->employee->official->division()->first()->department->name}}</dd>
                                        <dt class="col-sm-3">{{__('Division')}}</dt>
                                        <dd class="col-sm-9 division">{{$salary->employee->official->division()->first()->name}}</dd>
                                        <dt class="col-sm-3">{{__('Joining Date')}}</dt>
                                        <dd class="col-sm-9 joiningDate">{{\Carbon\Carbon::parse($salary->employee->official->from)->format('d M,Y')}}</dd>
                                    </dl>
                                </div>
                            </div>
                            <p class="h5">Salary Details</p>
                            <hr/>
                            <div class="row">
                                <div class="col-sm-6">
                                    <dl class="row">
                                        <dt class="col-sm-5">{{__('Salary Grade')}}</dt>
                                        <dd class="col-sm-7 grade">{{$salary->employee->official->grade->name}}</dd>
                                        <dt class="col-sm-5">{{__('Type')}}</dt>
                                        @if($salary->employee->official->grade->type)
                                            <dd class="col-sm-7 salaryType">{{__('Monthly')}}</dd>
                                        @else
                                            <dd class="col-sm-7 salaryType">{{__('Hourly')}}</dd>
                                        @endif
                                    </dl>
                                </div>
                                <div class="col-sm-6">
                                    <dl class="row">
                                        <dt class="col-sm-5">{{__('Basic')}}</dt>
                                        <dd class="col-sm-7 basicAmount">{{$salary->employee->official->grade->rate}}</dd>
                                        <dt class="col-sm-5">{{__('Overtime (Hourly)')}}</dt>
                                        <dd class="col-sm-7 overtimeAmount">{{$salary->employee->official->grade->overtime_rate}}</dd>
                                    </dl>
                                </div>
                                <div class="col-md-12">
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h5 class="widget-title">{{__('Basics')}}</h5>
                                            <div class="widget-tools pull-right">
                                                <button type="button"
                                                        class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="dt-responsive">
                                                <table id="basicTBL" class="table table-borderless table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('Total Days')}}</th>
                                                        <th>{{__('Working Days')}}</th>
                                                        <th>{{__('Total period')}}</th>
                                                        <th>{{__('Period')}}</th>
                                                        <th>{{__('Rate')}}</th>
                                                        <th>{{__('Amount')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{{$salary->details->total_working_day}}</td>
                                                        <td>{{$salary->details->working_day}}</td>
                                                        <td>{{$salary->details->total_working_hour}}</td>
                                                        <td>{{$salary->details->working_hour}}</td>
                                                        <td>{{$salary->employee->official->grade->rate}}</td>
                                                        <td>{{$salaryAmount+=$salary->details->basic}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $additions = $salary->allowances->filter(function($allowance){
                                        if(!$allowance->allowance->type) return true;
                                    });
                                @endphp
                                <div class="col-md-6">
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h5 class="widget-title">{{__('Allowances')}}</h5>
                                            <div class="widget-tools pull-right">
                                                <button type="button"
                                                        class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="dt-responsive">
                                                <table id="allowanceTBL" class="table table-borderless table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('Name')}}</th>
                                                        <th>{{__('Rate')}}</th>
                                                        <th>{{__('Amount')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($additions as $allowance)
                                                        <tr>
                                                            <td>{{$allowance->allowance->name}}</td>
                                                            @if($allowance->type)
                                                                <td>{{$allowance->rate}}%</td>
                                                                <td>{{$salaryAmount+=($allowance->rate / 100) * $salary->details->basic}}</td>
                                                            @else
                                                                <td>{{$allowance->rate}}</td>
                                                                <td>{{$salaryAmount+=$allowance->rate}}</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $deductions = $salary->allowances->filter(function($allowance){
                                        if($allowance->allowance->type) return true;
                                    });
                                @endphp
                                <div class="col-md-6">
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h5 class="widget-title">{{__('Deductions')}}</h5>
                                            <div class="widget-tools pull-right">
                                                <button type="button"
                                                        class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="dt-responsive">
                                                <table id="deductionTBL" class="table table-borderless table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('Name')}}</th>
                                                        <th>{{__('Rate')}}</th>
                                                        <th>{{__('Amount')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($deductions as $allowance)
                                                        <tr>
                                                            <td>{{$allowance->allowance->name}}</td>
                                                            @if($allowance->type)
                                                                <td>{{$allowance->rate}}%</td>
                                                                <td>{{$salaryAmount-=($allowance->rate / 100) * $salary->details->basic}}</td>
                                                            @else
                                                                <td>{{$allowance->rate}}</td>
                                                                <td>{{$salaryAmount-=$allowance->rate}}</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h5 class="widget-title">{{__('Leave')}}</h5>
                                            <div class="widget-tools pull-right">
                                                <button type="button"
                                                        class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="dt-responsive">
                                                <table id="leaveTBL" class="table table-borderless table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('Day')}}</th>
                                                        <th>{{__('Amount')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{{$salary->details->leave}}</td>
                                                        <td>{{($salaryAmount-=$salary->details->leave_amount) or 0}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h5 class="widget-title">{{__('Loans')}}</h5>
                                            <div class="widget-tools pull-right">
                                                <button type="button"
                                                        class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="col-md-12">
                                                <dl class="row">
                                                    <dt class="col-sm-4">{{__('Amount')}}</dt>
                                                    <dd class="col-sm-7 loanAmount">{{$salaryAmount-=$salary->details->loan}}</dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h5 class="widget-title">{{__('Overtime')}}</h5>
                                            <div class="widget-tools pull-right">
                                                <button type="button"
                                                        class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="dt-responsive">
                                                <table id="overtimeTBL" class="table table-borderless table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('Day')}}</th>
                                                        <th>{{__('Period')}}</th>
                                                        <th>{{__('Rate')}}</th>
                                                        <th>{{__('Amount')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{{$salary->details->overtime_day}}</td>
                                                        <td>{{$salary->details->overtime_hour}}</td>
                                                        <td>{{$salary->details->overtime_rate}}</td>
                                                        <td>{{$salaryAmount+=$salary->details->overtime}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-md-6">
                                    <p class="lead">{{__('Generated on')}}
                                        : {{\Carbon\Carbon::parse($salary->created_at)->format('d F,Y')}}</p>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    {{--                                    <p class="lead">Calculation</p>--}}
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th style="width:50%">{{__('Subtotal')}}:</th>
                                                <td>{{round($salaryAmount)}}</td>
                                            </tr>
                                            <tr>
                                                <th>{{__('Tax')}} (0%)</th>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <th>{{__('Total')}}:</th>
                                                <td>{{round($salaryAmount)}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--                Salary details end--}}
            </div>
        </div>
    </div>
    {{--    Content end--}}
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            // Print the state
            $(document).on('click', '.print-btn', function (e) {
                e.preventDefault();
                var $this = $(this);
                var originalContent = $('body').html();
                var printArea = $('.card').html();

                $('body').html(printArea);
                window.print();
                $('body').html(originalContent)
            });
        });
    </script>
@endsection

