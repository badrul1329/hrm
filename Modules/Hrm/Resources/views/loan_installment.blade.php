@extends('hrm::master')
@section('title')
    {{__('Installment')}}
@endsection
@section('content')
    {{--    content start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Installments')}}</h5>
                            <span>{{__('All loan installments according to date')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Loan')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Installment')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('Installments')}} </h3>
                        {{--                        Add button--}}
                        @if(Auth::user()->can('hrm-loan-installment-add'))
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#installmentModal" data-whatever="0"> + {{__('Add Installment')}}
                            </button>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            {{--                            Installment list start--}}
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Employee')}}</th>
                                    <th>{{__('Installment Amount')}}</th>
                                    <th>{{__('Paid')}}</th>
                                    <th>{{__('Received By')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            {{--                            Installment list end--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Installment Modal -->
    <div class="modal animated zoomIn faster" id="installmentModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Installment')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="InstallmentForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Installment')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date"
                                       value="{{\Carbon\Carbon::now()->format('d-M-Y')}}"
                                       placeholder="received date here" name="date" id="date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label"
                                   for="exampleTextarea1">{{__('Employee')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="employee" name="employee">
                                    <option value="">Loading ..</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Installment Amount')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control font-weight-bold form-txt-danger" value="0"
                                       min="0" name="installment_amount" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Paid Amount')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="number" class="form-control" value="0" min="0" name="paid" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label"
                                   for="exampleTextarea1">{{__('Received By')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="receivedBy" name="received_by">
                                    <option value="">Loading ..</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label"
                                   for="exampleTextarea1">{{__('Comment')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <textarea class="form-control" id="exampleTextarea1" rows="4"
                                          placeholder="{{__('comments here')}}.." name="comment"></textarea>
                            </div>
                        </div>

                        <table id="loanTBL" class="col-sm-12 table-bordered">
                            <thead>
                            <tr>
                                <td>{{__('ID')}}</td>
                                <td>{{__('Amount')}}</td>
                                <td>{{__('From')}}</td>
                                <td>{{__('Installment')}}</td>
                                <td>{{__('Created At')}}</td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--    content end--}}
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('installments.index') }}",
                deferRender: true,
                columns: [
                    {data: 'date', name: 'name'},
                    {data: 'employee.name', name: 'department'},
                    {data: 'installment_amount', name: 'created_at'},
                    {data: 'paid', name: 'created_at'},
                    {data: 'received_by', name: 'created_at'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            var loanTBL = $('#loanTBL').DataTable({
                'responsive': true,
                'paging': false,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': false,
                'autoWidth': true,
            });
            $(".select2").select2();

            // load Employee list
            loadEmployee();

            function loadEmployee() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#employee,#receivedBy').empty();
                        $('#employee').append('<option selected value="" disabled>{{__("Select an Employee")}}</option>');
                        $('#receivedBy').append('<option selected value="" disabled>{{__("Select an authorized person")}}</option>');
                        $.each(response, function (i, employee) {
                            $('#employee,#receivedBy').append($('<option>', {
                                value: employee.id,
                                text: employee.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            // Datewise employeewise Filter
            $(document).on('change', '#date,#employee', function () {
                let date = btoa($('#date').val());
                let employee = $('#employee').val();
                if (date && employee) {
                    // console.log(date+"===="+employee);
                    $.ajax({
                        url: '{{url('hrm')}}/loan/employees/' + employee + '/installments/' + date,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            $('input[name=installment_amount]').val(Math.round(response.installment_Amount));
                            loanTBL.clear().draw();
                            $.each(response.loans, function (i, loan) {
                                loanTBL.row.add([
                                    loan.id,
                                    loan.amount,
                                    moment(loan.repayment_from).format("DD-MM-YYYY"),
                                    loan.installment,
                                    loan.created_at,
                                ]).draw(true);
                            });
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                }
            });

            // Reset form
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $('input').val();
                $(".select2").select2();
                loanTBL.clear().draw();
            });
            // Show modal
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes
                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('{{__("Edit Installment")}}');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('installments.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('[name=employee]').val(response.employee_id).trigger('change');
                            modal.find('input[name=installment_amount]').val(response.installment_amount);
                            modal.find('input[name=paid]').val(response.paid);
                            modal.find('input[name=date]').val(moment(response.date).format("DD-MMM-YYYY"));
                            modal.find('[name=received_by]').val(response.received_by).trigger('change');
                            modal.find('[name=comment]').val(response.comment);
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Installment")}}');
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });
            // Save event listener
            $(document).on('click', '.save-btn', function () {
                let data = $('form').serialize();
                $.ajax({
                    url: '{{route('installments.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#installmentModal').modal('toggle');
                        $('form').trigger("reset");
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Update event listener
            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('installments.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#installmentModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Delete event listener
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('installments.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

