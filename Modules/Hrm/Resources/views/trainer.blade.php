@extends('hrm::master')
@section('title')
    {{__('Trainer')}}
@endsection
@section('content')
    {{--    Content start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Trainers')}}</h5>
                            <span>{{__('All trainer informations')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Trainings')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Trainer')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-6 m-auto">
                            <div class="card-search with-adv-search dropdown">
                                <form action="javascript:void(0)">
                                    <input type="text" class="form-control global_filter" id="global_filter"
                                           placeholder="{{__('Search')}}.." required>
                                    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
                                    <button type="button" id="adv_wrap_toggler"
                                            class="adv-btn ik ik-chevron-down dropdown-toggle" data-toggle="dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="adv-search-wrap dropdown-menu dropdown-menu-right"
                                         aria-labelledby="adv_wrap_toggler">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col1_filter" placeholder="{{__('Name')}}"
                                                           data-column="1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col2_filter" placeholder="{{__('Email')}}"
                                                           data-column="2">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col3_filter" placeholder="{{__('Phone')}}"
                                                           data-column="3">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col4_filter" placeholder="{{__('Company')}}"
                                                           data-column="4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @can('hrm-trainer-add')
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#trainerModal" data-whatever="0"> + {{__('Add Trainer')}}
                            </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    {{--                                    <th>ID</th>--}}
                                    <th></th>
                                    <th>{{__('Trainer')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Company')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Trainer View Modal -->
    <div class="modal animated zoomIn faster" id="viewModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Trainer')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{asset('resources/img/user.jpg')}}" class="img-thumbnail rounded-circle"
                                 height='150'
                                 width='150'/>
                        </div>
                        <div class="col-md-8">
                            <dl class="row">
                                <dt class="col-sm-4">{{__('Name')}}:</dt>
                                <dd class="col-sm-8 name"></dd>

                                <dt class="col-sm-4">{{__('Email')}}:</dt>
                                <dd class="col-sm-8 email"></dd>

                                <dt class="col-sm-4">{{__('Phone')}}:</dt>
                                <dd class="col-sm-8 phone"></dd>

                                <dt class="col-sm-4">{{__('Address')}}:</dt>
                                <dd class="col-sm-8 address"></dd>

                                <dt class="col-sm-4">{{__('Company')}}:</dt>
                                <dd class="col-sm-8 company"></dd>

                                <dt class="col-sm-4">{{__('Expertise')}}:</dt>
                                <dd class="col-sm-8 expertise"></dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Trainer Modal -->
    <div class="modal animated zoomIn faster" id="trainerModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Trainer')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="trainerForm" action="javascript:void(0)" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Name')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('name here')}}" name="name"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Email')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="email" class="form-control" placeholder="{{__('email here')}}" name="email"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Phone')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('phone no. here')}}"
                                       name="phone"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label"
                                   for="exampleTextarea1">{{__('Address')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <textarea class="form-control" id="exampleTextarea1" rows="3"
                                          placeholder="{{__('address here')}}.." name="address"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Company')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('Company name here')}}"
                                       name="company">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label"
                                   for="exampleTextarea1">{{__('Expertise')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <textarea class="form-control" id="exampleTextarea1" rows="4"
                                          placeholder="{{__('expertise here')}}.." name="expertise"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label"
                                   for="exampleTextarea1">{{__('Image')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="file" id="photo" name="photo">
                                <br/>
                                <img src="{{asset('resources/img/user.jpg')}}" class="img-thumbnail" height='100'
                                     width='100'/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--    Content end--}}
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('trainings.trainers.index') }}",
                deferRender: true,
                columns: [
                    // {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {
                        data: 'photo', name: 'photo', render: function (data, type, full, meta) {
                            if (data) {
                                return `<img src="{{asset('storage/hrm/trainer')}}/` + data + `" class='img-thumbnail' height='50' width='50'/>`;
                            } else {
                                return `<img src="{{asset('resources/img/user.jpg')}}" class='img-thumbnail' height='50' width='50'/>`;
                            }
                        }
                    },
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'company', name: 'company'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            // Search API start
            function filterColumn(i) {
                oTable.column(i).search(
                    $('#col' + i + '_filter').val(),
                    false,
                    true
                ).draw();
            }

            function filterGlobal() {
                oTable.search(
                    $('#global_filter').val(),
                    false,
                    true
                ).draw();
            }

            $('input.global_filter').on('keyup', function () {
                filterGlobal();
            });
            $('.column_filter').on('keyup change', function () {
                filterColumn($(this).attr('data-column'));
            });
            // Search API end
            // View photo thumbnail
            $(document).on('change', 'input[type=file]', function (event) {
                var that = $(this);
                var reader = new FileReader();
                reader.onload = function () {
                    var img = that.closest('.form-group').find('img');
                    img.attr('src', reader.result);
                };
                reader.readAsDataURL(event.target.files[0]);
            });
            // Reset Form
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $('form img').attr('src', "{{asset('resources/img/user.jpg')}}");
            });
            // Show Modal
            $('#trainerModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes

                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('{{__("Edit Trainer")}}');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('trainings.trainers.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('input[name=name]').val(response.name);
                            modal.find('input[name=email]').val(response.email);
                            modal.find('input[name=phone]').val(response.phone);
                            modal.find('input[name=company]').val(response.company);
                            modal.find('textarea[name=address]').val(response.address);
                            modal.find('textarea[name=expertise]').val(response.expertise);
                            let url = `{{asset('resources/img/user.jpg')}}`;
                            if (response.photo) {
                                url = `{{asset('storage/hrm/trainer')}}/` + response.photo;
                            }
                            modal.find('img').attr('src', url);
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Trainer")}}');
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });
            // View event listener
            $(document).on('click', '.view-btn', function () {
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ route('trainings.trainers.index')}}/" + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#viewModal .name').text(response.name);
                        $('#viewModal .email').text(response.email);
                        $('#viewModal .phone').text(response.phone);
                        $('#viewModal .company').text(response.company);
                        $('#viewModal .address').text(response.address);
                        $('#viewModal .expertise').text(response.expertise);
                        let url = `{{asset('resources/img/user.jpg')}}`;
                        if (response.photo) {
                            url = `{{asset('storage/hrm/trainer')}}/` + response.photo;
                        }
                        $('#viewModal img').attr('src', url);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Save event listener
            $(document).on('click', '.save-btn', function () {
                let form = $('#trainerForm')[0];
                let data = new FormData(form);
                $.ajax({
                    url: '{{route('trainings.trainers.store')}}',
                    type: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    cache: false,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#trainerModal').modal('toggle');
                        $('form').trigger("reset");
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Update event listener
            $(document).on('click', '.update-btn', function () {
                let form = $('#trainerForm')[0];
                let data = new FormData(form);
                let id = $(this).val();
                data.append('_method', 'PUT');
                $.ajax({
                    url: "{{ route('trainings.trainers.index')}}/" + id,
                    type: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    cache: false,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#trainerModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Delete event listener
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('trainings.trainers.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

