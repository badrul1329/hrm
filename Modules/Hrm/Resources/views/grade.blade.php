@extends('hrm::master')
@section('title')
{{__('Grade')}}
@endsection
@section('content')
{{--    content start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Grades')}}</h5>
                            <span>{{__('All Salary grades')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Employee')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Grade')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <div class="col-sm-6 m-auto">
                            <div class="card-search with-adv-search dropdown">
{{--                                Smart search start--}}
                                <form action="javascript:void(0)">
                                    <input type="text" class="form-control global_filter" id="global_filter"
                                           placeholder="{{__('Search')}}..">
                                    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
                                    <button type="button" id="adv_wrap_toggler"
                                            class="adv-btn ik ik-chevron-down dropdown-toggle" data-toggle="dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="adv-search-wrap dropdown-menu dropdown-menu-right"
                                         aria-labelledby="adv_wrap_toggler">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col1_filter" placeholder="{{__('Name')}}" data-column="1">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="select2 column_filter"
                                                            id="col2_filter" data-column="2">
                                                        <option selected value="">{{__('Select type')}}</option>
                                                        <option value="Monthly">{{__('Monthly')}}</option>
                                                        <option value="Hourly">{{__('Hourly')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{--                                Smart search end--}}

                            </div>
                        </div>
{{--                        add button--}}
                        @can('hrm-salary-grade-add')
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#gradeModal" data-whatever="0"> + {{__('Add Grade')}}
                            </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
{{--                            Grade list start--}}
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Grade')}}</th>
                                    <th>{{__('Type')}}</th>
                                    <th>{{__('Basic')}}</th>
                                    <th>{{__('Net')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
{{--                            Grade list end--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Grade Modal -->
    <div class="modal animated zoomIn faster" id="gradeModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Grade')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
{{--                Form start--}}
                <form id="GradeForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('New Grade')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('name here')}}" name="name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Type')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="type" name="type">
                                    <option value="0">{{__('Monthly')}}</option>
                                    <option value="1">{{__('Hourly')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Rate')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="number" class="form-control" value="0" min="0" name="rate" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Overtime Rate (per Hour)')}} </label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="number" class="form-control" value="0" min="0" name="orate"
                                       required>
                            </div>
                        </div>
                        <div class="row border p-2 my-4 bg-light">
                            <div class="col-sm-6">
                                <fieldset>
                                    <legend class="h6">{{__('Addition')}}:</legend>
                                    @foreach($additiveAllowances as $allowance)
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-lg-4 col-form-label">{{$allowance->name}}</label>
                                            <div class="col-sm-8 col-lg-8">
                                                <div class="input-group">
                                                    <input type="text" class="form-control form-control-center"
                                                           value="0" min="0"
                                                           name="allowance[{{$allowance->id}}][rate]"
                                                           id="allowance-{{$allowance->id}}-rate" required>
                                                    <div class="input-group-append">
                                                        <select class="form-control select2"
                                                                name="allowance[{{$allowance->id}}][type]"
                                                                id="allowance-{{$allowance->id}}-type">
                                                            <option value="0" selected>{{__('Cash')}}</option>
                                                            <option value="1">{{__('Percent')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </fieldset>
                            </div>
                            <div class="col-sm-6 border-left">
                                <fieldset>
                                    <legend class="h6">{{__('Deduction')}}:</legend>
                                    @foreach($deductiveAllowances as $allowance)
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-lg-4 col-form-label">{{$allowance->name}}</label>
                                            <div class="col-sm-8 col-lg-8">
                                                <div class="input-group">
                                                    <input type="text" class="form-control form-control-center"
                                                           value="0" min="0"
                                                           name="allowance[{{$allowance->id}}][rate]"
                                                           id="allowance-{{$allowance->id}}-rate" required>
                                                    <div class="input-group-append">
                                                        <select class="form-control select2"
                                                                name="allowance[{{$allowance->id}}][type]"
                                                                id="allowance-{{$allowance->id}}-type">
                                                            <option value="0" selected>{{__('Cash')}}</option>
                                                            <option value="1">{{__('Percent')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </fieldset>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Net Salary')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control form-control-center" value="0" min="0"
                                       disabled="disabled" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
{{--                Form end--}}
            </div>
        </div>
    </div>
{{--    content end--}}
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('grades.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'type', name: 'type'},
                    {data: 'rate', name: 'rate'},
                    {data: 'net', name: 'net'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            // Search API start
            function filterColumn(i) {
                oTable.column(i).search(
                    $('#col' + i + '_filter').val(),
                    false,
                    true
                ).draw();
            }

            function filterGlobal() {
                oTable.search(
                    $('#global_filter').val(),
                    false,
                    true
                ).draw();
            }

            $('input.global_filter').on('keyup', function () {
                filterGlobal();
            });
            $('.column_filter').on('keyup change', function () {
                filterColumn($(this).attr('data-column'));
            });
            // Search API end

            $('.select2').select2();
            // Reset form
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $('.select2').select2();
            });
            // Show Modal
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes

                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('{{__("Edit Grade")}}');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('grades.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('input[name=name]').val(response.name);
                            modal.find('[name=type]').val(response.type).trigger('change');
                            modal.find('input[name=rate]').val(response.rate);
                            modal.find('input[name=orate]').val(response.overtime_rate);
                            if (response.allowances) {
                                $.each(response.allowances, function (i, allowance) {
                                    modal.find(`#allowance-${allowance.allowance_id}-rate`).val(allowance.rate);
                                    modal.find(`#allowance-${allowance.allowance_id}-type`).val(allowance.type).trigger('change');
                                });
                            }
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Grade")}}');
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });
            // Save event listener
            $(document).on('click', '.save-btn', function () {
                let data = $('form').serialize();
                $.ajax({
                    url: '{{route('grades.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#gradeModal').modal('toggle');
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Update event listener
            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('grades.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#gradeModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // delete event listener
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('grades.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

