@extends('hrm::master')
@section('title')
    {{__('Employees')}}
@endsection
@section('content')
    {{--    Content Start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Employees')}}</h5>
                            <span>{{__('Employees with their details')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Employee')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Employee')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-6 m-auto">
                            <div class="card-search with-adv-search dropdown">
                                {{--                                Smart search start--}}
                                <form action="javascript:void(0)">
                                    <input type="text" class="form-control global_filter" id="global_filter"
                                           placeholder="{{__('Search')}}.." required>
                                    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
                                    <button type="button" id="adv_wrap_toggler"
                                            class="adv-btn ik ik-chevron-down dropdown-toggle" data-toggle="dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="adv-search-wrap dropdown-menu dropdown-menu-right"
                                         aria-labelledby="adv_wrap_toggler">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col1_filter" placeholder="{{__('Name')}}"
                                                           data-column="1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col3_filter" placeholder="{{__('email')}}"
                                                           data-column="3">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col2_filter" placeholder="{{__('phone')}}"
                                                           data-column="2">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control select2 column_filter" id="col4_filter"
                                                            data-column="4">
                                                        <option selected value="">{{__('Select a gender')}}</option>
                                                        <option value="Male">{{__('Male')}}</option>
                                                        <option value="Female">{{__('Female')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col5_filter" placeholder="{{__('Country')}}"
                                                           data-column="5">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col6_filter" placeholder="{{__('City')}}"
                                                           data-column="6">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control select2 column_filter divisionFilter"
                                                            id="col8_filter" data-column="8">
                                                        <option selected value="">{{__('Select a Division')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control select2 column_filter positionFilter"
                                                            id="col9_filter" data-column="9">
                                                        <option selected value="">{{__('Select a Position')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control select2 column_filter jtypeFilter"
                                                            id="col10_filter"
                                                            data-column="10">
                                                        <option selected value="">{{__('Select a Job Type')}}</option>
                                                        <option value="Monthly">{{__('Monthly')}}</option>
                                                        <option value="Hourly">{{__('Hourly')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{--                                Smart Search End--}}
                            </div>
                        </div>
                        {{--                        Add Button--}}
                        @can('hrm-employee-add')
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#employeeModal" data-whatever="0"> + {{__('Add Employee')}}
                            </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            {{--                            Employee List Start--}}
                            <table id="#" class="table nowrap" style="width: 100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Gender')}}</th>
                                    <th>{{__('Country')}}</th>
                                    <th>{{__('City')}}</th>
                                    <th>{{__('Zip Code')}}</th>
                                    <th>{{__('Division')}}</th>
                                    <th>{{__('Position')}}</th>
                                    <th>{{__('Type')}}</th>
                                    <th>{{__('From')}}</th>
                                    <th>{{__('To')}}</th>
                                    <th>{{__('Joined')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            {{--                            Employee List End--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Employee Modal -->
    <div class="modal animated zoomIn faster" id="employeeModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content app-content" id="validation">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Employee')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Form wizard with step validation section start -->
                    <form action="javascript:void(0)" class="form form-wizard wizard-circle" method="POST"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" id="id"/>
                        <!-- Step 1: General Info -->
                        <h6>General</h6>
                        <fieldset>
                            <div class="form-group text-center justify-content-md-center">
                                <img src="{{asset('resources')}}/img/user.jpg" class="rounded-circle" height="150"
                                     width="150"/>
                                <input type="file" id="photo" name="photo" placeholder="photo">
                            </div>
                            <div class="form-group">
                                <label for="name">{{__('First Name')}} <span class="text-danger">*</span></label>
                                <input type="text" class="form-control required" id="name" name="name"
                                       placeholder="{{__('Name')}}" required>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">{{__('Email')}}</label>
                                    <input type="email" class="form-control" id="email" placeholder="{{__('Email')}}"
                                           name="email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="phone">{{__('Phone')}} <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" id="phone"
                                           placeholder="{{__('Phone')}}" name="phone" required/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="street">{{__('Street/Holding No')}} <span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" id="street" name="street"
                                           placeholder="{{__('1234 Main St')}}" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="city">{{__('City')}} <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" id="city" name="city"
                                           placeholder="{{__('Dhaka')}}" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="district">{{__('District')}} <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" id="district" name="district"
                                           placeholder="{{__('Dhaka')}}" required/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="zip">{{__('Zip')}} <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" id="zip" name="zip"
                                           placeholder="{{__('1234')}}" required/>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="country">{{__('Country')}} <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" id="country" name="country"
                                           placeholder="{{__('Bangladesh')}}" required/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label for="gender">{{__('Gender')}}</label>
                                    <select id="gender" class="form-control select2" name="gender">
                                        <option value="0" selected>{{__('Male')}}</option>
                                        <option value="1">{{__('Female')}}</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="dob">{{__('Date of Birth')}}</label>
                                    <input type="text" class="form-control date" id="dob" placeholder="dd-mm-YYYY"
                                           name="dob">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="religion">{{__('Religion')}}</label>
                                    <select id="religion" class="form-control select2" name="religion">
                                        <option>Loading..</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="marital_status">{{__('Marital Status')}}</label>
                                    <select id="marital_status" class="form-control select2" name="marital_status">
                                        <option>Loading..</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>

                        <!-- Step 2 : Official Info -->
                        <h6>Official</h6>
                        <fieldset>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="position">{{__('Position')}} <span class="text-danger">*</span></label>
                                    <select id="position" class="form-control select2 required" name="position"
                                            required>
                                        <option>Loading..</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="division">{{__('Division')}} <span class="text-danger">*</span></label>
                                    <select id="division" class="form-control select2 required" name="division"
                                            required>
                                        <option>Loading..</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="supervisor">{{__('Supervisor')}} <span
                                            class="text-danger">*</span></label>
                                    <select id="supervisor" class="form-control select2" name="supervisor">
                                        <option>Loading..</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="jtype">{{__('Job Type')}} <span class="text-danger">*</span></label>
                                    <select id="jtype" class="form-control select2 required" name="job_type" required>
                                        <option>Loading..</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="from">{{__('From date')}} <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required date" id="from" name="from"
                                           required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="to">{{__('To date')}}</label>
                                    <input type="text" class="form-control date" id="to" name="to">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="shift">{{__('Shift')}} <span class="text-danger">*</span></label>
                                <select id="shift" class="form-control select2 required" name="shift" required>
                                    <option>Loading..</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="grade">{{__('Salary Grade')}} <span class="text-danger">*</span></label>
                                <select id="grade" class="form-control select2 required" name="grade" required>
                                    <option>Loading..</option>
                                </select>
                            </div>
                        </fieldset>

                        <!-- Step 3: Documents -->
                        <h6 class="step-document">{{__('Documents')}}</h6>
                        <fieldset class="step-document">
                            <div class="repeater col-md-8 mx-auto">
                                <div data-repeater-list="doc">
                                    <div data-repeater-item class="d-flex mb-2">
                                        <label class="sr-only" for="inlineFormInputGroup1">{{__('Documents')}}</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend col-md-8 col-xs-12">
                                                <input type="text" class="form-control"
                                                       placeholder="{{__('Subject of file')}}" name="title"/>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" class="form-control custom-file-input"
                                                       id="doc"
                                                       aria-describedby="docAddon" name="doc">
                                                <label class="custom-file-label" for="doc">{{__('Choose file')}}</label>
                                            </div>
                                        </div>
                                        <button data-repeater-delete type="button" class="btn btn-danger btn-icon ml-2">
                                            <i class="ik ik-trash-2"></i></button>
                                    </div>
                                </div>
                                <button data-repeater-create type="button" class="btn btn-success btn-icon ml-2 mb-2"><i
                                        class="ik ik-plus"></i></button>
                                {{__('Add New Document')}}
                            </div>
                        </fieldset>
                    </form>
                    <!-- Form wizard with step validation section end -->
                </div>
            </div>
        </div>
    </div>
    {{--    Content End--}}
@endsection
@section('extraCSS')
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/form/wizard.min.css">
@endsection
@section('extraJS')
    <script src="{{asset('resources')}}/plugins/form/jquery.steps.min.js"></script>
    <script src="{{asset('resources')}}/plugins/form/jquery.validate.min.js"></script>
    <script src="{{asset('resources')}}/plugins/form/wizard-steps.js"></script>
    <script src="{{asset('resources')}}/plugins/jquery.repeater/jquery.repeater.min.js"></script>
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('employees.index') }}",
                deferRender: true,
                columns: [
                    // {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {
                        data: 'photo', name: 'photo', render: function (data, type, full, meta) {
                            if (data) {
                                return `<img src="{{asset('storage/hrm/employee/photo/')}}/` + data + `" class='img-thumbnail' height='50' width='50'/>`;
                            } else {
                                return `<img src="{{asset('resources/img/user.jpg')}}" class='img-thumbnail' height='50' width='50'/>`;
                            }
                        }
                    },
                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'email', name: 'email'},
                    {data: 'gender', name: 'gender'},
                    {data: 'address.country', name: 'address.country'},
                    {data: 'address.city', name: 'address.city'},
                    {data: 'address.zip_code', name: 'address.zip_code'},
                    {data: 'official.division.name', name: 'official.division.name'},
                    {data: 'official.position.name', name: 'official.position.name'},
                    {data: 'official.job_type.type', name: 'official.job_type.type'},
                    {data: 'official.from', name: 'official.from'},
                    {data: 'official.to', name: 'official.to'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            // repeater
            $('.repeater').repeater({
                initEmpty: false,
                defaultValues: {
                    'title': '{{__("Document name")}}'
                },
                show: function () {
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    if (confirm('{{__("Are you sure you want to delete this element")}}?')) {
                        $(this).slideUp(deleteElement);
                    }
                },
                ready: function (setIndexes) {

                },
                isFirstItemUndeletable: true
            });

            // Search API start
            function filterColumn(i) {
                oTable.column(i).search(
                    $('#col' + i + '_filter').val(),
                    false,
                    true
                ).draw();
            }

            function filterGlobal() {
                oTable.search(
                    $('#global_filter').val(),
                    false,
                    true
                ).draw();
            }

            $('input.global_filter').on('keyup', function () {
                filterGlobal();
            });
            $('.column_filter').on('keyup change', function () {
                filterColumn($(this).attr('data-column'));
            });
            // Search API end

            $('.select2').each(function () {
                $(this).select2();
            });

            // Load Religion List
            loadReligion();

            function loadReligion() {
                $.ajax({
                    url: '{{route('religion.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#religion').empty();
                        // $('#religion').append('<option selected value="" disabled>Select a Religion</option>');
                        $.each(response, function (i, religion) {
                            $('#religion').append($('<option>', {
                                value: religion.id,
                                text: religion.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Load Marital Status List
            loadMStatus();

            function loadMStatus() {
                $.ajax({
                    url: '{{route('marital_info.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#marital_status').empty();
                        // $('#marital_status').append('<option selected value="" disabled>Select status</option>');
                        $.each(response, function (i, mstatus) {
                            $('#marital_status').append($('<option>', {
                                value: mstatus.id,
                                text: mstatus.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Load Position List
            loadPositions();

            function loadPositions() {
                $.ajax({
                    url: '{{route('position.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#position,.positionFilter').empty();
                        $('#position').append('<option selected value="" disabled>{{__("Select position")}}</option>');
                        $('.positionFilter').append('<option selected value="">{{__("Select position")}}</option>');
                        $.each(response, function (i, position) {
                            $('#position').append($('<option>', {
                                value: position.id,
                                text: position.name
                            }));
                            $('.positionFilter').append($('<option>', {
                                value: position.name,
                                text: position.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Load Division List
            loadDivisions();

            function loadDivisions() {
                $.ajax({
                    url: '{{route('division.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#division,.divisionFilter').empty();
                        $('#division').append('<option selected value="" disabled>{{__("Select division")}}</option>');
                        $('.divisionFilter').append('<option selected value="">{{__("Select division")}}</option>');
                        $.each(response, function (i, division) {
                            $('#division').append($('<option>', {
                                value: division.id,
                                text: division.name
                            }));
                            $('.divisionFilter').append($('<option>', {
                                value: division.name,
                                text: division.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Load Supervisor List
            loadSupervisors();

            function loadSupervisors() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#supervisor').empty();
                        $('#supervisor').append('<option selected value="">{{__("None")}}</option>');
                        $.each(response, function (i, supervisor) {
                            $('#supervisor').append($('<option>', {
                                value: supervisor.id,
                                text: supervisor.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Load Job Type List
            loadJobTypes();

            function loadJobTypes() {
                $.ajax({
                    url: '{{route('job_type.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#jtype,.jtypeFilter').empty();
                        $('.jtypeFilter').append('<option selected value="">{{__("Select job type")}}</option>');
                        $.each(response, function (i, type) {
                            $('#jtype').append($('<option>', {
                                value: type.id,
                                text: type.type
                            }));
                            $('.jtypeFilter').append($('<option>', {
                                value: type.type,
                                text: type.type
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Load Shift List
            loadJobShift();

            function loadJobShift() {
                $.ajax({
                    url: '{{route('shift_schedule.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#shift').empty();
                        $('#shift').append('<option selected value="" disabled>{{__("Select Shift")}}</option>');
                        $.each(response, function (i, shift) {
                            $('#shift').append($('<option>', {
                                value: shift.id,
                                text: shift.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Load Grade List
            loadSalaryGrade();

            function loadSalaryGrade() {
                $.ajax({
                    url: '{{route('grade.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#grade').empty();
                        $('#grade').append('<option selected value="" disabled>{{__("Select Grade")}}</option>');
                        $.each(response, function (i, grade) {
                            let type = (grade.type) ? '{{__("Hourly")}}' : '{{__("Monthly")}}';
                            $('#grade').append($('<option>', {
                                value: grade.id,
                                text: grade.name + ` ({{__("Basic")}}:${grade.rate}/${type})`
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

// Show file Name
            $(document).on('change', '.custom-file-input', function (event) {
                var fileName = event.target.files[0].name;
                let block = $(this).closest('.input-group');
                block.find('.custom-file-label').addClass("selected").html(fileName);
            });
// Reset Form
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $('form').find('img').attr('src', "{{asset('resources')}}/img/user.jpg");
                $(".select2").select2();
                $(this).find('.step-document').removeClass('hidden');
            });
            // Show Modal
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes
                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('{{__("Edit Employee")}}');
                    modal.find('.step-document').addClass('hidden');
                    var id = button.data('id');
                    $.ajax({
                            url: '{{route('employees.index')}}/' + id,
                            type: 'GET',
                            beforeSend: function (request) {
                                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                            },
                            success: function (response) {
                                if (response.photo) {
                                    let link = `{{asset('storage/hrm/employee/photo/')}}/` + response.photo;
                                    modal.find('img').attr('src', link);
                                }
                                modal.find('#name').val(response.name);
                                modal.find('#email').val(response.email);
                                modal.find('#phone').val(response.phone);
                                modal.find('#street').val(response.address.street);
                                modal.find('#city').val(response.address.city);
                                modal.find('#district').val(response.address.district);
                                modal.find('#zip').val(response.address.zip_code);
                                modal.find('#country').val(response.address.country);
                                modal.find('#gender').val(response.gender).trigger('change');
                                if (response.birthday) {
                                    modal.find('#dob').val(moment(response.birthday, 'YYYY-MM-DD').format('DD-MMM-YYYY'));
                                }
                                modal.find('#religion').val(response.religion_id).trigger('change');
                                modal.find('#marital_status').val(response.marital_id).trigger('change');
                                modal.find('#position').val(response.official.position).trigger('change');
                                modal.find('#division').val(response.official.division).trigger('change');
                                modal.find('#supervisor').val(response.official.supervisor).trigger('change');
                                modal.find('#job_type').val(response.official.job_type).trigger('change');
                                if (response.official.from) {
                                    modal.find('#from').val(moment(response.official.from, 'YYYY-MM-DD').format('DD-MMM-YYYY'));
                                }
                                if (response.official.to) {
                                    modal.find('#to').val(moment(response.official.to, 'YYYY-MM-DD').format('DD-MMM-YYYY'));
                                }
                                modal.find('#shift').val(response.official.shift).trigger('change');
                                modal.find('#grade').val(response.official.grade_id).trigger('change');
                                // repeater.setList([
                                //     { 'title': 'A','doc':'a'}
                                // ]);

                            },
                            error: function (data) {
                                let msg = '';
                                if (data.responseJSON.errors) {
                                    $.each(data.responseJSON.errors, function (i, error) {
                                        msg += '<p">' + error[0] + '</p>';
                                    });
                                } else {
                                    msg = data.responseJSON.message;
                                }
                                showDangerToast(msg);
                            }
                        }
                    );
                    modal.find('#id').val(id);
                    modal.find('.actions a[href="#finish"]').text('{{__("Update")}}');
                } else {
                    modal.find('.modal-title').text('{{__("Add Employee")}}');
                    modal.find('#id').val();
                    modal.find('.actions a[href="#finish"]').text('{{__("Save")}}');

                }
            });
            // Form Submit event Listener
            $(document).on('submit', 'form', function (e) {
                let id = $(this).find('#id').val();
                let data = new FormData($(this)[0]);
                if (id) {
                    // Edit Operation
                    data.append('_method', 'PUT');
                    $.ajax({
                        url: "{{ route('employees.index')}}/" + id,
                        type: 'POST',
                        data: data,
                        // async: false,
                        contentType: false,
                        processData: false,
                        cache: false,
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            $('#employeeModal').modal('toggle');
                            showSuccessToast('{{__("Updated Successfully")}}');
                            oTable.draw(false);
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                } else {
                    // Save Operation
                    $.ajax({
                        url: '{{route('employees.store')}}',
                        type: 'POST',
                        data: data,
                        async: false,
                        contentType: false,
                        processData: false,
                        cache: false,
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            $('#employeeModal').modal('toggle');
                            $('form').trigger("reset");
                            showSuccessToast('{{__("Added Successfully")}}');
                            oTable.draw(false);
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                }
            });
            // Delete Event Listener
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('employees.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Show file Thumbnail
            $(document).on('change', 'input[type=file]', function (event) {
                var that = $(this);
                var reader = new FileReader();
                reader.onload = function () {
                    var img = that.closest('.form-group').find('img');
                    img.attr('src', reader.result);
                };
                reader.readAsDataURL(event.target.files[0]);
            });
        })
        ;
    </script>
@endsection

