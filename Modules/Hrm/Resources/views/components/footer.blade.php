{{--Footer Start--}}
<footer class="footer">
    <div class="w-100 clearfix">
        {!! $settings['footer'] ?? '' !!}
    </div>
</footer>
{{--Footer End--}}
