{{--Left sidebar start--}}
<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="{{route('hrm')}}">
            <div class="logo-img">
                @if($settings['logo'])
                    <img src="{{url('storage',$settings['logo'])}}" class="header-brand-img img-fluid" alt="lavalite">
                @else
                    <img src="{{asset('resources')}}/logo.png" class="header-brand-img img-fluid" alt="lavalite">
                @endif
            </div>
            {{--            Application name--}}
            <span class="text ml-2 float-right"> {{$settings['title'] ?? config('app.name', 'Laravel')}} </span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded"
                                                    class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>

    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
                {{--                HRM Menu Start--}}
                {{--                <div class="nav-lavel">Menu</div>--}}
                <div class="nav-item">
                    <a href="{{url('hrm')}}"><i class="ik ik-bar-chart-2"></i><span>{{__('Dashboard')}}</span></a>
                </div>
                @if(Auth::user()->can('hrm-position-view') || Auth::user()->can('hrm-department-view') || Auth::user()->can('hrm-division-view') || Auth::user()->can('hrm-employee-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-users"></i><span>{{__('Employee')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-position-view')
                                <a href="{{route('positions.index')}}" class="menu-item">{{__('Position')}}</a>
                            @endcan
                            @can('hrm-department-view')
                                <a href="{{route('departments.index')}}" class="menu-item">{{__('Department')}}</a>
                            @endcan
                            @can('hrm-division-view')
                                <a href="{{route('divisions.index')}}" class="menu-item">{{__('Division')}}</a>
                            @endcan
                            @can('hrm-employee-view')
                                <a href="{{route('employees.index')}}" class="menu-item">{{__('Employee')}}</a>
                                <a href="{{route('employees.documents.index')}}" class="menu-item">Documents</a>
                            @endcan
                        </div>
                    </div>
                @endif
                @if(Auth::user()->can('hrm-training-type-view') || Auth::user()->can('hrm-trainer-view') || Auth::user()->can('hrm-workshop-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-award"></i><span>{{__('Training')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-training-type-view')
                                <a href="{{route('trainings.types.index')}}" class="menu-item">{{__('Types')}}</a>
                            @endcan
                            @can('hrm-trainer-view')
                                <a href="{{route('trainings.trainers.index')}}" class="menu-item">{{__('Trainers')}}</a>
                            @endcan
                            @can('hrm-workshop-view')
                                <a href="{{route('trainings.workshops.index')}}"
                                   class="menu-item">{{__('Workshops')}}</a>
                            @endcan
                        </div>
                    </div>
                @endif
                @if(Auth::user()->can('hrm-salary-view') || Auth::user()->can('hrm-salary-grade-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-dollar-sign"></i><span>{{__('Payroll')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-salary-grade-view')
                                <a href="{{route('grades.index')}}" class="menu-item">{{__('Grades')}}</a>
                            @endcan
                            @can('hrm-salary-view')
                                <a href="{{route('salaries.generate.index')}}"
                                   class="menu-item">{{__('Generate Salary')}}</a>
                                <a href="{{route('salaries.salary.index')}}"
                                   class="menu-item">{{__('Manage Salary')}}</a>
                            @endcan
                        </div>
                    </div>
                @endif
                @if(Auth::user()->can('hrm-attendance-view') || Auth::user()->can('hrm-shift-view') || Auth::user()->can('hrm-reshift-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i
                                class="ik ik-user-check"></i><span>{{__('Attendance')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-attendance-view')
                                <a href="{{route('attendances.index')}}" class="menu-item">{{__('Attendances')}}</a>
                            @endcan
                            @can('hrm-shift-view')
                                <a href="{{route('shift-schedules.index')}}"
                                   class="menu-item">{{__('Shift Schedules')}}</a>
                            @endcan
                            @can('hrm-reshift-view')
                                <a href="{{route('reschedules.index')}}"
                                   class="menu-item">{{__('Shift Reschedules')}}</a>
                            @endcan
                        </div>
                    </div>
                @endif
                @if(Auth::user()->can('hrm-holiday-view') || Auth::user()->can('hrm-leave-type-view') || Auth::user()->can('hrm-leave-application-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-user-minus"></i><span>{{__('Leave')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-holiday-view')
                                <a href="{{route('leave.holidays.index')}}" class="menu-item">{{__('Holidays')}}</a>
                            @endcan
                            @can('hrm-leave-type-view')
                                <a href="{{route('leave.types.index')}}" class="menu-item">{{__('Types')}}</a>
                            @endcan
                            @can('hrm-leave-application-view')
                                <a href="{{route('leave.applications.index')}}"
                                   class="menu-item">{{__('Applications')}}</a>
                            @endcan
                        </div>
                    </div>
                @endif
                @can('hrm-allowance-view')
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-heart-on"></i><span>{{__('Allowance')}}</span></a>
                        <div class="submenu-content">
                            <a href="{{route('allowances.index')}}" class="menu-item">{{__('Types')}}</a>
                        </div>
                    </div>
                @endcan
                @if(Auth::user()->can('hrm-loan-view') || Auth::user()->can('hrm-loan-installment-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-dollar-sign"></i><span>{{__('Loan')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-loan-view')
                                <a href="{{route('loans.index')}}" class="menu-item">{{__('Grant Loan')}}</a>
                            @endcan
                            @can('hrm-loan-installment-view')
                                <a href="{{route('installments.index')}}" class="menu-item">{{__('Installments')}}</a>
                            @endcan
                        </div>
                    </div>
                @endcan

                @if(Auth::user()->can('hrm-report-loan'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-file-text"></i><span>{{__('Report')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-report-loan')
                                <a href="{{route('report.loan')}}" class="menu-item">{{__('Loan')}}</a>
                            @endcan
                        </div>
                    </div>
                @endif
                {{--HRM Menu end--}}
                {{--                Common Menu Start--}}
                {{--                <div class="nav-lavel">General</div>--}}
                @can('hrm-notice-view')
                    <div class="nav-item">
                        <a href="{{route('hrm.notices.index')}}"><i
                                class="ik ik-bell"></i><span>{{__('Notice Board')}}</span></a>
                    </div>
                @endcan
                <div class="nav-item">
                    <a href="{{route('hrm.messenger.index')}}"><i
                            class="ik ik-message-square"></i><span>{{__('Messenger')}}</span></a>
                </div>
                <div class="nav-item">
                    <a href="{{route('hrm.calender.index')}}"><i
                            class="ik ik-calendar"></i><span>{{__('Calender')}}</span></a>
                </div>
                @can('hrm-log-view')
                    <div class="nav-item">
                        <a href="{{route('hrm.activities.index')}}"><i
                                class="ik ik-activity"></i><span>{{__('Activity Log')}}</span></a>
                    </div>
                @endcan
                @can('hrm-user-view')
                    <div class="nav-item">
                        <a href="{{route('hrm.users.index')}}"><i
                                class="ik ik-users"></i><span>{{__('Users')}}</span></a>
                    </div>
                @endcan
                @if(Auth::user()->can('hrm-role-view') || Auth::user()->can('hrm-user-role-view') || Auth::user()->can('hrm-permission-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i
                                class="ik ik-lock"></i><span>{{__('Role and Permission')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-role-view')
                                <a href="{{route('hrm.roles.index')}}" class="menu-item">{{__('Role')}}</a>
                            @endcan
                            @can('hrm-user-role-view')
                                <a href="{{route('hrm.user-role.index')}}" class="menu-item">{{__('User Roles')}}</a>
                            @endcan
                            @can('hrm-permission-view')
                                <a href="{{route('hrm.role-permission.index')}}"
                                   class="menu-item">{{__('Role permissions')}}</a>
                                <a href="{{route('hrm.user-permission.index')}}"
                                   class="menu-item">{{__('User permissions')}}</a>
                            @endcan
                        </div>
                    </div>
                @endif
                @if(Auth::user()->can('hrm-settings-view') || Auth::user()->can('hrm-language-view') || Auth::user()->can('hrm-phrase-view') || Auth::user()->can('hrm-currency-view'))
                    <div class="nav-item has-sub">
                        <a href="javascript:void(0)"><i class="ik ik-settings"></i><span>{{__('Settings')}}</span></a>
                        <div class="submenu-content">
                            @can('hrm-settings-view')
                                <a href="{{route('hrm.settings.index')}}" class="menu-item">{{__('General')}}</a>
                            @endcan
                            @can('hrm-language-view')
                                <a href="{{route('hrm.languages.index')}}" class="menu-item">{{__('Languages')}}</a>
                            @endcan
                            @can('hrm-phrase-view')
                                <a href="{{route('hrm.phrases.index')}}" class="menu-item">{{__('Phrases')}}</a>
                            @endcan
                            @can('hrm-currency-view')
                                <a href="{{route('hrm.currencies.index')}}" class="menu-item">{{__('Currency')}}</a>
                            @endcan
                        </div>
                    </div>
                @endif
                {{--                Common Menu End--}}
            </nav>
        </div>
    </div>
</div>
{{--Left sidebar end--}}
