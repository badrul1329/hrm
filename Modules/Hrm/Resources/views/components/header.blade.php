{{--Header Start--}}
<header class="header-top" header-theme="light">
    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="top-menu d-flex align-items-center">
                <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                {{--                Full screen button--}}
                <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i>
                </button>
            </div>
            <div class="top-menu d-flex align-items-center">
                <div>
                    {{--                    Language dropdown--}}
                    <select class="form-control select2" id="headerLanguage">
                    </select>
                </div>
                {{--                Message Box--}}
                <button type="button" class="nav-link ml-10 right-sidebar-toggle"><i
                        class="ik ik-message-square"></i></button>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0)" id="userDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        {{--                        User photo--}}
                        @if(Auth::user()->details && Auth::user()->details->photo)
                            <img src="{{asset('storage')}}/user/photo/{{Auth::user()->details->photo}}"
                                 alt="{{Auth::user()->details->photo}}" class="avatar rounded-circle"/>
                        @else
                            <img src="{{asset('resources')}}/img/user.jpg" class="avatar rounded-circle"/>
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        {{--                        Profile--}}
                        <a class="dropdown-item" href="{{route('hrm.profile.index')}}"><i
                                class="ik ik-user dropdown-icon"></i>
                            {{__('Profile')}}</a>
                        {{--                        settings--}}
                        <a class="dropdown-item" href="{{route('hrm.settings.index')}}"><i
                                class="ik ik-settings dropdown-icon"></i> {{__('Settings')}}</a>
                        {{--                        Logout start--}}
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                class="ik ik-power dropdown-icon"></i> {{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        {{--                        logout end--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
{{--Header End--}}
