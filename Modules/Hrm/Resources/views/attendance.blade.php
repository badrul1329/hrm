@extends('hrm::master')
@section('title')
    {{__('Attendance')}}
@endsection
@section('content')
    {{--    Content Start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-10">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Attendances')}}</h5>
                            <span>{{__('Attendance times are based on 24 Hours')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    {{--                    Real time Clock--}}
                    <div id="clock" class="h1 text-success pull-right">
                        <span class="hours"></span> <span class="delemeter invisible">:</span>
                        <span class="minutes"></span> <span class="delemeter invisible">:</span>
                        <span class="seconds"></span>
                        {{--                        <span id="ampm"></span>--}}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3> {{__('Attendances')}} </h3>
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            {{--                            Attendance List start--}}
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Employee')}}</th>
                                    <th>{{__('Check In')}}</th>
                                    <th>{{__('Check Out')}}</th>
                                    <th>{{__('Length')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            {{--                            Attendance List End--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Holiday Modal -->
    <div class="modal animated zoomIn faster" id="attendanceModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Schedule')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
{{--                Form Start--}}
                <form id="#" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Employee')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select id="employee" class="form-control select2" name="employee" disabled>
                                    <option>Loading..</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Check In')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control time" type="text" placeholder="{{__('From time')}}"
                                       name="checkin"
                                       required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Check Out')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control time" type="text" placeholder="{{__('To time')}}"
                                       name="checkout"
                                       required/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary update-btn">{{__('Update')}}</button>
                    </div>
                </form>
{{--                Form End--}}
            </div>
        </div>
    </div>
    {{--    Content End--}}
@endsection
@section('extraCSS')
    <style>
        #clock {
            /*display: flex;*/
            /*font-family:"Agency FB";*/
            font-weight: bolder;
            text-shadow: 0 2px 2px #666;
        }
    </style>
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            // Real time clock Start
            var $hOut = $('.hours'),
                $mOut = $('.minutes'),
                $sOut = $('.seconds');

            // $ampmOut = $('#ampm');

            function update() {
                var date = new Date();

                // var ampm = date.getHours() < 12
                //     ? 'AM'
                //     : 'PM';

                // var hours = date.getHours() == 0
                //     ? 12
                //     : date.getHours() > 12
                //         ? date.getHours() - 12
                //         : date.getHours();

                var hours = date.getHours() < 10
                    ? '0' + date.getHours()
                    : date.getHours();

                var minutes = date.getMinutes() < 10
                    ? '0' + date.getMinutes()
                    : date.getMinutes();

                var seconds = date.getSeconds() < 10
                    ? '0' + date.getSeconds()
                    : date.getSeconds();

                $hOut.text(hours);
                $mOut.text(minutes);
                $sOut.text(seconds);
                // $ampmOut.text(ampm);
                $('.delemeter').toggleClass('invisible');
            }

            update();
            window.setInterval(update, 1000);
            // Real time clock end

            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('attendances.index') }}",
                deferRender: true,
                columns: [
                    // {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'date', name: 'date'},
                    {data: 'name', name: 'name'},
                    {data: 'checkin', name: 'from'},
                    {data: 'checkout', name: 'to'},
                    {data: 'length', name: 'length'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            // Load Employee List
            loadEmployee();

            function loadEmployee() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#employee').empty();
                        $('#employee').append('<option selected value="">None</option>');
                        $.each(response, function (i, employee) {
                            $('#employee').append($('<option>', {
                                value: employee.id,
                                text: employee.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            // Reset Form
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
            });
            // Show Modal
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var modal = $(this);
                modal.find('.modal-title').text('{{__("Edit Attendance")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('attendances.index')}}/' + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('[name=employee]').val(response.employee_id).trigger('change');
                        modal.find('input[name=checkin]').val(moment(response.checkin, "H:m:s").format("HH:mm"));
                        modal.find('input[name=checkout]').val(moment(response.checkout, "H:m:s").format("HH:mm"));
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
                modal.find('button[type=submit]').val(id);
            });
            // Update Attendance Status
            $(document).on('click', '.check-btn', function () {
                let confirmation = confirm("{{__('Are you sure')}} ?");
                if (!confirmation) {
                    return;
                }
                let id = $(this).data('id');
                let date = $(this).data('date');
                let present = $(this).data('present');
                $.ajax({
                    url: '{{route('attendances.store')}}',
                    type: 'POST',
                    data: {
                        id: id,
                        date: date,
                        present: present
                    },
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Checked Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Update Event Listener
            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('attendances.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#attendanceModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Clear Attendance
            $(document).on('click', '.clear-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to clear')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('attendances.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Cleared Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

