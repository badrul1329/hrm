@extends('hrm::master')
@section('title')
{{__('Salary Generator')}}
@endsection
@section('content')
{{--    Content start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Salaries')}}</h5>
                            <span>{{__('Generate salary on specific department,division or employees')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Salary')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Generator')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-6 m-auto">
                            <div class="card-search with-adv-search dropdown">
                                <form action="javascript:void(0)">
                                    <input type="text" class="form-control global_filter" id="global_filter"
                                           placeholder="{{__('Search')}}.." required>
                                    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
                                    <button type="button" id="adv_wrap_toggler"
                                            class="adv-btn ik ik-chevron-down dropdown-toggle" data-toggle="dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="adv-search-wrap dropdown-menu dropdown-menu-right"
                                         aria-labelledby="adv_wrap_toggler">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col1_filter" placeholder="{{__('Name')}}" data-column="1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter departmentList"
                                                            id="col2_filter" data-column="2">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter divisionList" id="col3_filter"
                                                            data-column="3">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select class="select2 column_filter generatorList" id="col6_filter"
                                                            data-column="6">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @can('hrm-salary-add')
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#divisionModal" data-whatever="0"> + {{__('Generate Salary')}}
                            </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Department')}}</th>
                                    <th>{{__('Division')}}</th>
                                    <th>{{__('From')}}</th>
                                    <th>{{__('To')}}</th>
                                    <th>{{__('Generated By')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Divisional salary Modal -->
    <div class="modal animated zoomIn faster" id="divisionModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Division')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
{{--                form start--}}
                <form id="DivisionForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Department')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="department" name="department">
                                    <option value="">Loading ..</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Division')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="division" name="division">
                                    <option value="">Loading ..</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Employee')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="employee" name="employee">
                                    <option value="">Loading ..</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('From')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date required" placeholder="from date"
                                       name="from"
                                       value="{{\Carbon\Carbon::now()->subMonth(1)->startOfMonth()->format('d-M-Y')}}"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('To')}}<span class="text-danger">*</span></label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date required" placeholder="to date" name="to"
                                       value="{{\Carbon\Carbon::now()->subMonth(1)->endOfMonth()->format('d-M-Y')}}"
                                       required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary save-btn">{{__('Generate')}}</button>
                    </div>
                </form>
{{--                form end--}}
            </div>
        </div>
    </div>
{{--    Content end--}}
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('salaries.generate.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'employee.name', name: 'employee.name'},
                    {data: 'department', name: 'department'},
                    {data: 'division', name: 'division'},
                    {data: 'from', name: 'from'},
                    {data: 'to', name: 'to'},
                    {data: 'generated_by.name', name: 'generated_by.name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            // Search API start
            function filterColumn(i) {
                oTable.column(i).search(
                    $('#col' + i + '_filter').val(),
                    false,
                    true
                ).draw();
            }

            function filterGlobal() {
                oTable.search(
                    $('#global_filter').val(),
                    false,
                    true
                ).draw();
            }

            $('input.global_filter').on('keyup', function () {
                filterGlobal();
            });
            $('.column_filter').on('keyup change', function () {
                filterColumn($(this).attr('data-column'));
            });
            // Search API end
            $(".select2").select2();
            // Load department list
            loadDepartment();

            function loadDepartment() {
                $.ajax({
                    url: '{{route('department.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#department,.departmentList').empty();
                        $('#department').append('<option selected value="" disabled>{{__("Select a department")}}</option>');
                        $('.departmentList').append('<option selected value="">{{__("Select a department")}}</option>');
                        $.each(response, function (i, department) {
                            $('#department').append($('<option>', {
                                value: department.id,
                                text: department.name
                            }));
                        });
                        $.each(response, function (i, department) {
                            $('.departmentList').append($('<option>', {
                                value: department.name,
                                text: department.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            // Load division list
            loadDivisions();

            function loadDivisions() {
                $.ajax({
                    url: '{{route('division.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#division,.divisionList').empty();
                        $('#division').append('<option selected value="" disabled>{{__("Select division")}}</option>');
                        $('.divisionList').append('<option selected value="">{{__("Select division")}}</option>');
                        $.each(response, function (i, division) {
                            $('#division').append($('<option>', {
                                value: division.id,
                                text: division.name
                            }));
                        });
                        $.each(response, function (i, division) {
                            $('.divisionList').append($('<option>', {
                                value: division.name,
                                text: division.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            // Load Employee list
            loadEmployee();

            function loadEmployee() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#employee,.generatorList').empty();
                        $('#employee').append('<option selected value="" disabled>{{__("Select an employee")}}</option>');
                        $('.generatorList').append('<option selected value="">{{__("Select an employee")}}</option>');
                        $.each(response, function (i, employee) {
                            $('#employee').append($('<option>', {
                                value: employee.id,
                                text: employee.name
                            }));
                        });
                        $.each(response, function (i, employee) {
                            $('.generatorList').append($('<option>', {
                                value: employee.name,
                                text: employee.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            // Save event listener
            $(document).on('click', '.save-btn', function () {
                let data = $('form').serialize();
                $.ajax({
                    url: '{{route('salaries.generate.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#divisionModal').modal('toggle');
                        $('form').trigger("reset");
                        $(".select2").select2();
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            // Delete event listener
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('salaries.salary.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

