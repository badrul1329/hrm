@extends('hrm::master')
@section('title')
{{__('Profile')}}
@endsection
@section('content')
{{--    content start--}}
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-file-text bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Profile')}}</h5>
                            <span>{{__('Employee profile with details')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Employee')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Profile')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
{{--                            Profile Photo--}}
                            @if($employee->photo)
                                <img src="{{asset('storage')}}/hrm/employee/photo/{{$employee->photo}}"
                                     alt="{{$employee->name}}" class="rounded-circle" width="150"/>
                            @else
                                <img src="{{asset('resources')}}/img/user.jpg" class="rounded-circle" width="150"/>
                            @endif
                            <h4 class="card-title mt-10">{{$employee->name ?? ''}}</h4>
{{--                            Social button start--}}
                            <div class="m-auto">
                                <button class="btn btn-icon btn-facebook"><i class="fab fa-facebook-f"></i></button>
                                <button class="btn btn-icon btn-twitter"><i class="fab fa-twitter"></i></button>
                                <button class="btn btn-icon btn-instagram"><i class="fab fa-instagram"></i></button>
                            </div>
{{--                            Social button end--}}
                        </div>
                    </div>
                    <hr class="mb-0">
                    <div class="card-body">
                        <small class="text-muted d-block">{{__('Email')}} </small>
                        <h6>{{($employee->email) ?? ''}}</h6>
                        <small class="text-muted d-block pt-10">{{__('Phone')}}</small>
                        <h6>{{$employee->phone ?? ''}}</h6>
                        <small class="text-muted d-block pt-10">{{__('Address')}}</small>
                        <h6>{{$employee->address->street ?? ''}}
                            , {{$employee->address->city ?? ''}}
                            , {{$employee->address->district ?? ''}}
                            , {{$employee->address->zip_code ?? ''}}
                            , {{$employee->address->country ?? ''}}</h6>
                        <div class="map-box">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d456.54204407280827!2d90.3856132251634!3d23.735382185787646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b98b5de435c7%3A0xd386ad8245260a9d!2sPlanet+Hack+Bangladesh+Limited!5e0!3m2!1sen!2sbd!4v1554537349481!5m2!1sen!2sbd"
                                width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-7">
                <div class="card">
                    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month"
                               role="tab"
                               aria-controls="pills-profile" aria-selected="true">{{__('General')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-official-tab" data-toggle="pill" href="#official"
                               role="tab" aria-controls="pills-official" aria-selected="false">{{__('Official')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
{{--                        General Info tab start--}}
                        <div class="tab-pane fade show active" id="last-month" role="tabpanel"
                             aria-labelledby="pills-profile-tab">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-4"><strong>{{__('Name')}}</strong>
                                        <br>
                                        <p class="text-muted">{{$employee->name ?? ''}}</p>
                                    </div>
                                    <div class="col-md-4 col-4"><strong>{{__('Mobile')}}</strong>
                                        <br>
                                        <p class="text-muted">{{$employee->phone ?? ''}}</p>
                                    </div>
                                    <div class="col-md-4 col-4"><strong>{{__('Email')}}</strong>
                                        <br>
                                        <p class="text-muted">{{$employee->email ?? ''}}</p>
                                    </div>
                                </div>
                                <div class="mt-30">
                                    <h6>Personal Info</h6>
                                    <hr/>
                                    <ul class="offset-md-2 list-group-flush">
                                        <li class="list-group-item border-top-0">{{__('Gender')}}
                                            <span class="float-right">{{($employee->gender) ? 'Female':'Male'}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('Birthday')}} <span
                                                class="float-right">{{$employee->birthday ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item border-bottom-0">{{__('Marital Status')}} <span
                                                class="float-right">{{$employee->maritalStatus->name ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item border-bottom-0">{{__('Religion')}} <span
                                                class="float-right">{{$employee->religion->name ?? ''}}</span>
                                        </li>
                                    </ul>
                                    <h6>Contact Info</h6>
                                    <hr/>
                                    <ul class="offset-md-2 list-group-flush border-0">
                                        <li class="list-group-item border-top-0">{{__('Street/Holding No.')}} <span
                                                class="float-right">{{$employee->address->street ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('City')}} <span
                                                class="float-right">{{$employee->address->city ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('District')}} <span
                                                class="float-right">{{$employee->address->district ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('Zip code')}} <span
                                                class="float-right">{{$employee->address->zip_code ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item border-bottom-0">{{__('Country')}} <span
                                                class="float-right">{{$employee->address->country ?? ''}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {{--                        General Info tab end--}}
                        {{--                        Official Info tab start--}}
                        <div class="tab-pane fade" id="official" role="tabpanel"
                             aria-labelledby="pills-official-tab">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-3"><strong>{{__('Division')}}</strong>
                                        <br>
                                        <p class="text-muted">{{$employee->official->division()->first()->name ?? ''}}</p>
                                    </div>
                                    <div class="col-md-3 col-3"><strong>{{__('Position')}}</strong>
                                        <br>
                                        <p class="text-muted">{{$employee->official->position()->first()->name ?? ''}}</p>
                                    </div>
                                    <div class="col-md-3 col-3"><strong>{{__('Shift')}}</strong>
                                        <br>
                                        <p class="text-muted">
                                            @if($employee->official->shift)
                                                {{$employee->official->shift()->first()->name}} ( {{$employee->official->shift()->first()->from}} - {{$employee->official->shift()->first()->to}})
                                            @endif
                                        </p>
                                    </div>
                                    <div class="col-md-3 col-3"><strong>{{__('Grade')}}</strong>
                                        <br>
                                        <p class="text-muted">{{$employee->official->grade()->first()->name ?? ''}}</p>
                                    </div>
                                </div>
                                <div class="mt-30">
                                    <hr/>
                                    <ul class="offset-md-2 list-group-flush">
                                        <li class="list-group-item border-top-0">{{__('Supervisor')}} <span
                                                class="float-right">{{$employee->official->supervisor()->first()->name ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('Job Type')}}
                                            <span
                                                class="float-right">{{$employee->official->job_type()->first()->type ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('From')}} <span
                                                class="float-right">{{$employee->official->from ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item border-bottom-0">{{__('To')}}<span
                                                class="float-right">{{$employee->official->to ?? ''}}</span>
                                        </li>
                                    </ul>
                                    <h6>{{__('Documents')}}</h6>
                                    <hr/>
                                    <ul class="offset-md-2 list-group-flush border-0">
                                        @foreach($employee->documents as $document)
                                            @if($loop->first)
                                                <li class="list-group-item border-top-0">
                                                    <a href="{{asset('storage/hrm/employee/files/'.$document->file)}}"
                                                       target="_blank">
                                                        <i class="ik ik-file ik-2x"></i> {{$document->title}}
                                                    </a>
                                                </li>
                                            @else
                                                <li class="list-group-item">
                                                    <a href="{{asset('storage/hrm/employee/files/'.$document->file)}}"
                                                       target="_blank">
                                                        <i class="ik ik-file ik-2x"></i> {{$document->title}}
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                        {{--                        Official Info tab end--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    content end--}}
@endsection
