<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('hrm')->group(function () {
    Route::get('/', [\Modules\Hrm\Http\Controllers\HrmController::class, 'index'])->name('hrm');
    Route::prefix('employee')->group(function () {
        Route::get('/departments/all', [\Modules\Hrm\Http\Controllers\DepartmentController::class, 'all'])->name('department.all');
        Route::get('/positions/all', [\Modules\Hrm\Http\Controllers\PositionController::class, 'all'])->name('position.all');
        Route::get(' / divisions / all', [\Modules\Hrm\Http\Controllers\DivisionController::class, 'all'])->name('division.all');
        Route::get('/employees/all', [\Modules\Hrm\Http\Controllers\EmployeeController::class, 'all'])->name('employee.all');
        Route::apiResources([
            'departments' => \Modules\Hrm\Http\Controllers\DepartmentController::class,
            'positions' => \Modules\Hrm\Http\Controllers\PositionController::class,
            'divisions' => \Modules\Hrm\Http\Controllers\DivisionController::class,
            'employees' => \Modules\Hrm\Http\Controllers\EmployeeController::class,
            'reschedules' => \Modules\Hrm\Http\Controllers\RescheduleController::class
        ]);
        Route::name('employees.')->group(function () {
            Route::apiResource('documents', \Modules\Hrm\Http\Controllers\EmployeeDocController::class);
        });
    });
    Route::get('/job-type/all', [\Modules\Hrm\Http\Controllers\JobTypeController::class, 'all'])->name('job_type.all');
    Route::get('/shift-schedules/all', [\Modules\Hrm\Http\Controllers\ShiftScheduleController::class, 'all'])->name('shift_schedule.all');
    Route::apiResources([
        'allowances' => \Modules\Hrm\Http\Controllers\AllowanceController::class,
        'shift-schedules' => \Modules\Hrm\Http\Controllers\ShiftScheduleController::class,
        'attendances' => \Modules\Hrm\Http\Controllers\AttendanceController::class,
        'loans' => \Modules\Hrm\Http\Controllers\LoanController::class,
    ]);
    Route::prefix('loan')->group(function () {
        Route::get('employees /{employee}/installments /{date?}', [\Modules\Hrm\Http\Controllers\LoanInstallmentController::class, 'installment'])->name('loans.employee.installment');
        Route::apiResources([
            'installments' => \Modules\Hrm\Http\Controllers\LoanInstallmentController::class
        ]);
    });
    Route::prefix('salaries')->name('salaries.')->group(function () {
        Route::apiResource('/generate', \Modules\Hrm\Http\Controllers\SalaryGenerationController::class)->only(['index','store']);
        Route::get('/salary/{salary}/change-status',[\Modules\Hrm\Http\Controllers\SalaryController::class, 'changeStatus'])->name('salary.change-status');
        Route::apiResource('/salary', \Modules\Hrm\Http\Controllers\SalaryController::class);
    });

    Route::prefix('leave')->name('leave.')->group(function () {
        Route::get('/types/all', [\Modules\Hrm\Http\Controllers\LeaveTypeController::class, 'all'])->name('types.all');
        Route::apiResources([
            'holidays' => \Modules\Hrm\Http\Controllers\HolidayController::class,
            'types' => \Modules\Hrm\Http\Controllers\LeaveTypeController::class,
            'applications' => \Modules\Hrm\Http\Controllers\LeaveApplicationController::class,
        ]);
    });
    Route::get('/grade/all', [\Modules\Hrm\Http\Controllers\GradeController::class, 'all'])->name('grade.all');
    Route::apiResource('grades', \Modules\Hrm\Http\Controllers\GradeController::class);

    Route::prefix('trainings')->name('trainings.')->group(function () {
        Route::get('/types/all', [\Modules\Hrm\Http\Controllers\TrainingTypeController::class, 'all'])->name('types.all');
        Route::get('/trainers/all', [\Modules\Hrm\Http\Controllers\TrainerController::class, 'all'])->name('trainers.all');
        Route::apiResources([
            'types' => \Modules\Hrm\Http\Controllers\TrainingTypeController::class,
            'trainers' => \Modules\Hrm\Http\Controllers\TrainerController::class,
            'workshops' => \Modules\Hrm\Http\Controllers\WorkshopController::class
        ]);
    });
    Route::prefix('report')->name('report.')->group(function () {
        Route::get('loan', [\Modules\Hrm\Http\Controllers\ReportController::class, 'loan'])->name('loan');
    });
    Route::name('hrm.')->group(function () {
        Route::apiResource('/settings', \Modules\Hrm\Http\Controllers\SettingsController::class)->only(['index','store']);
        Route::apiResource('/settings', \Modules\Hrm\Http\Controllers\SettingsController::class)->only(['index','store']);
    });

//    Common Menu items
    Route::get('/notices', [\Modules\Hrm\Http\Controllers\Common\NoticeController::class, 'index'])->name('hrm.notices.index');
    Route::get('/messenger', [\Modules\Hrm\Http\Controllers\Common\MessengerController::class, 'index'])->name('hrm.messenger.index');
    Route::get('/calender', [\Modules\Hrm\Http\Controllers\Common\CalenderController::class, 'index'])->name('hrm.calender.index');
    Route::get('/activities', [\Modules\Hrm\Http\Controllers\Common\ActivityController::class, 'index'])->name('hrm.activities.index');
    Route::get('/users', [\Modules\Hrm\Http\Controllers\Common\UserController::class, 'index'])->name('hrm.users.index');
    Route::get('/roles', [\Modules\Hrm\Http\Controllers\Common\RoleController::class, 'index'])->name('hrm.roles.index');
    Route::get('/user-role', [\Modules\Hrm\Http\Controllers\Common\UserRoleController::class, 'index'])->name('hrm.user-role.index');
    Route::get('/role-permission', [\Modules\Hrm\Http\Controllers\Common\RolePermissionController::class, 'index'])->name('hrm.role-permission.index');
    Route::get('/user-permission', [\Modules\Hrm\Http\Controllers\Common\UserPermissionController::class, 'index'])->name('hrm.user-permission.index');
    Route::get(' / languages', [\Modules\Hrm\Http\Controllers\Common\LanguageController::class, 'index'])->name('hrm.languages.index');
    Route::get('/languages/{language}/phrases',[\Modules\Hrm\Http\Controllers\Common\LanguagePhraseController::class, 'index'])->name('hrm.languages.phrases.index');
    Route::get('/phrases', [\Modules\Hrm\Http\Controllers\Common\PhraseController::class, 'index'])->name('hrm.phrases.index');
    Route::get('/currencies', [\Modules\Hrm\Http\Controllers\Common\CurrencyController::class, 'index'])->name('hrm.currencies.index');

    Route::post('/profile/change-password', [\Modules\Hrm\Http\Controllers\Common\ProfileController::class,'changePass'])->name('profile.update-password');
    Route::get('/profile', [\Modules\Hrm\Http\Controllers\Common\ProfileController::class, 'index'])->name('hrm.profile.index');
    Route::post('profile', [\Modules\Hrm\Http\Controllers\Common\ProfileController::class,'store']);
});
