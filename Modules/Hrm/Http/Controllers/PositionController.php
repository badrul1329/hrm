<?php

namespace Modules\Hrm\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\Position;
use Illuminate\Support\Facades\Validator;

class PositionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $positions = Position::all();
            return Datatables::of($positions)
                ->addIndexColumn()
                ->addColumn('action', function ($position) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-position-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#positionModal" data-whatever="1" data-id="' . $position->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-position-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $position->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::position');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:positions',
            'desc' => 'required',
        ];
        $messages = [
            'desc.required' => 'The description field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $position = new Position();
        $position->name = $request->name;
        $position->desc = $request->desc;
        $position->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Position $position)
    {
        if ($request->ajax()) {
            return $position;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Position $position)
    {
        $rules = [
            'name' => 'required|unique:positions,name,'.$position->id,
            'desc' => 'required',
        ];
        $messages = [
            'desc.required' => 'The description field is required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $position->name = $request->name;
        $position->desc = $request->desc;
        $position->save();
        return $position;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Position $position)
    {
        $position->delete();
        return $position;
    }

    /**
     * all positions.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if ($request->ajax()) {
            return Position::all();
        }
    }

}
