<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Hrm\Entities\JobType;

class JobTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Return all job Types.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if($request->ajax()){
            return JobType::all();
        }
    }
}
