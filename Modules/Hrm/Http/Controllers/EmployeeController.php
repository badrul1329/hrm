<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeAddress;
use Modules\Hrm\Entities\GradeAllowance;
use Modules\Hrm\Entities\EmployeeDocument;
use Modules\Hrm\Entities\EmployeeOfficial;
use Modules\Hrm\Entities\EmployeeSalary;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $employees = Employee::with('maritalStatus', 'religion', 'address', 'official.division', 'official.position', 'official.job_type')->get();
            return Datatables::of($employees)
                ->addIndexColumn()
                ->editColumn('gender', function ($employee) {
                    if ($employee->gender) {
                        return "Female";
                    } else {
                        return 'Male';
                    }
                })
                ->addColumn('action', function ($employee) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-employee-view')) {
                        $btn .= '<a href="' . url("hrm/employee/employees/$employee->id") . '" type="button"><i class="ik ik-eye"></i></a>';
                    }
                    if (Auth::user()->can('hrm-employee-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#employeeModal" data-whatever="1" data-id="' . $employee->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-employee-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $employee->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::employee');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
            $rules = [
                'photo' => 'nullable',
                'name' => 'required',
                'email' => 'nullable|unique:employees,email',
                'phone' => 'required|unique:employees,phone',

                'street' => 'required',
                'city' => 'required',
                'district' => 'required',
                'zip' => 'required',
                'country' => 'required',

                'gender' => 'required',
                'birthday' => 'nullable',
                'religion' => 'required',
                'marital_status' => 'required',

                'position' => 'required',
                'division' => 'required',
                'supervisor' => 'nullable',
                'job_type' => 'required',
                'from' => 'required',
                'to' => 'nullable',
                'shift' => 'required',
                'grade' => 'required',
                'doc.*.title' => 'string|nullable',
                'doc.*.doc' => 'file|nullable',

            ];
            $messages = [
                'name.required' => 'The name field is required.',
                'marital_id.required' => 'The marital status is required.',
                'job_type.required' => 'The job type is required.',
            ];
            Validator::make($request->all(), $rules, $messages)->validate();
            $employee = new Employee();
            $employee->name = $request->name;
            $employee->email = $request->email;
            $employee->phone = $request->phone;
            $employee->gender = $request->gender;
            $employee->birthday = date('Y-m-d', strtotime($request->dob));
            $employee->marital_id = $request->marital_status;
            $employee->religion_id = $request->religion;
            if ($request->hasFile('photo')) {
                $value = $request->photo;
                $extension = $value->getClientOriginalExtension();
//            $name = $value->getClientOriginalName();
                $image = microtime(true) . ".$extension";
                Image::make($value)->resize(400, 400)->save(storage_path('/app/public/hrm/employee/photo/') . $image);
                $employee->photo = $image;
            }
            $employee->save();

            $address = new EmployeeAddress();
            $address->street = $request->street;
            $address->city = $request->city;
            $address->district = $request->district;
            $address->country = $request->country;
            $address->zip_code = $request->zip;
            $employee->address()->save($address);

            $official = new EmployeeOfficial();
            $official->position = $request->position;
            $official->division = $request->division;
            $official->supervisor = $request->supervisor;
            $official->job_type = $request->job_type;
            $official->from = date('Y-m-d', strtotime($request->from));
            $official->to = date('Y-m-d', strtotime($request->to));
            $official->shift = $request->shift;
            $official->grade_id = $request->grade;
            $employee->official()->save($official);

            foreach ($request->doc as $doc) {
                if ($doc['title'] && $doc['doc']) {
                    $document = new EmployeeDocument();
                    $document->title = $doc['title'];
                    $extension = $doc['doc']->getClientOriginalExtension();
                    $name = microtime(true) . ".$extension";
                    $doc['doc']->move(storage_path('/app/public/hrm/employee/files/'), $name);
                    $document->file = $name;
                    $employee->documents()->save($document);
                }
            }
        if($employee && $employee->address && $employee->official){
            DB::commit();
            return $employee->load('maritalStatus', 'religion', 'address', 'official', 'documents');
        }else{
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Employee $employee)
    {
        $employee->load('maritalStatus', 'religion', 'address', 'official.division', 'official.position', 'official.shift', 'official.grade', 'official.supervisor', 'documents');
        if ($request->ajax()) {
            return $employee;
        }
        return view('hrm::employee_profile', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Employee $employee)
    {
        DB::beginTransaction();
            $rules = [
                'photo' => 'nullable',
                'name' => 'required',
                'email' => 'nullable|unique:employees,email,' . $employee->id,
                'phone' => 'required|unique:employees,phone,' . $employee->id,

                'street' => 'required',
                'city' => 'required',
                'district' => 'required',
                'zip' => 'required',
                'country' => 'required',

                'gender' => 'required',
                'birthday' => 'nullable',
                'religion' => 'required',
                'marital_status' => 'required',

                'position' => 'required',
                'division' => 'required',
                'supervisor' => 'nullable',
                'job_type' => 'required',
                'from' => 'required',
                'to' => 'nullable',
                'shift' => 'required',
                'grade' => 'required',
//            'doc.*.title' => 'string|nullable',
//            'doc.*.doc' => 'file|nullable',

            ];
            $messages = [
                'name.required' => 'The name field is required.',
                'marital_status.required' => 'The marital status is required.',
                'job_type.required' => 'The job type is required.',
            ];
            Validator::make($request->all(), $rules, $messages)->validate();
            $employee->name = $request->name;
            $employee->email = $request->email;
            $employee->phone = $request->phone;
            $employee->gender = $request->gender;
            $employee->birthday = date('Y-m-d', strtotime($request->dob));
            $employee->marital_id = $request->marital_status;
            $employee->religion_id = $request->religion;
            if ($request->hasFile('photo')) {
                $value = $request->photo;
                $extension = $value->getClientOriginalExtension();
//            $name = $value->getClientOriginalName();
                $image = microtime(true) . ".$extension";
                Image::make($value)->resize(400, 400)->save(storage_path('/app/public/hrm/employee/photo/') . $image);
                File::delete(storage_path('/app/public/hrm/employee/photo/') . $employee->photo);
                $employee->photo = $image;
            }
            $employee->address->street = $request->street;
            $employee->address->city = $request->city;
            $employee->address->district = $request->district;
            $employee->address->country = $request->country;
            $employee->address->zip_code = $request->zip;

            $employee->official->position = $request->position;
            $employee->official->division = $request->division;
            $employee->official->supervisor = $request->supervisor;
            $employee->official->job_type = $request->job_type;
            $employee->official->from = date('Y-m-d', strtotime($request->from));
            $employee->official->to = date('Y-m-d', strtotime($request->to));
            $employee->official->shift = $request->shift;
            $employee->official->grade_id = $request->grade;

//        if (count($request->doc)) {
//            foreach ($request->doc as $doc) {
//                if ($doc['title'] && $doc['doc']) {
//                    $document = new EmployeeDocument();
//                    $document->title = $doc['title'];
//                    $extension = $doc['doc']->getClientOriginalExtension();
//                    $name = microtime(true) . ".$extension";
//                    $doc['doc']->move(storage_path('/app/public/hrm/employee/files/'), $name);
//                    $document->file = $name;
//                    $employee->documents()->save($document);
//                }
//            }
//        }
            $employee->push();

        if($employee && $employee->address && $employee->official){
            DB::commit();
            return $employee->load('maritalStatus', 'religion', 'address', 'official', 'documents');
        }else{
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Employee $employee)
    {
        File::delete(storage_path('/app/public/hrm/employee/photo/') . $employee->photo);
        foreach ($employee->documents as $document) {
            File::delete(storage_path('/app/public/hrm/employee/files/') . $document->file);
        };
        $employee->delete();
        return $employee;
    }

    /**
     * Return all employees.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if ($request->ajax()) {
            return Employee::all();
        };
    }
}
