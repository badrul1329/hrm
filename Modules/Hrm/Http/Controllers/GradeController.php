<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Allowance;
use Modules\Hrm\Entities\Grade;
use Modules\Hrm\Entities\GradeAllowance;
use Yajra\DataTables\DataTables;

class GradeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $grades = Grade::all();
            return Datatables::of($grades)
                ->addIndexColumn()
                ->editColumn('type', function ($grade) {
                    if ($grade->type) {
                        return "Hourly";
                    }
                    return 'Monthly';
                })
                ->addColumn('net', function ($grade) {
                    $net = $grade->rate;
                    foreach ($grade->allowances as $allowance) {
                        if ($allowance->type) {
                            //percentage
                            $amount = ($allowance->rate * 100) / $grade->rate;
                        } else {
                            //cash
                            $amount = $allowance->rate;
                        }
                        if ($allowance->allowance->type) {
                            //deduction
                            $net -= $amount;
                        } else {
                            //addition
                            $net += $amount;
                        }
                    }
                    return round($net, 2);
                })
                ->addColumn('action', function ($grade) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-salary-grade-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#gradeModal" data-whatever="1" data-id="' . $grade->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-salary-grade-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $grade->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $allowances = Allowance::all();
        $additiveAllowances = $allowances->where('type', 0)->all();
        $deductiveAllowances = $allowances->where('type', 1)->all();
        return view('hrm::grade', compact('additiveAllowances', 'deductiveAllowances'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hrm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:grades,name',
            'type' => 'required',
            'rate' => 'required|integer| min:0',
            'orate' => 'required|integer| min:0',
            'allowance.*.rate' => 'required|integer|min:0',
            'allowance.*.type' => 'required|integer',

        ];
        $messages = [
            'type.required' => 'The Salary type is required.',
            'orate.required' => 'The Overtime rate is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $grade = new Grade();
        $grade->name = $request->name;
        $grade->type = $request->type;
        $grade->rate = $request->rate;
        $grade->overtime_rate = $request->orate;
        $grade->save();
        foreach ($request->allowance as $key => $value) {
            $allowance = new GradeAllowance();
            $allowance->allowance_id = $key;
            $allowance->type = $value['type'];
            $allowance->rate = $value['rate'];
            $grade->allowances()->save($allowance);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Grade $grade)
    {
        if ($request->ajax()) {
            return $grade->load('allowances');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Grade $grade)
    {
        $rules = [
            'name' => 'required|unique:grades,name,' . $grade->id,
            'type' => 'required',
            'rate' => 'required|integer| min:0',
            'orate' => 'required|integer| min:0',
            'allowance.*.rate' => 'required|integer|min:0',
            'allowance.*.type' => 'required|integer',

        ];
        $messages = [
            'type.required' => 'The Salary type is required.',
            'orate.required' => 'The Overtime rate is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $grade->name = $request->name;
        $grade->type = $request->type;
        $grade->rate = $request->rate;
        $grade->overtime_rate = $request->orate;
        $grade->save();
        foreach ($request->allowance as $key => $value) {
            GradeAllowance::where('allowance_id', $key)->update([
                'type' => $value['type'],
                'rate' => $value['rate']
            ]);
        }
        return $grade->load('allowances');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, Grade $grade)
    {
        if ($request->ajax()) {
            $grade->delete();
        }
    }

    public function all(Request $request)
    {
        if ($request->ajax()) {
            return Grade::all();
        }
    }

}
