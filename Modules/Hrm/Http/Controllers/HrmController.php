<?php

namespace Modules\Hrm\Http\Controllers;

use App\Models\Notice;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\Department;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeOfficial;
use Modules\Hrm\Entities\LeaveApplication;
use Modules\Hrm\Entities\Loan;
use Modules\Hrm\Entities\Position;
use Modules\Hrm\Entities\Salary;
use Spatie\Activitylog\Models\Activity;

class HrmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = array();
            $data['total']['users'] = User::count();
            $data['total']['salary_paid'] = Salary::whereMonth('paid_at', date('m'))->sum('amount');
            $data['total']['application'] = LeaveApplication::whereMonth('created_at', date('m'))->count();
            $data['total']['loan'] = Loan::whereMonth('approved_at', date('m'))->sum('amount');
            $totalEmployee = Employee::count();
            $data['attendance']['present'] = Attendance::whereDate('date', date('Y-m-d'))->count();
            if($totalEmployee){
                $data['attendance']['present_percent'] = round(($data['attendance']['present'] / $totalEmployee) * 100, 2);
            }else{
                $data['attendance']['present_percent'] = 0;
            }
            $data['attendance']['absent'] = $totalEmployee - $data['attendance']['present'];
            $data['attendance']['absent_percent'] = round(100 - $data['attendance']['present_percent'], 2);
//            Designation chart data
            $designation = Position::withCount('employees')->get();
            $data['designation'] = [
                'name' => $designation->pluck('name'),
                'total' => $designation->pluck('employees_count')
            ];
//            Department chart data
            $dept = Department::withCount('employees')->get();
            $data['department'] = [
                'name' => $dept->pluck('name'),
                'total' => $dept->pluck('employees_count')
            ];
            return $data;
        }
        $notices = Notice::orderBy('id','desc')->take(6)->get();
        $activities = Auth::user()->actions()->orderBy('created_at','desc')->take(6)->get();
        return view('hrm::index',compact('notices','activities'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hrm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('hrm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('hrm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
