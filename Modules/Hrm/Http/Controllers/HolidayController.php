<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\Holiday;
use Illuminate\Support\Facades\Validator;

class HolidayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $holidays = Holiday::all();
            return Datatables::of($holidays)
                ->addIndexColumn()
                ->editColumn('from', function (Holiday $holiday) {
                    return date('d-m-Y', strtotime($holiday->from));
                })
                ->editColumn('to', function (Holiday $holiday) {
                    return date('d-m-Y', strtotime($holiday->to));
                })
                ->addColumn('days', function ($holiday) {
                    $from = Carbon::parse($holiday->from);
                    $to = Carbon::parse($holiday->to);
                    return $from->diffInDays($to) + 1;
                })
                ->addColumn('action', function ($holiday) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-holiday-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#holidayModal" data-whatever="1" data-id="' . $holiday->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-holiday-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $holiday->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::holidays');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:holidays,name',
            'from' => 'required',
            'to' => 'required',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $holiday = new Holiday();
        $holiday->name = $request->name;
        $holiday->from = date('Y-m-d', strtotime($request->from));
        $holiday->to = date('Y-m-d', strtotime($request->to));
        $holiday->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Holiday $holiday)
    {
        if ($request->ajax()) {
            return $holiday;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Holiday $holiday)
    {
        $rules = [
            'name' => 'required',
            'from' => 'required',
            'to' => 'required',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $holiday->name = $request->name;
        $holiday->from = date('Y-m-d', strtotime($request->from));
        $holiday->to = date('Y-m-d', strtotime($request->to));
        $holiday->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Holiday $holiday)
    {
        $holiday->delete();
        return $holiday;
    }
}
