<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeReschedule;
use Modules\Hrm\Entities\Holiday;
use Modules\Hrm\Entities\Salary;
use Modules\Hrm\Entities\SalaryAllowance;
use Modules\Hrm\Entities\SalaryDetails;
use Yajra\DataTables\Facades\DataTables;

class SalaryGenerationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $salaries = Salary::with('employee.official', 'generatedBy')->get();
            return Datatables::of($salaries)
                ->addIndexColumn()
                ->addColumn('department', function ($salary) {
                    return $salary->employee->official->division()->first()->department->name;
                })
                ->addColumn('division', function ($salary) {
                    return $salary->employee->official->division()->first()->name;
                })
                ->editColumn('from', function ($salary) {
                    return date('d-m-Y', strtotime($salary->from));
                })
                ->editColumn('to', function ($salary) {
                    return date('d-m-Y', strtotime($salary->to));
                })
                ->addColumn('action', function ($salary) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-salary-add')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $salary->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::salary_generator');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hrm::create');
    }

    public function calculateSalary(Employee $employee, $from, $to)
    {
        $employee->load('official.shift', 'official.grade', 'official.allowances');

//        Calculate total Working days
        $start = $employee->official->shift()->first()->from;
        $end = $employee->official->shift()->first()->to;
        $attendance = [
            'workingHour' => 0,
            'totalWorkingHour' => 0,
            'workingDay' => 0,
            'totalWorkingDay' => 0,
            'overtimeHour' => 0,
            'overtimeDay' => 0,
            'basicAmount' => 0,
            'overtimeAmount' => 0,
        ];
        if ($employee->official->grade->type) {
            //hourly
            $workingHour = $this->calculateAttendence($employee, $from, $to)[0];
            $attendance['workingHour'] += $workingHour['workingHour'];
            $attendance['workingDay'] += $workingHour['workingDay'];
            $attendance['overtimeHour'] += $workingHour['overtimeHour'];
            $attendance['overtimeDay'] += $workingHour['overtimeDay'];

            $workingTime = $this->calculateWorkingTime($start, $end, $from, $to);
            $attendance['totalWorkingDay'] += $workingTime['day'];
            $attendance['totalWorkingHour'] += $workingTime['seconds'];

            if ($attendance['workingHour']) {
                $attendance['basicAmount'] = round(($employee->official->grade->rate / 3600) * $attendance['workingHour'], 2);
            } else {
                $attendance['basicAmount'] += 0;
            }
            if ($attendance['overtimeHour']) {
                $attendance['overtimeAmount'] += round(($employee->official->grade->overtime_rate / 3600) * $attendance['overtimeHour'], 2);
            } else {
                $attendance['overtimeAmount'] += 0;
            }

        } else {
            //monthly
            $period = CarbonPeriod::create($from, '1 months', $to);
            $months = [];
            foreach ($period as $date) {
                $months[] = $date->format('d-m-Y');
            }
            foreach ($months as $month) {
                $month = Carbon::parse($month);
//                Calculate months working Hours
                if (($month->format('m-Y')) === (Carbon::parse($from)->format('m-Y'))) {
                    $startingPoint = $from;
                } else {
                    $startingPoint = $month->startOfMonth();
                }

                if (($month->format('m-Y')) === (Carbon::parse($to)->format('m-Y'))) {
                    $endingPoint = $to;
                } else {
                    $endingPoint = $month->endOfMonth();
                }

                $workingHour = $this->calculateAttendence($employee, $startingPoint, $endingPoint)[0];

                $attendance['workingHour'] += $workingHour['workingHour'];
                $attendance['workingDay'] += $workingHour['workingDay'];
                $attendance['overtimeHour'] += $workingHour['overtimeHour'];
                $attendance['overtimeDay'] += $workingHour['overtimeDay'];
//                Calculate months total working Hours

//                $top = $month->startOfMonth();
                $top = date('Y-m-01 00:00:00', strtotime($month));
                $bottom = $month->endOfMonth();
//                $bottom = date('Y-m-t 00:00:00',strtotime($month));

                $workingTime = $this->calculateWorkingTime($start, $end, $top, $bottom);

                $attendance['totalWorkingDay'] += $workingTime['day'];
                $attendance['totalWorkingHour'] += $workingTime['seconds'];
//                calculate months salary amount
                if ($attendance['workingHour'] && $workingTime['seconds']) {
                    $attendance['basicAmount'] += round(($employee->official->grade->rate / $workingTime['seconds']) * $attendance['workingHour'], 2);
                } else {
                    $attendance['basicAmount'] += 0;
                }
                if ($attendance['overtimeHour']) {
                    $attendance['overtimeAmount'] += round(($employee->official->grade->overtime_rate / 3600) * $attendance['overtimeHour'], 2);
                } else {
                    $attendance['overtimeAmount'] += 0;
                }
            }
        }
        return $attendance;
    }

    public function calculateWorkingTime($start, $end, $from, $to)
    {
        $period = CarbonPeriod::create($from, $to);
//        working days without holidays
        $holidays = Holiday::all();
        $holidayFilter = function ($date) use ($holidays) {
            foreach ($holidays as $holiday) {
                return !(Carbon::parse($date)->between(Carbon::parse($holiday->from), Carbon::parse($holiday->to)));
            }
        };
        $period->filter($holidayFilter);
//        Working days without weekend
        Carbon::setWeekendDays([
//            Carbon::FRIDAY,
        ]);
        //return Carbon::getWeekendDays();
        $weekendFilter = function ($date) {
            return !$date->isWeekend();
        };
        $period->filter($weekendFilter);
        $workingTime = [];
        $workingTime['day'] = count($period);

        $shiftFrom = Carbon::parse($start);
        $shiftTo = Carbon::parse($end);
        $shiftSeconds = $shiftFrom->diffInSeconds($shiftTo);

        $workingTime['seconds'] = $workingTime['day'] * $shiftSeconds;
        return $workingTime;
    }

    public function calculateAttendence(Employee $employee, $from, $to)
    {
        $employee->load('attendances');
//            check attendance
        $workingHour = 0;
        $overtimeHour = 0;
        $workingDay = 0;
        $overtimeDay = 0;
        $employee->attendances->filter(function ($attendance) use ($from, $to, &$workingDay, &$workingHour, &$overtimeDay, &$overtimeHour) {
            $date = Carbon::parse($attendance->date);
            if ($date->isBetween($from, $to)) {
                $workingDay += 1;
//                    Checking reschedule has opened or not
                $reschedule = EmployeeReschedule::whereDate('date', $attendance->date)->where('employee_id', $attendance->employee_id)->first();
                if ($reschedule) {
//                      reschedule present
                    $shiftFrom = $reschedule->schedule->from;
                    $shiftTo = $reschedule->schedule->to;
                } else {
//                      reschedule not present
                    $shiftFrom = Carbon::parse($attendance->employee->official->shift()->first()->from);
                    $shiftTo = Carbon::parse($attendance->employee->official->shift()->first()->to);
                }

                $checkin = Carbon::parse($attendance->checkin);
                if ($shiftFrom->gt($checkin)) {
                    $checkin = $shiftFrom;
                } else {
                    $checkin = $checkin;
                }
                $checkout = Carbon::parse($attendance->checkout);
                $overtime = $shiftTo->floatDiffInHours($checkout, false);
                if ($overtime > 0) {
                    $workingHour += $checkin->diffInSeconds($shiftTo);
                    $overtimeHour += $shiftTo->diffInSeconds($checkout);
                    $overtimeDay += 1;
                } else {
                    $workingHour += $checkin->diffInSeconds($checkout);
                }
                return true;
            }
        });
        return array([
            "workingHour" => $workingHour,
            "workingDay" => $workingDay,
            "overtimeHour" => $overtimeHour,
            "overtimeDay" => $overtimeDay,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use($request) {
//        dd($request->all());
            $from = carbon::parse($request->from);
            $to = carbon::parse($request->to);
            $department = $request->department;
            $division = $request->division;
            $employee_id = $request->employee;

            $employees = Employee::all();
            //get all employee under department
            if ($department) {
                $employees = $employees->filter(function ($employee) use ($department) {
                    if ($employee->official->division()->first()->dept_id == $department) {
                        return true;
                    }
                    return false;
                });
            }
            //get all employee under division
            if ($division) {
                $employees = $employees->filter(function ($employee) use ($division) {
                    if ($employee->official->division == $division) {
                        return true;
                    }
                    return false;
                });
            }
            //get all employee under employee id
            if ($employee_id) {
                $employees = $employees->filter(function ($employee) use ($employee_id) {
                    if ($employee->id == $employee_id) {
                        return true;
                    }
                    return false;
                });
            }
            foreach ($employees as $employee) {
                //check already generated or not
//            some code
//            if not generated

                $workingDetails = $this->calculateSalary($employee, $from, $to);

// check leave
                $totalLeave = 0;
                $employee->leave->filter(function ($leave) use ($from, $to, &$totalLeave) {
                    $begin = Carbon::parse($leave->from);
                    $end = Carbon::parse($leave->to);
                    $period = CarbonPeriod::create($begin, $end);
                    foreach ($period as $date) {
                        if (Carbon::parse($date)->isBetween($from, $to)) {
                            $totalLeave++;
                        }
                    };
                });
// check loan
                $loans = $employee->loan->filter(function ($loan) use ($from, $to) {
                    $repayment_from = Carbon::parse($loan->repayment_from);
                    $repayment_to = Carbon::parse($loan->repayment_to);
                    if ($from->isBetween($repayment_from, $repayment_to) && $to->isBetween($repayment_from, $repayment_to)) {
                        return true;
                    }
                    return false;
                })->sum(function ($loan) {
                    return round($loan->amount / $loan->installment, 2);
                });
// check installment
                $installment = $employee->installment->filter(function ($installment) use ($from, $to) {
                    $date = Carbon::parse($installment->date);
                    if ($date->isBetween($from, $to)) {
                        return true;
                    }
                    return false;
                })->sum('paid');

                $salaryAmount = 0;

                $salary = new Salary();
                $salary->employee_id = $employee->id;
                $salary->generated_by = Auth::id();
                $salary->from = date('Y-m-d', strtotime($from));
                $salary->to = date('Y-m-d', strtotime($to));
                $salary->save();

                $details = new SalaryDetails();
                $details->type = $employee->official->grade->type;
                $details->job_type = $employee->official->job_type;

                $details->working_hour = floor($workingDetails['workingHour'] / 3600) . gmdate(":i:s", $workingDetails['workingHour'] % 3600);
                $details->total_working_hour = floor($workingDetails['totalWorkingHour'] / 3600) . gmdate(":i:s", $workingDetails['totalWorkingHour'] % 3600);
                $details->overtime_hour = floor($workingDetails['overtimeHour'] / 3600) . gmdate(":i:s", $workingDetails['overtimeHour'] % 3600);
                $details->working_day = $workingDetails['workingDay'];
                $details->total_working_day = $workingDetails['totalWorkingDay'];
                $details->overtime_day = $workingDetails['overtimeDay'];
                $details->basic = $workingDetails['basicAmount'];

                $salaryAmount += $details->basic;

                $details->overtime = $workingDetails['overtimeAmount'];

                $salaryAmount += $details->overtime;

                $details->leave = $totalLeave;
                $details->loan = round($loans - $installment, 2);

                $salaryAmount -= $details->loan;

                $details->shift = $employee->official->shift;
                $details->rate = $employee->official->grade->rate;
                $details->overtime_rate = $employee->official->grade->overtime_rate;
                $salary->details()->save($details);

                foreach ($employee->official->allowances as $allowance) {
                    $salaryAllowance = new SalaryAllowance();
                    $salaryAllowance->allowance_id = $allowance->allowance_id;
                    $salaryAllowance->type = $allowance->type;
                    $salaryAllowance->rate = $allowance->rate;
                    $salary->allowances()->save($salaryAllowance);

                    if ($allowance->type) {
//            calculate percent
                        $amount = ($allowance->rate / $salary->details->rate) * 100;
                    } else {
//              calculate currency;
                        $amount = $allowance->rate;
                    }
//            addition or deduction amount
                    if ($allowance->allowance->type) {
                        $salaryAmount -= $amount;
                    } else {
                        $salaryAmount += $amount;
                    }
                }
//            update total Salary amount
                $salary->update(['amount' => $salaryAmount]);
            }
        });
    }
}
