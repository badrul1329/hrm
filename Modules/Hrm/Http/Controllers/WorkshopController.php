<?php

namespace Modules\Hrm\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\EmployeeWorkshop;
use Modules\Hrm\Entities\Trainer;
use Modules\Hrm\Entities\TrainingType;
use Modules\Hrm\Entities\Workshop;
use Illuminate\Support\Facades\Validator;

class WorkshopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $workshops = Workshop::with('type', 'trainer', 'employees')->get();
            return Datatables::of($workshops)
                ->addIndexColumn()
                ->editColumn('type_id', function ($workshop) {
                    return $workshop->type->name;
                })
                ->editColumn('trainer_id', function ($workshop) {
                    return $workshop->trainer->name;
                })
                ->addColumn('action', function ($workshop) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-workshop-view')) {
                        $btn .= '<a class="view-btn" type="button" data-toggle="modal" data-target="#viewModal" data-id="' . $workshop->id . '"><i class="ik ik-eye"></i></a>';
                    }
                    if (Auth::user()->can('hrm-workshop-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#workshopModal" data-whatever="1" data-id="' . $workshop->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-workshop-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $workshop->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $types = TrainingType::all();
        $trainers = Trainer::all();
        return view('hrm::workshop', compact('types', 'trainers'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:workshops,name',
            'type' => 'required',
            'trainer' => 'required',
            'cost' => 'nullable',
            'from' => 'required|date_format:d-M-Y',
            'to' => 'required|date_format:d-M-Y',
            'desc' => 'nullable',
            'employee.*' => 'required',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $workshop = new Workshop();
        $workshop->name = $request->name;
        $workshop->desc = $request->desc;
        $workshop->type_id = $request->type;
        $workshop->trainer_id = $request->trainer;
        $workshop->amount = $request->cost;
        $workshop->from = date('Y-m-d', strtotime($request->from));
        $workshop->to = date('Y-m-d', strtotime($request->to));
        $workshop->save();
        if ($request->employee) {
            foreach ($request->employee as $empid) {
                $employee = new EmployeeWorkshop();
                $employee->employee_id = $empid;
                $employee->workshop_id = $workshop->id;
                $employee->save();
            }
        }
        return $workshop;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Workshop $workshop)
    {
        if ($request->ajax()) {
            return $workshop->load('type', 'trainer', 'employees');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Workshop $workshop)
    {
        $rules = [
            'name' => 'required|unique:workshops,name,' . $workshop->id,
            'type' => 'required',
            'trainer' => 'required',
            'cost' => 'nullable',
            'from' => 'required|date_format:d-M-Y',
            'to' => 'required|date_format:d-M-Y',
            'desc' => 'nullable',
            'employee.*' => 'required',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $workshop->name = $request->name;
        $workshop->desc = $request->desc;
        $workshop->type_id = $request->type;
        $workshop->trainer_id = $request->trainer;
        $workshop->amount = $request->cost;
        $workshop->from = date('Y-m-d', strtotime($request->from));
        $workshop->to = date('Y-m-d', strtotime($request->to));
        $workshop->save();
        $workshop->employees()->detach();
        if ($request->employee) {
            foreach ($request->employee as $empid) {
                $employee = new EmployeeWorkshop();
                $employee->employee_id = $empid;
                $employee->workshop_id = $workshop->id;
                $employee->save();
            }
        }
        return $workshop;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Workshop $workshop)
    {
        $workshop->delete();
        return $workshop;
    }
}
