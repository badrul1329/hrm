<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\LeaveType;
use DataTables;

class LeaveTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $types = LeaveType::all();
            return Datatables::of($types)
                ->addIndexColumn()
                ->addColumn('action', function ($type) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-leave-type-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#typeModal" data-whatever="1" data-id="' . $type->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-leave-type-delete')){
                    $btn .= '<a class="delete-btn" type="button" data-id="' . $type->id . '"><i class="ik ik-trash-2"></i></a>';
                }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::leave_types');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:leave_types,name',
            'days' => 'required',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $leaveType = new LeaveType();
        $leaveType->name = $request->name;
        $leaveType->days = $request->days;
        $leaveType->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, LeaveType $type)
    {
        if ($request->ajax()) {
            return $type;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, LeaveType $type)
    {
        $rules = [
            'name' => 'required|unique:leave_types,name,' . $type->id,
            'days' => 'required',
        ];
        $messages = [
            'name.required' => 'The name field is required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $type->name = $request->name;
        $type->days = $request->days;
        $type->save();
        return $type;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(LeaveType $type)
    {
        $type->delete();
        return $type;
    }

    /**
     * Return all types.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if ($request->ajax()) {
            return LeaveType::all();
        }
    }
}
