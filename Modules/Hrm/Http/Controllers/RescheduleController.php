<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\EmployeeReschedule;
use Yajra\DataTables\Facades\DataTables;

class RescheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $schedules = EmployeeReschedule::with('employee', 'schedule')->get();
            return Datatables::of($schedules)
                ->addIndexColumn()
                ->editColumn('date', function (EmployeeReschedule $schedule) {
                    return date('d-m-Y', strtotime($schedule->date));
                })
                ->editColumn('shift', function (EmployeeReschedule $schedule) {
                    return $schedule->schedule->name . " (" . $schedule->schedule->from . " - " . $schedule->schedule->to . ")";
                })
                ->addColumn('action', function ($schedule) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-reshift-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#rescheduleModal" data-whatever="1" data-id="' . $schedule->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-reshift-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $schedule->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::reschedule');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'employee' => 'required',
            'shift' => 'required',
            'from' => 'required|date_format:d-M-Y',
            'to' => 'required|date_format:d-M-Y',
        ];
        $messages = [
            'from.required' => 'The Starting date is required.',
            'to.required' => 'The Ending date is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $begin = Carbon::parse($request->from);
        $end = Carbon::parse($request->to);
        if ($begin->diffInDays($end, false) < 0) {
            return response()->json([
                'message' => 'Ending should be greater than or equal to Begin date',
            ], 400);
        }
        $difference = $begin->diffInDays($end) + 1;
        for ($i = $difference; $i > 0; $i--) {
            $reschedule = new EmployeeReschedule();
            $reschedule->employee_id = $request->employee;
            $reschedule->shift = $request->shift;
            $reschedule->date = $begin->format('Y-m-d');
            $reschedule->save();
            $begin = $begin->addDay();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, EmployeeReschedule $reschedule)
    {
        if ($request->ajax()) {
            return $reschedule;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, EmployeeReschedule $reschedule)
    {
        $rules = [
            'employee' => 'required',
            'shift' => 'required',
            'from' => 'required|date_format:d-M-Y',
        ];
        $messages = [
            'from.required' => 'The Date is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();

        $schedule = EmployeeReschedule::where('id', '<>', $reschedule->id)->where('employee_id', $request->employee)->whereDate('date', Carbon::parse($request->from)->format('Y-m-d'))->get();
        if (count($schedule)) {
            return response()->json([
                'message' => 'Already Have another schedule',
            ], 400);
        } else {
            $reschedule->employee_id = $request->employee;
            $reschedule->shift = $request->shift;
            $reschedule->date = Carbon::parse($request->from)->format('Y-m-d');
            $reschedule->save();
            return $reschedule;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public
    function destroy(Request $request, EmployeeReschedule $reschedule)
    {
        if ($request->ajax()) {
            $reschedule->delete();
        }
    }
}
