<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\ShiftSchedule;
use Illuminate\Support\Facades\Validator;

class ShiftScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $shift_schedules = ShiftSchedule::all();
            return Datatables::of($shift_schedules)
                ->addIndexColumn()
                ->editColumn('from', function (ShiftSchedule $shift_schedule) {
                    return date('H:i', strtotime($shift_schedule->from));
                })
                ->editColumn('to', function (ShiftSchedule $shift_schedule) {
                    return date('H:i', strtotime($shift_schedule->to));
                })
                ->addColumn('length', function ($shift_schedule) {
                    $start = Carbon::parse($shift_schedule->from);
                    $end = Carbon::parse($shift_schedule->to);
                    return $start->diff($end)->format('%H hour %I minutes');
                })
                ->addColumn('action', function ($shift_schedule) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-shift-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#shiftScheduleModal" data-whatever="1" data-id="' . $shift_schedule->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-shift-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $shift_schedule->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::shift_schedules');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:shift_schedules,name',
            'from' => 'required|date_format:H:i',
            'to' => 'required|date_format:H:i',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();

        $beginHour = Carbon::parse($request->from);
        $endHour = Carbon::parse($request->to);
        if ($beginHour->gt($endHour)) {
            return response()->json([
                'message' => 'End hour should be greater than Begin hour',
            ], 400);
        }

        $shift_schedule = new ShiftSchedule();
        $shift_schedule->name = $request->name;
        $shift_schedule->from = date('H:i:00', strtotime($request->from));
        $shift_schedule->to = date('H:i:00', strtotime($request->to));
        $shift_schedule->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, ShiftSchedule $shift_schedule)
    {
        if ($request->ajax()) {
            return $shift_schedule;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, ShiftSchedule $shift_schedule)
    {
        $rules = [
            'name' => 'required|unique:shift_schedules,name,' . $shift_schedule->id,
            'from' => 'required|date_format:H:i',
            'to' => 'required|date_format:H:i',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $beginHour = Carbon::parse($request->from);
        $endHour = Carbon::parse($request->to);
        if ($beginHour->addMinute()->gt($endHour)) {
            return response()->json([
                'message' => 'End hour should be after than Begin hour',
            ], 400);
        }
        $shift_schedule->name = $request->name;
        $shift_schedule->from = date('H:i', strtotime($request->from));
        $shift_schedule->to = date('H:i', strtotime($request->to));
        $shift_schedule->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(ShiftSchedule $shift_schedule)
    {
        $shift_schedule->delete();
        return $shift_schedule;
    }

    /**
     * Return all employees.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if ($request->ajax()) {
            return ShiftSchedule::all();
        };
    }
}
