<?php

namespace Modules\Hrm\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Modules\Hrm\Entities\Trainer;
use Illuminate\Support\Facades\Validator;

class TrainerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $trainers = Trainer::all();
            return Datatables::of($trainers)
                ->addIndexColumn()
                ->addColumn('action', function ($trainer) {
                    $btn = '<div class="table-actions">';
                    if(Auth::user()->can('hrm-trainer-view')) {
                        $btn .= '<a class="view-btn" type="button" data-toggle="modal" data-target="#viewModal" data-id="' . $trainer->id . '"><i class="ik ik-eye"></i></a>';
                    }
                    if(Auth::user()->can('hrm-trainer-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#trainerModal" data-whatever="1" data-id="' . $trainer->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if(Auth::user()->can('hrm-trainer-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $trainer->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::trainer');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:trainers,email',
            'phone' => 'required',
            'address' => 'nullable',
            'company' => 'nullable',
            'expertise' => 'nullable',
            'photo' => 'nullable|mimes:jpeg,bmp,png'
        ];
        $messages = [
            'email.required' => 'The email field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $trainer = new Trainer();
        $trainer->name = $request->name;
        $trainer->email = $request->email;
        $trainer->phone = $request->phone;
        $trainer->address = $request->address;
        $trainer->company = $request->company;
        $trainer->expertise = $request->expertise;
        if ($request->hasFile('photo')) {
            $value = $request->photo;
            $extension = $value->getClientOriginalExtension();
//            $name = $value->getClientOriginalName();
            $image = microtime(true) . ".$extension";
            Image::make($value)->resize(400, 400)->save(storage_path('/app/public/hrm/trainer/') . $image);
            $trainer->photo = $image;
        }
        $trainer->save();
        return $trainer;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request,Trainer $trainer)
    {
        if ($request->ajax()) {
            return $trainer;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Trainer $trainer)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:trainers,email,'.$trainer->id,
            'phone' => 'required',
            'address' => 'nullable',
            'company' => 'nullable',
            'expertise' => 'nullable',
            'photo' => 'nullable|mimes:jpeg,bmp,png'
        ];
        $messages = [
            'email.required' => 'The email field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $trainer->name = $request->name;
        $trainer->email = $request->email;
        $trainer->phone = $request->phone;
        $trainer->address = $request->address;
        $trainer->company = $request->company;
        $trainer->expertise = $request->expertise;
        if ($request->hasFile('photo')) {
            $value = $request->photo;
            $extension = $value->getClientOriginalExtension();
//            $name = $value->getClientOriginalName();
            $image = microtime(true) . ".$extension";
            Image::make($value)->resize(400, 400)->save(storage_path('/app/public/hrm/trainer/') . $image);
            File::delete(storage_path('/app/public/hrm/trainer/') . $trainer->photo);
            $trainer->photo = $image;
        }
        $trainer->save();
        return $trainer;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Trainer $trainer)
    {
        File::delete(storage_path('/app/public/hrm/trainer/') . $trainer->photo);
        $trainer->delete();
        return $trainer;
    }

    /**
     * all positions.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if($request->ajax()){
            return Trainer::all();
        }
    }

}
