<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\Models\Phrase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class PhraseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $phrases = Phrase::all();
            return Datatables::of($phrases)
                ->addIndexColumn()
                ->addColumn('action', function ($phrase) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-phrase-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#phraseModal" data-whatever="1" data-id="' . $phrase->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-phrase-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $phrase->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::common.phrase');
    }
}
