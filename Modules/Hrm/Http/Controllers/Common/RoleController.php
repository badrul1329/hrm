<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $roles = Role::all();
            return Datatables::of($roles)
                ->addIndexColumn()
                ->addColumn('action', function ($allowance) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-role-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#roleModal" data-whatever="1" data-id="' . $allowance->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-role-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $allowance->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::common.role');
    }

}
