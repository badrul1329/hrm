<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hrm::common.profile');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'photo' => 'nullable',
            'name' => 'required',
            'email' => 'nullable|unique:users,email',
            'phone' => 'required',

            'street' => 'required',
            'city' => 'required',
            'district' => 'required',
            'zip' => 'required',
            'country' => 'required',

            'gender' => 'required',
            'birthday' => 'nullable',
            'religion' => 'required',
            'marital_status' => 'required'
        ];
        $messages = [
            'name.required' => 'The name field is required.',
            'marital_id.required' => 'The marital status is required.'
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $user = User::with('details.address')->find($request->id);
        if (!$user->details) {
            $details = new UserDetails();
            $user->details_type = UserDetails::class;
        } else {
            $details = $user->details;
        }
        $details->name = $request->name;
        $details->email = $request->email;
        $details->phone = $request->phone;
        $details->gender = $request->gender;
        $details->birthday = $request->dob;
        $details->marital_id = $request->marital_status;
        $details->religion_id = $request->religion;
        if ($request->hasFile('photo')) {
            $value = $request->photo;
            $extension = $value->getClientOriginalExtension();
            $image = microtime(true) . ".$extension";
            Image::make($value)->resize(400, 400)->save(storage_path('/app/public/user/photo/') . $image);
            $details->photo = $image;
        }
        $details->save();
        if (!$details->address) {
            $address = new UserAddress();
//            $address->user_details_id = $details->id;
        } else {
            $address = $details->address;
        }
        $address->street = $request->street;
        $address->city = $request->city;
        $address->district = $request->district;
        $address->zip_code = $request->zip;
        $address->country = $request->country;
//        $address->save();
        if (!$details->address) {
            $details->address()->save($address);
        } else {
            $details->push();
        }

        $user->details_id = $details->id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('hrm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('hrm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function changePass(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'current_password' => 'required|string|min:8',
                'password' => 'required|string|min:8|confirmed',
            ];
            $messages = [
                'current_password.required' => 'The Current Password is required.',
            ];
            Validator::make($request->all(), $rules, $messages)->validate();
            $user = Auth::user();
            if (Hash::check($request->current_password, $user->getAuthPassword())) {
                $user->password = bcrypt($request->password);
                $user->save();
                return $user;
            } else {
                return response()->json([
                    'errors' => [
                        'password' => ['Current Password is not matched']
                    ]
                ], 422);
            }
        }
    }
}
