<?php

namespace Modules\Hrm\Http\Controllers\Common;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $activities = Activity::orderBy('created_at','desc')->get();
            return Datatables::of($activities)
                ->addIndexColumn()
                ->addColumn('causer', function ($activity) {
                    if($activity->causer){
                        return $activity->causer->name;
                    }
                })
                ->make(true);
        }
        return view('hrm::common.activity');
    }
}
