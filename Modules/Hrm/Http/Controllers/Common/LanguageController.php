<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class LanguageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $languages = Language::all();
            return Datatables::of($languages)
                ->addIndexColumn()
                ->addColumn('action', function ($language) {
                    $btn = '<div class="table-actions">';
                    if(Auth::user()->can('hrm-language-phrase')) {
                        $btn .= '<a href="' . route('hrm.languages.phrases.index', $language->id) . '" class="settings-btn" type="button"><i class="ik ik-settings"></i></a>';
                    }
                    if(Auth::user()->can('hrm-language-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#languageModal" data-whatever="1" data-id="' . $language->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if(Auth::user()->can('hrm-language-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $language->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    if ($language->code != 'en') return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::common.language');
    }
}
