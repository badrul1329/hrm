<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\Division;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class DivisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $divisions = Division::with('department')->get();
            return Datatables::of($divisions)
                ->addIndexColumn()
                ->addColumn('action', function ($division) {
                    $btn = '<div class="table-actions">';
                    if(Auth::user()->can('hrm-division-edit')){
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#divisionModal" data-whatever="1" data-id="' . $division->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if(Auth::user()->can('hrm-division-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $division->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::division');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:divisions,name',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $division = new Division();
        $division->name = $request->name;
        $division->dept_id = $request->department;
        $division->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request,Division $division)
    {
        if ($request->ajax()) {
            return $division->load('department');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Division $division)
    {
        $rules = [
            'name' => 'required|unique:divisions,name,'.$division->id,
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $division->name = $request->name;
        $division->dept_id = $request->department;
        $division->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Division $division)
    {
        $division->delete();
        return $division;
    }

    /**
     * all positions.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if($request->ajax()){
            return Division::all();
        }
    }
}
