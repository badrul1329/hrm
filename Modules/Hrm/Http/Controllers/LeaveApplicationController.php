<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\LeaveApplication;
use Modules\Hrm\Entities\LeaveDocument;
use Yajra\DataTables\DataTables;

class LeaveApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $applications = LeaveApplication::with('employee', 'type', 'approvedBy')->get();
            return DataTables::of($applications)
                ->addIndexColumn()
                ->editColumn('applied_from', function ($application) {
                    return Carbon::parse($application->applied_from)->format('d-M-Y');
                })
                ->editColumn('applied_to', function ($application) {
                    return Carbon::parse($application->applied_to)->format('d-M-Y');
                })
                ->addColumn('applied_days', function ($application) {
                    $from = Carbon::parse($application->applied_from);
                    $to = Carbon::parse($application->applied_to);
                    return $from->diffInDays($to) + 1;
                })
                ->editColumn('from', function ($application) {
                    if ($application->from) {
                        return Carbon::parse($application->from)->format('d-M-Y');
                    }
                })
                ->editColumn('to', function ($application) {
                    if ($application->to) {
                        return Carbon::parse($application->to)->format('d-M-Y');
                    }
                })
                ->editColumn('approved_days', function ($application) {
                    if ($application->from && $application->to) {
                        $from = Carbon::parse($application->from);
                        $to = Carbon::parse($application->to);
                        return $from->diffInDays($to) + 1;
                    }
                })
                ->editColumn('approved_by', function ($application) {
                    if ($application->approved_by) {
                        return $application->approvedBy->name;
                    }
                })
                ->addColumn('action', function ($application) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-leave-application-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#applicationModal" data-whatever="1" data-id="' . $application->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-leave-application-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $application->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::leave_application');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'employee' => 'required',
            'type' => 'required',
            'applied_from' => 'required|date_format:d-M-Y',
            'applied_to' => 'required|date_format:d-M-Y',
            'from' => 'nullable|date_format:d-M-Y',
            'to' => 'nullable|date_format:d-M-Y|required_with:from',
            'approved_by' => 'nullable',
            'reason' => 'nullable',
            'doc.*.title' => 'string|nullable',
            'doc.*.doc' => 'file|nullable',
        ];
        $messages = [
            'applied_from.required' => 'The Starting date is required.',
            'applied_to.required' => 'The Ending date is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $begin = Carbon::parse($request->applied_from);
        $end = Carbon::parse($request->applied_to);
        if ($begin->diffInDays($end, false) < 0) {
            return response()->json([
                'message' => 'Ending should be greater than or equal to Begin date',
            ], 400);
        }
        $begin = Carbon::parse($request->from);
        $end = Carbon::parse($request->to);
        if ($begin->diffInDays($end, false) < 0) {
            return response()->json([
                'message' => 'Ending should be greater than or equal to Begin date',
            ], 400);
        }
        $application = new LeaveApplication();
        $application->employee_id = $request->employee;
        $application->type_id = $request->type;
        $application->applied_from = date('Y-m-d', strtotime($request->applied_from));
        $application->applied_to = date('Y-m-d', strtotime($request->applied_to));
        if ($request->from) {
            $application->from = date('Y-m-d', strtotime($request->from));
        }
        if ($request->to) {
            $application->to = date('Y-m-d', strtotime($request->to));
        }
        $application->approved_by = $request->approved_by;
        $application->reason = $request->reason;
        $application->save();

        foreach ($request->doc as $doc) {
            if ($doc['title'] && $doc['doc']) {
                $document = new LeaveDocument();
                $document->title = $doc['title'];
                $extension = $doc['doc']->getClientOriginalExtension();
                $name = microtime(true) . ".$extension";
                $doc['doc']->move(storage_path('/app/public/hrm/leave/'), $name);
                $document->file = $name;
                $document->application_id = $application->id;
                $document->save();
            }
        }
        return $application;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, LeaveApplication $application)
    {
        if ($request->ajax()) {
            return $application->load('documents');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, LeaveApplication $application)
    {
        $rules = [
            'employee' => 'required',
            'type' => 'required',
            'applied_from' => 'required|date_format:d-M-Y',
            'applied_to' => 'required|date_format:d-M-Y',
            'from' => 'nullable|date_format:d-M-Y',
            'to' => 'nullable|date_format:d-M-Y|required_with:from',
            'approved_by' => 'nullable',
            'reason' => 'nullable',
        ];
        $messages = [
            'applied_from.required' => 'The Starting date is required.',
            'applied_to.required' => 'The Ending date is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $begin = Carbon::parse($request->applied_from);
        $end = Carbon::parse($request->applied_to);
        if ($begin->diffInDays($end, false) < 0) {
            return response()->json([
                'message' => 'Ending should be greater than or equal to Begin date',
            ], 400);
        }
        $begin = Carbon::parse($request->from);
        $end = Carbon::parse($request->to);
        if ($begin->diffInDays($end, false) < 0) {
            return response()->json([
                'message' => 'Ending should be greater than or equal to Begin date',
            ], 400);
        }
        $application->employee_id = $request->employee;
        $application->type_id = $request->type;
        $application->applied_from = date('Y-m-d', strtotime($request->applied_from));
        $application->applied_to = date('Y-m-d', strtotime($request->applied_to));
        if ($request->from) {
            $application->from = date('Y-m-d', strtotime($request->from));
        }
        if ($request->to) {
            $application->to = date('Y-m-d', strtotime($request->to));
        }
        $application->approved_by = $request->approved_by;
        $application->reason = $request->reason;
        $application->save();
        return $application->load('documents');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, LeaveApplication $application)
    {
        if ($request->ajax()) {
            $application->delete();
        }
    }
}
