<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Hrm\Entities\Allowance;
use Modules\Hrm\Entities\Employee;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loan(Request $request)
    {
        if ($request->ajax()) {
            $employees = Employee::with('official.division', 'loan', 'installment')->get();
            return Datatables::of($employees)
                ->addIndexColumn()
                ->addColumn('division', function ($employee) {
                    if ($employee->official->division) {
                        return $employee->official->division()->first()->name;
                    }
                })
                ->addColumn('department', function ($employee) {
                    if ($employee->official->division) {
                        return $employee->official->division()->first()->department->name;
                    }
                })
                ->addColumn('loan', function ($employee) {
                    return $employee->loan->sum('amount');
                })
                ->addColumn('installment', function ($employee) {
                    return $employee->installment->sum('paid');
                })
                ->make(true);
        }
        return view('hrm::reports.loan');
    }

}
