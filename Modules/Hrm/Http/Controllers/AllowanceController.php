<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\Allowance;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class AllowanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $allowances = Allowance::all();
            return Datatables::of($allowances)
                ->addIndexColumn()
                ->editColumn('type', function (Allowance $allowance) {
                    if ($allowance->type) {
                        return "Deduction";
                    } else {
                        return "Addition";
                    }
                })
                ->addColumn('action', function ($allowance) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-allowance-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#allowanceModal" data-whatever="1" data-id="' . $allowance->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-allowance-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $allowance->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::allowance');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:allowances,name',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $allowance = new Allowance();
        $allowance->name = $request->name;
        $allowance->type = $request->type;
        $allowance->save();
        return $allowance;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Allowance $allowance)
    {
        if ($request->ajax()) {
            return $allowance;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Allowance $allowance)
    {
        $rules = [
            'name' => 'required|unique:allowances,name,' . $allowance->id,
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $allowance->name = $request->name;
        $allowance->type = $request->type;
        $allowance->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Allowance $allowance)
    {
        $allowance->delete();
        return $allowance;
    }
}
