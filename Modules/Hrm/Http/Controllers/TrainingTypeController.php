<?php

namespace Modules\Hrm\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\TrainingType;
use Illuminate\Support\Facades\Validator;

class TrainingTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $types = TrainingType::all();
            return Datatables::of($types)
                ->addIndexColumn()
                ->addColumn('action', function ($type) {
                    $btn = '<div class="table-actions">';
                    if(Auth::user()->can('hrm-training-type-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#typeModal" data-whatever="1" data-id="' . $type->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if(Auth::user()->can('hrm-training-type-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $type->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::training_type');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:training_types,name',
            'desc' => 'nullable',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $type = new TrainingType();
        $type->name = $request->name;
        $type->desc = $request->desc;
        $type->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request,TrainingType $type)
    {
        if ($request->ajax()) {
            return $type;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, TrainingType $type)
    {
        $rules = [
            'name' => 'required|unique:training_types,name,'.$type->name,
            'desc' => 'nullable',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $type->name= $request->name;
        $type->desc = $request->desc;
        $type->save();
        return $type;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(TrainingType $type)
    {
        $type->delete();
        return $type;
    }

    /**
     * all positions.
     * @param int $id
     * @return Response
     */
    public function all(Request $request)
    {
        if($request->ajax()){
            return TrainingType::all();
        }
    }

}
