<?php

namespace Modules\Hrm\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('hrm::settings');
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'favicon' => 'nullable|mimes:jpeg,png,jpg,gif,svg,ico|max:2048',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'language' => 'required|integer',
            'currency' => 'required|integer',
            'login' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'register' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'footer' => 'nullable|string'
        ];
        $messages = [

        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        foreach ($request->all() as $key => $value) {
            if (is_object($value)) {
                $extension = $value->getClientOriginalExtension();
//                $name = $value->getClientOriginalName();
                $image = microtime(true) . ".$extension";
                if ($key == 'favicon') {
                    if ($extension == 'ico') {
                        Storage::putFileAs('public',$value,$image);
                    } else {
                        Image::make($value)->resize(45, 45)->save(storage_path('/app/public/') . $image);
                    }
                } else {
                    Image::make($value)->save(storage_path('/app/public/') . $image);
                }
                $value = $image;
                $old = Settings::where('option',$key)->first()->value;
                if ($old) {
                    File::delete(storage_path('/app/public/') . $old);
                }
            }
            settings::updateOrCreate(
                ['option' => $key],
                ['value' => $value]
            );
        }
    }
}
