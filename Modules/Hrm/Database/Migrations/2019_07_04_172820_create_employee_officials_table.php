<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeOfficialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_officials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id')->unique();
            $table->unsignedBigInteger('division');
            $table->unsignedBigInteger('position');
            $table->unsignedBigInteger('job_type');
            $table->string('from');
            $table->string('to')->nullable();
            $table->unsignedBigInteger('supervisor')->nullable();
            $table->unsignedBigInteger('shift');
            $table->unsignedBigInteger('grade_id')->nullable();
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('division')->references('id')->on('divisions');
            $table->foreign('position')->references('id')->on('positions');
            $table->foreign('job_type')->references('id')->on('job_types');
            $table->foreign('supervisor')->references('id')->on('employees')->onDelete('set null');
            $table->foreign('shift')->references('id')->on('shift_schedules');
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_officials');
    }
}
