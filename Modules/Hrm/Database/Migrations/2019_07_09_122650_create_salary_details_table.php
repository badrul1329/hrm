<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('salary_id');
            $table->unsignedBigInteger('job_type')->nullable();
            $table->unsignedBigInteger('shift')->nullable();
            $table->boolean('type')->default(0);
            $table->float('basic')->default(0);
            $table->float('overtime')->default(0);
            $table->string('working_hour');
            $table->string('total_working_hour');
            $table->integer('working_day');
            $table->integer('total_working_day');
            $table->integer('rate');
            $table->string('overtime_hour');
            $table->integer('overtime_day');
            $table->integer('overtime_rate');
            $table->integer('leave');
            $table->integer('loan');
            $table->timestamps();

            $table->foreign('salary_id')->references('id')->on('salaries')->onDelete('cascade');
            $table->foreign('job_type')->references('id')->on('job_types')->onDelete('set null');
            $table->foreign('shift')->references('id')->on('shift_schedules')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_details');
    }
}
