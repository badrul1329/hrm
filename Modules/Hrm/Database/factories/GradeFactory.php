<?php

namespace Modules\Hrm\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class GradeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Hrm\Entities\Grade::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=> fake()->unique()->word,
            'type' => fake()->boolean,
            'rate' => fake()->randomNumber(3),
            'overtime_rate' => fake()->randomNumber(3),
        ];
    }
}

