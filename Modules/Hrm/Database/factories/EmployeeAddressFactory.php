<?php

namespace Modules\Hrm\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Hrm\Entities\EmployeeAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'street' => fake()->streetAddress,
            'city' => fake()->city,
            'district' => fake()->city,
            'zip_code' => fake()->randomNumber(3),
            'country' => fake()->country,
        ];
    }
}

