<?php

namespace Modules\Hrm\Database\factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Hrm\Entities\Division;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\Grade;
use Modules\Hrm\Entities\JobType;
use Modules\Hrm\Entities\Position;
use Modules\Hrm\Entities\ShiftSchedule;

class EmployeeOfficialFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Hrm\Entities\EmployeeOfficial::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $from = fake()->date('Y-m-d');
        $to = Carbon::parse($from)->addMonths(fake()->numberBetween(1, 12))->format('Y-m-d');
        return [
            'division' => Division::all()->random()->id,
            'position' => Position::all()->random()->id,
            'job_type' => JobType::all()->random()->id,
            'from' => $from,
            'to' => fake()->randomElement([null, $to]),
            'supervisor' => Employee::all()->random()->id,
            'shift' => ShiftSchedule::all()->random()->id,
            'grade_id' => Grade::all()->random()->id,
        ];
    }
}

