<?php

namespace Modules\Hrm\Database\factories;

use App\Models\MaritalInfo;
use App\Models\Religion;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Hrm\Entities\Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => fake()->name,
            'email' => fake()->email,
            'phone' => fake()->phoneNumber,
            'gender' => fake()->boolean,
            'birthday' => fake()->date('Y-m-d'),
            'marital_id' => MaritalInfo::all()->random()->id,
            'religion_id' => Religion::all()->random()->id,
            'photo' => null,
        ];
    }
}

