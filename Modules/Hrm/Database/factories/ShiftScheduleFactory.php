<?php

namespace Modules\Hrm\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ShiftScheduleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Hrm\Entities\ShiftSchedule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>fake()->word,
            'from'=>$from = fake()->time('H:i'),
            'to'=>fake()->time('H:i',$from)
        ];
    }
}

