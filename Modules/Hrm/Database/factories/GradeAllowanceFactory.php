<?php

namespace Modules\Hrm\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Hrm\Entities\Allowance;

class GradeAllowanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Hrm\Entities\GradeAllowance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'allowance_id' => Allowance::all()->random()->id,
            'type' => fake()->boolean,
            'rate' => fake()->randomNumber(1)
        ];
    }
}

