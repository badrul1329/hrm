<?php

namespace Modules\Hrm\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeDocumentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Hrm\Entities\EmployeeDocument::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => fake()->word,
            'file' => fake()->file(public_path('demo'), public_path('storage\hrm\employee\files'), false),
        ];
    }
}

