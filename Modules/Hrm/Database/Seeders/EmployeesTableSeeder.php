<?php

namespace Modules\Hrm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeAddress;
use Modules\Hrm\Entities\EmployeeDocument;
use Modules\Hrm\Entities\EmployeeOfficial;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        Employee::factory()->count(10)->create()->each(function ($employee) {
                $employee->address()->save(EmployeeAddress::factory()->make());
                $employee->official()->save(EmployeeOfficial::factory()->make());
                $employee->documents()->saveMany(EmployeeDocument::factory()->count(3)->make());
            });

    }
}
