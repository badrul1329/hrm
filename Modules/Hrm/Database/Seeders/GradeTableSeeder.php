<?php

namespace Modules\Hrm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\Grade;
use Modules\Hrm\Entities\GradeAllowance;

class GradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        Grade::factory()->count(10)->create()->each(function ($grade) {
            $grade->allowances()->saveMany(GradeAllowance::factory()->count(3)->make());
        });
    }
}
