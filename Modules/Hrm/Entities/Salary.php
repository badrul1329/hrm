<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Salary extends Model
{
    use LogsActivity;

    const name = "Employees Salary";
    
    protected $fillable = ['employee_id', 'from', 'to', 'amount', 'generated_by', 'paid_by', 'paid_at', 'paid_status'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->employee->name} has been {$eventName}";
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function generatedBy()
    {
        return $this->belongsTo(Employee::class, 'generated_by', 'id');
    }

    public function paidBy()
    {
        return $this->belongsTo(Employee::class, 'paid_by', 'id');
    }

    public function details()
    {
        return $this->hasOne(SalaryDetails::class, 'salary_id', 'id');
    }

    public function allowances()
    {
        return $this->hasMany(SalaryAllowance::class, 'salary_id', 'id');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
