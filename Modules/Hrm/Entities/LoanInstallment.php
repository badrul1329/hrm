<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class LoanInstallment extends Model
{
    use LogsActivity;

    const name = "Loan Installment";
    
    protected $fillable = ['employee_id', 'installment_amount', 'paid', 'date', 'comment', 'received_by'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->employee->name} has been {$eventName}";
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function receivedBy()
    {
        return $this->hasOne(Employee::class, 'id', 'received_by');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
