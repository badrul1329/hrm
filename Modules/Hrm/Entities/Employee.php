<?php

namespace Modules\Hrm\Entities;

use App\Models\User;
use App\Models\Religion;
use App\Models\MaritalInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;


class Employee extends Model
{
    use LogsActivity,HasFactory;

    const name = "Employee";
    
    protected $fillable = ['name', 'email', 'phone', 'photo', 'gender', 'birthday', 'marital_id', 'religion_id'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name . " {$this->name} has been {$eventName}";
    }

//    public function getPhotoAttribute($value)
//    {
//        return asset('storage/hrm/employee/photo')."/$value";
//    }
    public function maritalStatus()
    {
        return $this->hasOne(MaritalInfo::class, 'id', 'marital_id');
    }

    public function religion()
    {
        return $this->hasOne(Religion::class, 'id', 'religion_id');
    }

    public function address()
    {
        return $this->hasOne(EmployeeAddress::class, 'employee_id', 'id');
    }

    public function official()
    {
        return $this->hasOne(EmployeeOfficial::class, 'employee_id', 'id');
    }

    public function documents()
    {
        return $this->hasMany(EmployeeDocument::class, 'employee_id', 'id');
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class, 'employee_id', 'id');
    }

    public function loan()
    {
        return $this->hasMany(Loan::class, 'employee_id', 'id');
    }

    public function installment()
    {
        return $this->hasMany(LoanInstallment::class, 'employee_id', 'id');
    }

    public function leave()
    {
        return $this->hasMany(LeaveApplication::class, 'employee_id', 'id');
    }

    public function salary()
    {
        return $this->hasMany(Salary::class, 'employee_id', 'id');
    }

    public function user()
    {
        return $this->morphOne(User::class, 'details');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    protected static function newFactory()
    {
        return \Modules\Hrm\Database\factories\EmployeeFactory::new();
    }
}
