<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Workshop extends Model
{
    use LogsActivity;

    const name = "Workshop";
    
    protected $fillable = ['type_id', 'trainer_id', 'name', 'desc', 'amount', 'from', 'to'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }
    public function type(){
        return $this->belongsTo(TrainingType::class,'type_id','id');
    }
    public function trainer(){
        return $this->belongsTo(Trainer::class,'trainer_id','id');
    }
    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employee_workshops', 'workshop_id', 'employee_id');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
