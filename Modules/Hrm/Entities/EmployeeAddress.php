<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class EmployeeAddress extends Model
{
    use LogsActivity,HasFactory;

    const name = "Employee Address";
    
    protected $fillable = ['employee_id', 'street', 'city', 'district', 'zip_code', 'country'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    protected static $recordEvents = ['updated'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$this->employee->name}'s ".$this::name." has been {$eventName}";
    }
    public function employee(){
        return $this->belongsTo(Employee::class,'id','employee_id');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    protected static function newFactory()
    {
        return \Modules\Hrm\Database\factories\EmployeeAddressFactory::new();
    }
}
