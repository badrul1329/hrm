<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDocument extends Model
{
    use HasFactory;
    const name = "Employee documents";
    
    protected $fillable = ['employee_id', 'title', 'file'];
	
    protected static function newFactory()
    {
        return \Modules\Hrm\Database\factories\EmployeeDocumentFactory::new();
    }
}
