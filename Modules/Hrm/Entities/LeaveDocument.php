<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class LeaveDocument extends Model
{
    use LogsActivity;

    const name = "Leave application Document";
    
    protected $fillable = ['application_id', 'title', 'file'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$this::name} has been {$eventName}";
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
