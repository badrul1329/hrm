<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class EmployeeReschedule extends Model
{
    use LogsActivity;

    const name = "Reschedule";
    
    protected $fillable = ['employee_id', 'shift', 'date'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->employee->name} has been {$eventName}";
    }
    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function schedule()
    {
        return $this->hasOne(ShiftSchedule::class, 'id', 'shift');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
