<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class ShiftSchedule extends Model
{
    use LogsActivity,HasFactory;

    const name = "Shift Schedule";
    
    protected $fillable = ['name','from','to','status'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    protected static function newFactory()
    {
        return \Modules\Hrm\Database\factories\ShiftScheduleFactory::new();
    }
}
