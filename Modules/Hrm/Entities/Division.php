<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Division extends Model
{
    use LogsActivity,HasFactory;

    const name = "Division";
    
    protected $fillable = ['name', 'dept_id'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'dept_id');
    }
	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
    protected static function newFactory()
    {
        return \Modules\Hrm\Database\factories\DivisionFactory::new();
    }
}
