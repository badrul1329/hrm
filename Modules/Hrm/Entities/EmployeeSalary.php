<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class EmployeeSalary extends Model
{
    use LogsActivity;

    const name = "Salary";
    
    protected $fillable = ['employee_id', 'type', 'rate', 'overtime_rate', 'amount'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->employee->name} has been {$eventName}";
    }
    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
