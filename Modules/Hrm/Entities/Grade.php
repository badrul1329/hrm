<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Grade extends Model
{
    use LogsActivity,HasFactory;

    const name = "Salary Grade";
    
    protected $fillable = ['name', 'type', 'rate', 'overtime_rate', 'amount'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }
    public function allowances(){
        return $this->hasMany(GradeAllowance::class,'grade_id','id');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }

    protected static function newFactory()
    {
        return \Modules\Hrm\Database\factories\GradeFactory::new();
    }
}
