<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Loan extends Model
{
    use LogsActivity;

    const name = "Loan";
    
    protected $fillable = ['employee_id', 'amount', 'approved_by', 'approved_at', 'repayment_from','repayment_to', 'installment', 'comment'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->employee->name} has been {$eventName}";
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function approvedBy()
    {
        return $this->hasOne(Employee::class, 'id', 'approved_by');
    }

	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
