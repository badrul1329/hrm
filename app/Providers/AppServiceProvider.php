<?php

namespace App\Providers;

use App\Models\Language;
use App\Models\Settings;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Nwidart\Modules\Facades\Module;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //        Modules
        $activeModules = Module::allEnabled();
        View::share('activeModules', $activeModules);
        //        settings
        if (Schema::hasTable('settings')) {
            $settings = Settings::all()->mapWithKeys(function ($item, $key) {
                return [$item['option'] => $item['value']];
            });
            View::share('settings', $settings);
        }
    }
}
