<?php

namespace App\Http\Controllers;

use App\Models\Religion;
use Illuminate\Http\Request;

class ReligionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display all of the resource.
     * @return Response
     */
    public function all(Request $request)
    {
        if ($request->ajax()) {
            return Religion::all();
        }
    }
}
