<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Currency;
use Illuminate\Support\Facades\Validator;

class CurrencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $currencies = Currency::all();
            return Datatables::of($currencies)
                ->addIndexColumn()
                ->addColumn('action', function ($currency) {
                    $btn = '<div class="table-actions">';
                    $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#currencyModal" data-whatever="1" data-id="' . $currency->id . '"><i class="ik ik-edit-2"></i></a>';
                    $btn .= '<a class="delete-btn" type="button" data-id="' . $currency->id . '"><i class="ik ik-trash-2"></i></a>';
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('currency');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:currencies',
            'symbol' => 'required',
        ];
        $messages = [
            'symbol.required' => 'The symbol field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $currency = new Currency();
        $currency->name = $request->name;
        $currency->symbol = $request->symbol;
        $currency->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request,Currency $currency)
    {
        if ($request->ajax()) {
            return $currency;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Currency $currency)
    {
        $rules = [
            'name' => 'required|unique:currencies,name,'.$currency->id,
            'symbol' => 'required',
        ];
        $messages = [
            'symbol.required' => 'The symbol field is required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $currency->name= $request->name;
        $currency->symbol = $request->symbol;
        $currency->save();
        return $currency;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Currency $currency)
    {
        $currency->delete();
        return $currency;
    }

    public function all(){
        return Currency::all();
    }
}
