<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $activities = Activity::orderBy('created_at', 'desc')->get();
            return Datatables::of($activities)
                ->addIndexColumn()
                ->addColumn('causer', function ($activity) {
                    if ($activity->causer) {
                        return $activity->causer->name;
                    }
                })
                ->make(true);
        }
        return view('activity');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Activity $activity)
    {
        if ($request->ajax()) {
            return $activity;
        }else{
            return redirect()->route('login');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Activity $activity)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Activity $activity)
    {

    }

    /**
     * All Activity list.
     * @param int $id
     * @return Response
     */
    public function all()
    {

    }
}
