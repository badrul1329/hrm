<?php

namespace App\Http\Controllers;

use App\Models\Notice;
use App\Models\NoticeDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class NoticeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $notices = Notice::with('createdBy', 'documents')->get();
            return Datatables::of($notices)
                ->addIndexColumn()
                ->editColumn('created_by', function ($notice) {
                    return $notice->createdBy->name;
                })
                ->addColumn('files', function ($notice) {
                    if (count($notice->documents)) {
                        $files = '';
                        foreach ($notice->documents as $doc) {
                            $files .= "<a href='" . asset('storage/notices/' . $doc->file) . "' target='_blank'><i class='ik ik-file ik-3x text-green'></i></a>";
                        }
                        return $files;
                    };
                })
                ->addColumn('action', function ($notice) {
                    $btn = '<div class="table-actions">';
                    $btn .= '<a class="view-btn" type="button" data-toggle="modal" data-target="#viewModal" data-id="' . $notice->id . '"><i class="ik ik-eye"></i></a>';
                    $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#noticeModal" data-whatever="1" data-id="' . $notice->id . '"><i class="ik ik-edit-2"></i></a>';
                    $btn .= '<a class="delete-btn" type="button" data-id="' . $notice->id . '"><i class="ik ik-trash-2"></i></a>';
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['files', 'action'])
                ->make(true);
        }
        return view('notice');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'desc' => 'nullable',
            'file' => 'nullable|file|max:2048',
        ];
        $messages = [
            'title.required' => 'Title is required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $notice = new Notice();
        $notice->title = $request->title;
        $notice->desc = $request->desc;
        $notice->created_by = Auth::id();
        $notice->save();
        if ($request->document) {
            foreach ($request->document as $value) {
                $document = new NoticeDocument();
                $extension = $value->getClientOriginalExtension();
                $name = microtime(true) . ".$extension";
                $value->move(storage_path('/app/public/notices/'), $name);
                $document->file = $name;
                $notice->documents()->save($document);
            }
        }
        return $notice;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Notice $notice)
    {
        if ($request->ajax()) {
            return $notice->load('documents');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice)
    {
        $rules = [
            'title' => 'required',
            'desc' => 'nullable',
            'file' => 'nullable|file|max:2048',
        ];
        $messages = [
            'title.required' => 'Title is required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $notice->title = $request->title;
        $notice->desc = $request->desc;

        if ($request->document) {
            foreach ($notice->documents()->pluck('file') as $file) {
                File::delete(storage_path("/app/public/notices/" . $file));
            }
            $notice->documents()->delete();
            foreach ($request->document as $value) {
                $document = new NoticeDocument();
                $extension = $value->getClientOriginalExtension();
                $name = microtime(true) . ".$extension";
                $value->move(storage_path('/app/public/notices/'), $name);
                $document->file = $name;
                $notice->documents()->save($document);
            }
        }
        $notice->push();
        return $notice;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Notice $notice)
    {
        if ($request->ajax()) {
            $docs = $notice->documents()->pluck('file');
            foreach ($docs as $file) {
                File::delete(storage_path("/app/public/notices/" . $file));
            }
            $notice->delete();
            return $notice;
        }
    }

    public function all()
    {
        return Notice::all();
    }
}
