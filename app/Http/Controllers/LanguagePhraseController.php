<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Language_phrase;
use App\Models\phrase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use function MongoDB\BSON\toJSON;

class LanguagePhraseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Language $language)
    {
        $phrases = DB::table('phrases')
            ->leftJoin('language_phrases', function ($join) use ($language) {
                $join->on('language_phrases.phrase_id', '=', 'phrases.id')
                    ->where('language_id', $language->id);
            })
            ->select('phrases.id', 'phrases.name', 'language_phrases.name as translation')
            ->get();
        $language->phrases = $phrases;
        return view('language_phrase', compact('language'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Language $language)
    {
        $translation = $request->all();
        $language->phrases()->detach();
        $data = [];
        foreach($translation as $key=>$value){
            if($key && $value){
                $language->phrases()->attach($key, ['name' => $value]);
                $phrase = Phrase::find($key)->name;
                $data[$phrase]=$value;
            }
        }
        File::put(App::langPath() . "/$language->code.json", json_encode($data,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
        return $language->phrases;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Language_phrase $language_phrase
     * @return \Illuminate\Http\Response
     */
    public function show(Language_phrase $language_phrase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Language_phrase $language_phrase
     * @return \Illuminate\Http\Response
     */
    public function edit(Language_phrase $language_phrase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Language_phrase $language_phrase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language_phrase $language_phrase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Language_phrase $language_phrase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language_phrase $language_phrase)
    {
        //
    }
}
