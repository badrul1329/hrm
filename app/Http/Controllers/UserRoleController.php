<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserRoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::with('roles')->get();
            return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('roles', function ($user) {
                    return $user->roles->pluck('name')->implode(' , ');
                })
                ->addColumn('action', function ($user) {
                    $btn = '<div class="table-actions">';
                    $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#roleModal" data-whatever="1" data-id="' . $user->id . '"><i class="ik ik-edit-2"></i></a>';
                    $btn .= '<a class="delete-btn" type="button" data-id="' . $user->id . '"><i class="ik ik-trash-2"></i></a>';
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('user_role');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->ajax()){
            $user = User::find($id);
            return $user->load('roles');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'role' => 'nullable',
        ];
        $messages = [

        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $user = User::find($id);
        $user->syncRoles($request->role);
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $user = User::find($id);
            $user->syncRoles();
            return $user;
        }
    }
}
