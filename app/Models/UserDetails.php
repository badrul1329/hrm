<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class UserDetails extends Model
{
    use LogsActivity;

    protected $fillable = ['name', 'email', 'phone', 'photo', 'gender', 'birthday', 'marital_id', 'religion_id'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this->name . "'s information has been {$eventName}";
    }
    public function user()
    {
        return $this->morphOne('App\User', 'details');
    }
    public function maritalStatus()
    {
        return $this->hasOne(MaritalInfo::class, 'id', 'marital_id');
    }

    public function religion()
    {
        return $this->hasOne(Religion::class, 'id', 'religion_id');
    }
    public function address()
    {
        return $this->hasOne(UserAddress::class, 'user_details_id', 'id');
    }
	
	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
