<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class UserAddress extends Model
{
    use LogsActivity;

    const name = "Address";
    protected $fillable = ['user_details_id', 'street', 'city', 'district', 'zip_code', 'country'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    protected static $recordEvents = ['updated'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$this->user->name}'s " . $this::name . " has been {$eventName}";
    }

    public function user()
    {
        return $this->belongsTo( UserDetails::class, 'user_details_id', 'id');
    }
	
	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
