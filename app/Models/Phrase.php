<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Phrase extends Model
{
    use LogsActivity;

    const name = "Phrase";
    protected $fillable = ['name'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }
	
	public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
