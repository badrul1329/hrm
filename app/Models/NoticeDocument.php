<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticeDocument extends Model
{
    const name = "Notice Document";
    protected $fillable = ['notice_id','file'];
}
