<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguagePhrase extends Model
{
    const name = "Phrase";
    protected $fillable = ['language_id', 'phrase_id', 'name'];

    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }

    public function phrase()
    {
        return $this->hasOne(phrase::class, 'id', 'phrase_id');
    }
}

